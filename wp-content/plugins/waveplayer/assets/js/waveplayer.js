/**
* waveplayer.js v2.0.12
*/

(function($){

    var players,
        options,
		observer,
        loggedUser = false;

    var WavePlayer = function(element) {

        var elem = $(element);
        var obj = this;

        var INTERVAL  = 200,
            ANIMATION = 250;

        var status = 'empty',
            n = -1;

        var getData = function(name) {
            return elem.attr('data-'+name);
        }

        var wavesurfer = _.extend({}, WaveSurfer),
            peaks,
            timerHook,
            tracks,
            runtime = 0,
            info = getData('info'),
            lastStart = 0;

        var audio               = $('<audio>'),
            infoBar             = elem.find('.wvpl-infobar'),
            interface           = elem.find('.wvpl-interface'),
            posterImg           = elem.find('.wvpl-poster'),
            rightBox            = elem.find('.wvpl-right-box'),
            infoText            = elem.find('.wvpl-playing-info'),
            playlist            = elem.find('.wvpl-playlist'),
            playlistWrapper     = elem.find('.wvpl-playlist-wrapper'),
            waveform            = elem.find('.wvpl-waveform'),
            position            = elem.find('.wvpl-position'),
            duration            = elem.find('.wvpl-duration'),
            nextBtn             = elem.find('.wvpl-next .wvpl-icon'),
            prevBtn             = elem.find('.wvpl-prev .wvpl-icon'),
            playBtn             = elem.find('.wvpl-play .wvpl-icon'),
            timeLine            = elem.find('.wvpl-timeline'),
            centerLine          = elem.find('.wvpl-centerline'),
            pointer             = elem.find('.wvpl-pointer'),
            infoBtn             = elem.find('.wvpl-info'),
            volumeBtn           = elem.find('.wvpl-volume');

        /**
        *
        * Initializes the wavesurfer object to the waveform element of the current WavePlayer instance
        *
        */
        var wavesurferInit = function() {

            wavesurfer.init({
                container:          waveform[0],
                waveColor:          getData('wave_color'),
                waveColor2:         getData('wave_color_2'),
                progressColor:      getData('progress_color'),
                progressColor2:     getData('progress_color_2'),
                cursorColor:        getData('cursor_color'),
                cursorColor2:       getData('cursor_color_2'),
                cursorWidth:        getData('cursor_width'),
                hoverOpacity:       parseInt(getData('hover_opacity')),
                waveMode:           parseInt(getData('wave_mode')),
                gapWidth:           parseInt(getData('gap_width')),
                compression:        parseInt(getData('wave_compression')),
                asymmetry:          parseInt(getData('wave_asymmetry')),
                height:             waveform.outerHeight(false),
                normalize:          true,
                reflection:         true,
                hideScrollbar:      true,
            });

            /**
            *
            * Called when the wavesurfer object finishes reading the audio
            * tries to save a peak file in the peak subfolder of the plugin
            *
            */
            wavesurfer.on('ready', function() {

                peaks = wavesurfer.backend.getPeaks(960);

                if (tracks[n].id) {

                    var postData = [
                        {name: 'action', value: 'waveplayer_write_peaks'},
                        {name: 'peaks', value: peaks},
                        {name: 'id', value: tracks[n].id},
                        {name: 'temp_file', value: tracks[n].temp_file},
                    ];
                    $.ajax({
                        url: waveplayer_ajax.ajax_url,
                        type: 'post',
                        data: postData,
                        dataType: 'json',
                        success: function(result) {
                            if ( result.success ) {
                                if ( tracks[n].temp_file ) tracks[n].temp_file = null
                                status = 'stop'
                                displayInfo( tracks[n] );
                                obj.audio.src = tracks[n].file;
                                obj.audio.load()
                                duration.text(tracks[n].length_formatted)
                                position.text(lengthFormatted())
                                peaks = result.data.peaks
                                tracks[n].peaks = peaks
                                wavesurfer.drawer.clearWave()
                                wavesurfer.drawer.drawPeaks(peaks, waveform.width() * wavesurfer.params.pixelRatio)
                                ready()
                                posterImg.animate( {opacity: 1}, ANIMATION )
                                rightBox.animate( {opacity: 1}, ANIMATION )
                                if ( obj.autoplay ) obj.play()
                            }
                        },
                        error: function(result) {
                            console.log(result.responseText);
                        }
                    });
                }
            });

            /**
            *
            * Relocate the progress position when the user click on the waveform
            *
            */
            wavesurfer.drawer.on('click', function(event) {
                if (status == 'play') {
                    var d = new Date();
                    runtime += d.getTime() - lastStart;
                    lastStart = (new Date()).getTime();
                    progress(event.offsetX / wavesurfer.container.clientWidth);
                    obj.audio.currentTime = obj.audio.duration * event.offsetX / wavesurfer.container.clientWidth;
                } else {
                    obj.play();
                }
            });
            wavesurfer.drawer.on('mousemove', function(event) {
                if (wavesurfer.drawer.playing) wavesurfer.drawer.updatePosition(event.offsetX / ( wavesurfer.drawer.width / wavesurfer.drawer.params.pixelRatio ));
            });

            wavesurfer.drawer.on('mouseout', function(event) {
                if (wavesurfer.drawer.playing) wavesurfer.drawer.updatePosition(-1);
            });

        }

        /**
        *
        * Reset the waveform element to display a blank canvas
        *
        */
        var progress = function(pos){
            wavesurfer.drawer.updateProgress(pos);
        }

        /**
        *
        * Reset the waveform element to display a blank canvas
        *
        */
        var waveReset = function(){
            wavesurfer.drawer.clearWave();
            progress(0);
        }

        /**
        *
        * Redraw the current waveform
        *
        */
        var waveRedraw = function(){
            if (n >= 0) {
                wavesurfer.params.height = waveform.height() - parseInt(waveform.css("marginTop")) + parseInt(waveform.css("marginBottom"));
                wavesurfer.updateDrawer();
                wavesurfer.drawer.drawPeaks(peaks, waveform.width() * wavesurfer.params.pixelRatio);
            }
        }

        /**
        *
        * Initializes the current WavePlayer instance
        *
        * @param params, not in use (for future development)
        */
        this.init = function(params) {
            obj.audio = audio[0];

            var asymmetry = parseInt(getData('wave_asymmetry'))
            asymmetry = asymmetry * 100 / ( 1 + asymmetry )
            position.css({top:asymmetry+'%'})
            duration.css({top:asymmetry+'%'})

            var ids = elem.data( 'ids' ),
                url = elem.data( 'url' )

            var index = $('.waveplayer').index(elem);
            tracks = players[index];

            playBtn.addClass( 'fa-spin fa-refresh').removeClass('fa-play' )
            // load the WavePlayer settings from WordPress and sets the 'options' variable

            if (options.full_width_playlist) playlist.css({'margin-left': 0});

            // Initializes the wavesurfer plugin for this instance
            wavesurferInit()

                        // Gets the options for this player from the waveplayer dataset
            obj.autoplay = elem.data( 'autoplay' )
            obj.repeatAll = elem.data( 'repeat_all' )

            if (tracks.length > 0) {
                n = 0;
                if (tracks[n].temp_file) {
                    wavesurfer.load( tracks[n].temp_file );
                } else {
                    infoBar.toggle(info != 'none');
                    playlist.toggle(info == 'playlist' && tracks.length > 1);
                    displayPlaylist();
                    status = 'stop';
                    wavesurfer.drawer.playing = false;
                    preloadTrack();
                    playBtn.removeClass('fa-refresh fa-spin wvpl-disabled').addClass('fa-play');
                    activated()
                }
            } else {
                playBtn.addClass('wvpl-disabled');
                elem.hide()
            }
            if ( index < $('.waveplayer').length - 1 ) {
                $( $('.waveplayer')[index+1] ).waveplayer()
            }
        }

        /**
        *
        * Updates the current position of the player
        *
        */
        var timeUpdate = function() {
            var pc = obj.audio.currentTime / obj.audio.duration;
            progress(pc);
            position.text(lengthFormatted(obj.audio.currentTime))
            var data = {
                position:   obj.audio.currentTime,
                index:      n,
                title:      tracks[n].title
            };
            elem.trigger('timeUpdate', data);
        }

        /**
        *
        * Returns the current position of the player
        *
        */
        this.getCurrentTime = function() {
            return obj.audio.currentTime;
        }

        /**
        *
        * Sets the current position of the player
        *
        */
        this.setCurrentTime = function(position) {
            if ( position < 0 || position > obj.audio.duration ) return false;
            obj.audio.currentTime = position;
            timeUpdate()
        }

        /**
        *
        * Starts the playback
        *
        */
        this.play = function() {
            elem.find('.wvpl-playlist li').removeClass('playing')
            elem.find('.wvpl-playlist li:nth-child('+(1*n+1)+')').addClass('playing')
            lastStart = new Date();
            $('.waveplayer.active').each(function(index, el){
                if ( index != $('.waveplayer').index(elem) ) $(el).data('waveplayer').stop();
            });
            obj.audio.play();
            playBtn.removeClass('fa-play fa-spin fa-spinner').addClass('fa-pause pause');
            status = 'play';
            wavesurfer.drawer.playing = true;
        }

        /**
        *
        * Pauses the playback
        *
        */
        this.pause = function() {
            elem.find('.wvpl-playlist li').removeClass('playing')
            var d = new Date();
            runtime += d.getTime() - lastStart;
            obj.audio.pause();
            playBtn.removeClass('fa-pause pause').addClass('fa-play');
            wavesurfer.drawer.playing = false;
            status = 'pause';
        }

        /**
         *
         * Stops the playback
         *
         * @param   (bool) reset    if true resets the playlist to the first track
         *
         */
        this.stop = function() {
            if (status == 'stop') return false;
            elem.find('.wvpl-playlist li').removeClass('playing')
            obj.audio.pause();
            obj.audio.currentTime = 0;
            position.text(lengthFormatted(obj.audio.currentTime))
            playBtn.removeClass('fa-pause pause').addClass('fa-play');
            wavesurfer.drawer.playing = false;
            status = 'stop';
            n = 0;
            preloadTrack();
        }

        /**
        *
        * Skips to the next or previous track
        *
        * @param    (bool) next     if true or undefined, skips to next track, if false, skip to previous track
        */
        this.skip = function(next) {
            obj.pause();
            loading();
            if (n >= 0) updateStatistics();
            posterImg.animate({opacity: 0},ANIMATION);
            rightBox.animate(
                {opacity: 0},
                ANIMATION,
                function() {
                    waveReset();
                    if (next == null) next = true;
                    next ? n++ : n--;
                    if ( !obj.repeatAll && ( (n >= tracks.length) || (n < 0) ) ) {
                        scrollTo( 0, false );
                        obj.stop();
                        waveReset();
                        obj.autoplay = false;
                        preloadTrack();
                        if (options.jump) {
                            var curPlayerIndex = elem.index('.waveplayer.active') + 1;
                            if ( curPlayerIndex < $('.waveplayer.active').length ) {
                                $('.waveplayer.active').eq(curPlayerIndex).data('waveplayer').play();
                            }
                        };
                    } else {
                        if (n == tracks.length) n = 0;
                        if (n < 0) n = tracks.length - 1;
                        scrollTo( n, next );
                        status = 'stop';
                        wavesurfer.drawer.playing = false;
                        clearInterval(timerHook);
                        preloadTrack(true);
                    }
                }
            );
        }

        /**
        *
        * Skips to the next or previous track
        *
        * @param    (bool) next     if true or undefined, skips to next track, if false, skip to previous track
        */
        this.skipTo = function(index) {
            if (index == null || index < 0 || index >= tracks.length) return false;
            obj.pause();
            loading();
            if (n >= 0 && n < tracks.length) updateStatistics();
            posterImg.animate({opacity: 0},ANIMATION);
            rightBox.animate(
                {opacity: 0},
                ANIMATION,
                function() {
                    waveReset();
                    n = index;
                    preloadTrack(true);
                }
            );
        }


        var scrollTo = function(index, next) {
            var $el = playlist.find('li').eq(index);
            if ( !$el ) return false;

            if ( next ) {
                if ( $el.position().top + $el.height() > playlist.scrollTop() + playlist.height() )
                    playlist.animate({scrollTop: $el.position().top +$el.outerHeight() - playlist.outerHeight()})
            } else {
                if ( $el.position().top < playlist.scrollTop() )
                    playlist.animate({scrollTop: $el.position().top})
            }
        }


        var loading = function() {
            playBtn.removeClass('fa-play wvpl-disabled').addClass('fa-spin fa-refresh');
            elem.trigger( 'loading', {track: tracks[n]} );
        }

        var ready = function() {
            playBtn.removeClass('fa-spin fa-refresh wvpl-disabled');
            status == 'play' ? playBtn.addClass('fa-pause') : playBtn.addClass('fa-play');
            elem.trigger( 'ready', {track: tracks[n]} );
        }

        var activated = function() {
            elem.trigger( 'activated', {id: elem.attr('id'), index: $('.waveplayer').index(elem)} );
        }

        this.refresh = function() {
            waveRedraw();
        }

        /**
        *
        * Preloads the current track
        *
        * @param playnow, if true, after preloading the track, automatically starts the playback
        */
        var preloadTrack = function(playnow) {
            if (n < 0) n = 0;
            checkSkipState();
            if ( typeof( playnow ) == 'undefined' ) playnow = false;
            loading();

            if ( tracks[n].peaks ) {
                displayInfo( tracks[n] );
                obj.audio.src = tracks[n].file;
                duration.text(tracks[n].length_formatted)
                position.text(lengthFormatted())
                obj.audio.load();
                peaks = tracks[n].peaks;
                wavesurfer.drawer.clearWave();
                wavesurfer.drawer.drawPeaks(peaks, waveform.width() * wavesurfer.params.pixelRatio);
                ready();
                posterImg.animate({opacity: 1}, ANIMATION);
                rightBox.animate({opacity: 1}, ANIMATION, function(event){
                    if ( playnow || obj.autoplay ) obj.play();
                });
            } else {
                wavesurfer.load(tracks[n].file);
            }
        }

        this.reload = function() {
            preloadTrack();
        }

        /**
        *
        * Toggle the playback status
        *
        */
        this.toggle = function() {
            switch (status) {
                case 'play':
                    obj.pause();
                    break;
                case 'stop':
                case 'pause':
                    obj.play();
                    break;
            }
        }

        this.toggleInfo = function() {
            if (info == 'playlist' && tracks.length == 1 ) info = 'bar';
            switch (info) {
                // No info
                case 'playlist':
                    playlist.slideUp();
                    infoBar.fadeOut().addClass('wvpl-inactive');
                    infoBtn.find('span').addClass('fa-info').removeClass('fa-times fa-list');
                    info = 'none';
                    break;
                // Info Bar only
                case 'none':
                    infoBar.show().removeClass('wvpl-inactive');
                    if ( tracks.length > 1 ) {
                        infoBtn.find('span').addClass('fa-list').removeClass('fa-times fa-info');
                    } else {
                        infoBtn.find('span').addClass('fa-times').removeClass('fa-info fa-list');
                    }
                    info = 'bar';
                    break;
                // Info Bar and Playlist
                case 'bar':
                    if ( tracks.length > 1 ) {
                        playlist.slideDown();
                        infoBtn.find('span').addClass('fa-times').removeClass('fa-info fa-list');
                        info = 'playlist';
                    } else {
                        infoBar.hide().addClass('wvpl-inactive');
                        infoBtn.find('span').addClass('fa-info').removeClass('fa-times fa-list');
                        info = 'none';
                    }
                    break;
            }
        }


        /**
        *
        * Sets the visibility state of each skip button based on the current track in the playlist and the 'repeatAll' option
        *
        * @param playnow, if true, after preloading the track, automatically starts the playback
        *
        */
        var checkSkipState = function() {
            switch (n) {
                case 0:
                    if (!obj.repeatAll) prevBtn.addClass('wvpl-disabled');
                    if (tracks.length > 1) nextBtn.removeClass('wvpl-disabled');
                    break;
                case 1:
                    if (tracks.length > 1) {
                        prevBtn.removeClass('wvpl-disabled');
                        nextBtn.removeClass('wvpl-disabled');
                    }
                    if (tracks.length == 2 && !obj.repeatAll) nextBtn.addClass('wvpl-disabled');
                    break;
                case tracks.length - 2:
                    if (tracks.length > 1) {
                        prevBtn.removeClass('wvpl-disabled');
                        nextBtn.removeClass('wvpl-disabled');
                    }
                    break;
                case tracks.length - 1:
                    if (tracks.length > 1) {
                        prevBtn.removeClass('wvpl-disabled');
                        nextBtn.removeClass('wvpl-disabled');
                    }
                    if (!obj.repeatAll) nextBtn.addClass('wvpl-disabled');
                    break;
                default:
                    prevBtn.removeClass('wvpl-disabled');
                    nextBtn.removeClass('wvpl-disabled');
            }
        }

        /**
        *
        * Displays the information of the track provided
        *
        * @param track, an object containing the track whose info are going to be displayed
        *
        */
        var displayInfo = function(track) {

            var message = options.template,
                poster = track.poster ? 'url('+track.poster+')' : '',
                thumbnail = track.thumbnail ? 'url('+track.thumbnail+')' : '';

            posterImg.css('background-image', poster);

            if ( message == null ) {
                message = track.title;
            } else {
                message = infoTemplate(message, track)
            }

            message ? infoText.show().find('.wvpl-infoblock').html( message ) : infoText.hide();

        }

        var infoTemplate = function(template, track) {
            template = template.replace(/%([^ \{]*)(\{[^%]*\})*%/g, function(match, $1, $2){
                var key = $1,
                    s = track[ key ] != null ? track[ key ] : '',
                    attributes = _.extend({
                        class:'',
                        showValue: 1,
                        text: '',
                        raw: 0,
                        icon: ''
                    }, $2 ? JSON.parse($2) : ''),
                    $span = $('<span>');

                switch (key) {
                    case 'title':
                        $span = $('<span>', {class: 'wvpl-title'}).append(
                            $('<span>', {class: 'wvpl-value'}).text(s)
                        )
                        break;
                    case 'play_count':
                        if ( ! track.stats ) break;
                        s = track.stats.play_count;
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Played '+s+' time'+(s!=1?'s':'')}).append(
                            $('<span>', {class: 'fa fa-play-circle-o'}),
                            $('<span>', {class: 'wvpl-value'}).text(s)
                        )
                        break;
                    case 'cart':
                        s = track.product;
                        var cart = track.in_cart > 0 ? 'fa-shopping-cart wvpl-cart' : 'fa-cart-plus wvpl-cart-add';
                        var title = track.in_cart > 0 ? 'Already in the cart – Go to cart' : 'Add to cart';
                        if (s) {
                            $span = $('<span>', {class: 'wvpl-stats', title: title, 'data-product': s}).append(
                                $('<span>', {class: 'fa ' + cart})
                            )
                        } else {
                            $span = $('<span>');
                        }
                        break;
                    case 'runtime':
                        if ( ! track.stats ) break;
                        s = lengthFormatted(track.stats.runtime);
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Total runtime: '+s}).append(
                            $('<span>', {class: 'fa fa-hourglass-half'}),
                            $('<span>', {class: 'wvpl-value'}).text(s)
                        )
                        break;
                    case 'length_formatted':
                        s = track.length_formatted;
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Track length: '+s}).append(
                            // $('<span>', {class: 'fa fa-clock-o'}),
                            $('<span>', {class: 'wvpl-value'}).text(s)
                        )
                        break;
                    case 'likes':
                        if ( ! track.stats ) break;
                        s = track.stats.likes.length;
                        var id = track.id,
                            index = track.index,
                            msg = !loggedUser ? ' (users must log in to like tracks)' : '';
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Liked by '+s+' user' + ( s!=1?'s':'' ) + msg }).append(
                            $('<span>', {class: 'fa fa-heart wvpl-likes', 'data-id': id, 'data-index': index}).toggleClass('liked', track.liked).toggleClass('disabled', !loggedUser),
                            $('<span>', {class: 'wvpl-value'}).text(s)
                        )
                        break;
                    case 'downloads':
                        s = track.file != null ? track.file : ''
                        if ( ! track.stats ) break;
                        var count = track.stats.downloads != null ? track.stats.downloads : 0
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Downloaded by '+count+' user'+(count!=1?'s':'')}).append(
                            $('<a>', {href: s, download:'', class:'wvpl-downloads'}).attr('data-id', track.id).attr('data-index', track.index).append($('<span>', {class: 'fa fa-download'})),
                            $('<span>', {class: 'wvpl-value'}).text(count)
                        )
                        break;
                    case 'share_fb':
                    case 'share_gp':
                    case 'share_tw':
                    case 'share_ln':
                        s = track.post_url != null ? track.post_url : '';
                        var title = track.title,
                            social = key.replace('share_', '');
                        $span = $('<span>', {class: 'wvpl-stats', title: 'Share'}).append(
                            $('<span>', {class: 'fa fa-'+social+' wvpl-'+social+' wvpl-share', 'data-title': title, 'data-social': social, 'data-url': s})
                        )
                        break;
                    default:
                        $span = $('<span>').text(s);
                        break;
                }
                if ( !attributes.raw ) s = $span.prop('outerHTML');
                return s;
            });
            return template;
        }


        var displayPlaylist = function() {
            var list = $('<ul>')
            $.each(tracks, function(i,e){
                var item = $('<li>')
                item.append(
                    $('<img>', {class:'wvpl-playlist-thumbnail', src:e.thumbnail ? e.thumbnail : ''}),
                    $('<span>', {class:'wvpl-playlist-title'}).html(e.title),
                    $('<span>', {class:'wvpl-playlist-stats'}).html(infoTemplate(options.playlist_template, tracks[i]))
                )
                list.append(item)
            })
            playlistWrapper.append(list)
        }

        var lengthFormatted = function(position) {

            if (position == null) return '0:00'

            position = Math.round(position)

            var seconds = position % 60,
                minutes = Math.floor(position / 60) % 60,
                hours = Math.floor(position / 3600)

            return (hours > 0 ? hours + ":" : "") + (hours > 0 && minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds

        }

        var updateStatistics = function() {
            var nLocal = n,
                postData = [
                    {name: 'action', value: 'waveplayer_update_statistics'},
                    {name: 'id', value: tracks[nLocal].id},
                    {name: 'length', value: tracks[nLocal].length},
                    {name: 'runtime', value: runtime}
                ];
            runtime = 0;
            if (lastStart == 0) return false;
            $.ajax({
                url: waveplayer_ajax.ajax_url,
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function(result) {
                    if ( result.success ) {
                        tracks[nLocal].stats = result.data.stats
                    } else {
                        console.log('error', result.data.message)
                    }
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }

        var updateLikes = function($el) {
            if (!loggedUser) return false;
            var postData = [
                    {name: 'action', value: 'waveplayer_update_likes'},
                    {name: 'id', value: $el.data('id')},
                ];
            $.ajax({
                url: waveplayer_ajax.ajax_url,
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function(result) {
                    if ( result.success ) {
                        tracks[$el.data('index')].liked = result.data.liked
                        tracks[$el.data('index')].stats = result.data.stats
                        $el.toggleClass('liked', result.data.liked).closest('.wvpl-stats').find('.wvpl-value').text(tracks[$el.data('index')].stats.likes.length)
                    }
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }

        var addToCart = function($cart) {
            // if (!loggedUser) return false;
            var nLocal = n,
                product = $cart.parent().data('product'),
                postData = [
                    {name: 'action', value: 'waveplayer_add_to_cart'},
                    {name: 'product', value: product}
                ];
            $.ajax({
                url: waveplayer_ajax.ajax_url,
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function(result) {
                    if ( result.success ) {
                        $('[data-product='+product + '] span.fa').attr('title', "Already in cart – Go to cart").toggleClass('fa-cart-plus fa-shopping-cart wvpl-cart-add wvpl-cart').off('click').on('click',function(event){
                            goToCart();
                        });
                        $.ajax({
                            url: woocommerce_params.ajax_url,
                            data: {'action': 'woocommerce_get_refreshed_fragments'},
                            dataFormat: 'json',
                            type: 'post',
                            success: function(response) {
                                $.each(response.fragments, function(i,e){
                                    $(i).prop('outerHTML', e);
                                });
                                // $('div.woocommerce').html(result.data.message);
                            },
                            error: function(response) {
                                console.log(response.responseText);
                            }
                        });
                    } else {
                        console.log(result.data);
                    }
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }

        var goToCart = function() {
            window.location = options.cartURL;
        }

        var updateDownloads = function($el) {
            var postData = [
                    {name: 'action', value: 'waveplayer_update_downloads'},
                    {name: 'id', value: $el.data('id')}
                ];
            $.ajax({
                url: waveplayer_ajax.ajax_url,
                type: 'post',
                data: postData,
                dataType: 'json',
                success: function(result) {
                    if ( result.success ) {
                        tracks[$el.data('index')].stats = result.data.stats
                        $el.closest('.wvpl-stats').find('.wvpl-value').text(tracks[$el.data('index')].stats.downloads)
                    }
                },
                error: function(result) {
                    console.log(result.responseText);
                }
            });
        }

        var socialShare = function($el) {
            var sharer,
                url = location.protocol + encodeURIComponent($el.closest('span.wvpl-stats').data('url')),
                title = encodeURIComponent($el.closest('span.wvpl-stats').data('title')),
                site = encodeURIComponent(options.site);
            switch($el.data('social')){
                case 'fb':
                    sharer = 'https://www.facebook.com/sharer/sharer.php?display=popup&u=' + url;
                    break;
                case 'gp':
                    sharer = 'https://plus.google.com/share?url=' + url;
                    break;
                case 'tw':
                    sharer = 'https://twitter.com/intent/tweet?url=' + url;
                    break;
                case 'ln':
                    sharer = 'https://www.linkedin.com/shareArticle?mini=true&url=' + url + '&title=' + title + '&source=' + site;
                    break;
            }
            window.open( sharer, '' );
        }




        /**
        *
        * Bind the 'ended' event of the audio element to skip to the next track
        * and fires an 'ended' event
        *
        */
        audio.on('ended', function(event) {
            var track = tracks[n];
            elem.trigger('ended', {track: track});
            obj.skip();
        });

        audio.on('canplay', function(event) {
            $('.wvpl-volume-overlay').html(Math.round($(this)[0].volume * 100));
        });

        audio.on('play', function(event) {
            timerHook = setInterval(timeUpdate, INTERVAL);
            var track = tracks[n];
            elem.trigger('play', {track: tracks[n]});
        });

        audio.on('pause', function(event) {
            clearInterval(timerHook);
            status = 'pause';
            wavesurfer.drawer.playing = false;
            var track = tracks[n];
            elem.trigger('pause', {track: tracks[n]});
        });

        var scrolling = false;
        infoText.on('mouseenter', function(event) {
            if (1) return false;

            if ( status != 'play' ) return false;
            var el = $(this).find('.wvpl-infoblock'),
                scrollable = el.prop('scrollWidth') >= infoBar.width();
            if (scrollable && !scrolling) {
                scrolling = true;
                var elClone = el.clone().css({marginLeft:'1em'});
                $(this).append(elClone);
                var dV = elClone.outerWidth(true),
                    dT = 50 * dV;
                el.animate({left: -dV}, dT, 'linear', function() {
                        $(this).css({left: 0});
                        elClone.remove();
                        elem.trigger('titleScrollEnd', {track: tracks[n]});
                        scrolling = false;
                });
                elem.trigger('titleScrollStart', {track: tracks[n]});
                elClone.animate({left: -dV}, dT, 'linear');
            }
        });

        infoBtn.find('.wvpl-icon').click(function() {
            obj.toggleInfo();
        });

        playBtn.click(function() {
            obj.toggle();
        });

        prevBtn.click(function() {
            if (!$(this).hasClass('wvpl-disabled')) obj.skip(false);
        });

        nextBtn.click(function() {
            if (!$(this).hasClass('wvpl-disabled')) obj.skip();
        });

        elem.on('activated', function(event, data){
            $(this).toggleClass('active')
        })

        $(window).resize(function() {
            setTimeout(waveRedraw, 500);
        })

        $(document.body).off('click', '.wvpl-cart-add').on('click', '.wvpl-cart-add', function(event) {
            addToCart($(this))
        })
        $(document.body).off('click', '.wvpl-cart').on('click', '.wvpl-cart', function(event) {
            goToCart();
        })
        $(document.body).off('click', '.wvpl-share').on('click', '.wvpl-share', function(event) {
            socialShare($(this))
        })
        $(document.body).off('click', '.wvpl-likes').on('click', '.wvpl-likes', function(event) {
            updateLikes($(this))
        })
        $(document.body).off('click', '.wvpl-downloads').on('click', '.wvpl-downloads', function(event) {
            updateDownloads($(this))
        })

        /*
        *
        * Makes the volume button draggable (yet not moving it)
        * so that the user can alter the volume clicking on the volume button
        * and dragging horizontally
        *
        */

        var timerOverlay,
            startOffset,
            startVol;
        volumeBtn.find('.wvpl-icon').on('mousedown', function(e) {
            startOffset = e.clientX;
            startVol = obj.audio.volume;

            // The overlay displaying the level appears after the user holds the mouse for more than 150ms
            timerOverlay = setTimeout(function() {

                // Disables the default behavior of the click and drag gesture
                e.preventDefault();
                elem.find('.wvpl-volume-overlay').addClass('dragging');
                elem.find('.wvpl-controls').addClass('wvpl-inactive');
                $(window)
                    .on('mousemove', function(e) {
                        e.preventDefault();
                        volumeBtn.addClass('dragging');
                        var newVol = Math.round( 100 * ( startVol + ( e.clientX - startOffset ) / ( interface.width() * wavesurfer.params.pixelRatio / 4 ) ) ) / 100;
                        if ( newVol >= 0 && newVol <= 1 ) {
                            obj.audio.volume = newVol;
                            elem.find('.wvpl-volume-overlay').html(Math.round(newVol*100));
                            obj.audio.muted = (newVol == 0);
                            newVol == 0 ?
                                volumeBtn.find('.wvpl-icon').removeClass('fa-volume-up').addClass('fa-volume-off') :
                                volumeBtn.find('.wvpl-icon').removeClass('fa-volume-off').addClass('fa-volume-up');
                        }
                    }).on('mouseup', function(e) {
                        $(window).off('mousemove');
                        $('.dragging').removeClass('dragging');
                        elem.find('.wvpl-controls').removeClass('wvpl-inactive');
                    });
            }, 150);
        }).on('mouseup', function() {
            clearTimeout(timerOverlay);
            if ( !volumeBtn.hasClass('dragging') ) {
                volumeBtn.find('.wvpl-icon').toggleClass('fa-volume-off').toggleClass('fa-volume-up');
                obj.audio.muted = volumeBtn.find('.wvpl-icon').hasClass('fa-volume-off');
                elem.find('.wvpl-volume-overlay').html(Math.round(obj.audio.volume * 100))
            }
            $('.dragging').removeClass('dragging');
        });

    }

    $.fn.waveplayer = function(op, params) {
        return this.each(function() {
            var element = $(this),
                instance = null;

            if ( instance = element.data( 'waveplayer' ) ) {
                if ( 'object' !== typeof(params) ) params = [params];
                var thisOp = instance[op].apply(thisOp, params);
                return;
            }

            var waveplayer = new WavePlayer(this);

            element.data('waveplayer', waveplayer);

            waveplayer['init'](params);
        });
    };

    if ( $('.waveplayer').length ) {
        var listData = [];
        $('.waveplayer').each(function(i,e){
            $(this).find('.wvpl-play .wvpl-icon').addClass( 'fa-spin fa-refresh').removeClass('fa-play' );
            data = {ids: $(e).data('ids'), url: $(e).data('url')};
            listData.push(data);
        })

        $.ajax({
            url: waveplayer_ajax.ajax_url,
            type: 'post',
            data: [
                {name: 'action', value: 'waveplayer_load_page_playlist'},
                {name: 'type', value: 'playlist'},
                {name: 'list_data', value: JSON.stringify(listData)},
            ],
            dataType: 'json',
            success: function( result ) {
                options = result.data.options;
                players = result.data.players;
                loggedUser = result.data.user > 0;

				observer = new MutationObserver(function(mutations) {
					mutations.forEach(function(mutation) {
						var target = mutation.target;
						var waveplayer = $(mutation.target).find('.waveplayer');
						if ($(mutation.target).is(":visible") && waveplayer.length) waveplayer.waveplayer('refresh');
					});
				});

				$(options.ajax_containers).each(function(index, element){
					var config = { attributes: true, childList: true, characterData: true }
					observer.observe(element, config);
				});

                $( $('.waveplayer')[0] ).waveplayer()

            },
            error: function(response) {
                console.log( response.responseText )
            }
        });

    }

    $(document.body).off('click', '.woocommerce-music-type span.woocommerce-option:not(.active)').on('click', '.woocommerce-music-type span.woocommerce-option:not(.active)', function(event){
        $('.woocommerce-music-type span.woocommerce-option').toggleClass('active');
        var type = $(this).data('type');
        $('input[name=music_type]').val(type);
        $('input[name=page]').val(1);
        $(this).closest('form').submit();
    });

	$(document.body).off('click', '.wvpl-playlist-title, .wvpl-playlist-thumbnail').on('click', '.wvpl-playlist-title, .wvpl-playlist-thumbnail', function(event) {
		var index = $(this).closest('li').index();
		var $instance = $(this).closest('.waveplayer');
		$instance.waveplayer('skipTo', index);
	})

})(jQuery);
