/**
*
* WavePlayer 2.0.12
*
*
*/

(function ($) {

    var frame;

    $.extend( wp.Uploader.prototype, {
        success : function( file_attachment ) {
            var start = new Date(),
                url = file_attachment.attributes.url,
                id = file_attachment.attributes.id,
                isAudio = !!file_attachment.attributes.mime.match(/^audio\/.*$/);

            if ( isAudio ) {
                var wvplWS = _.extend({}, WaveSurfer);
                wvplWS.init({container: $('<div>')[0]});
                wvplWS.on('ready', function(event){
                    var peaks = wvplWS.backend.getPeaks(960);
                    var postData = [
                        {name: 'action', value: 'waveplayer_write_peaks'},
                        {name: 'peaks', value: peaks},
                        {name: 'id', value: id}
                    ];
                    $.ajax({
                        url: waveplayer_ajax.ajax_url,
                        type: 'post',
                        data: postData,
                        dataType: 'json',
                        success: function(result) {
                            if ( result.success ) {
                                var stop = new Date();
                                console.log( "Peak file created in " + (stop-start)/1000 + " seconds!" );
                            }
                        },
                        error: function(result) {
                            console.log(result.responseText);
                        }
                    });
                })
                wvplWS.load(url);
            }
        }
    });


    $(document.body).on('click', '.media-frame a.play-button', function(event){
        event.preventDefault();
        var id = $(this).data('id'),
            audio = $('.media-frame audio#' + id )[0];
            $('.media-frame audio[id!='+id+']').each(function(i,e){
                $(e)[0].pause();
                $(e)[0].currentTime = 0;
            })
        audio.paused ? audio.play() : audio.pause();
        event.stopPropagation();
        return false;
    });

    $(document.body).on('click', '.woocommerce-music-type .woocommerce-option', function(event){
        console.log($(this).data('type'));
    });

    $('.waveplayer_tab').click(function(){
        $('.waveplayer_tab').removeClass('nav-tab-active');
        $(this).addClass('nav-tab-active');
        $('.waveplayer-option-page:visible').hide();
        $('#'+$(this).attr('id').replace('-tab','')).show();
        $('#waveplayer_current_tab').val($(this).attr('id').replace('-tab',''));
    });

    $('#waveplayer_default_thumbnail_preview').click(function(event) {
        event.preventDefault();

        if ( frame ) {
          frame.open();
          return;
        }

        frame = wp.media({
            title: 'Select or Upload the default thumbnail',
            button: {
            text: 'Use this image'
            },
            library : {
                type : 'image',
            },
            multiple: false,
        });

        frame.on( 'select', function() {

          var attachment = frame.state().get('selection').first().toJSON();

          $('#waveplayer_default_thumbnail_preview').css( {backgroundImage: 'url(' + attachment.url + ')'} );
          $('input[name=waveplayer_default_thumbnail]').val(attachment.url);
        });

        // Finally, open the modal on click
        frame.open();
    });

    $('.waveplayer-color-picker').wpColorPicker({
        change: function(event, ui){
            $(event.target).val(ui.color.toString());
            $(event.target).attr('name') == 'background_color' ?
                changeBackground(ui.color.toString()) :
                redraw();
        }
    });

    $('select[name=waveplayer_wave_mode]').change(function() {
        $(this).val() > 0 ?
            $('#waveplayer_gap_width_group').removeClass('wvpl-inactive') :
            $('#waveplayer_gap_width_group').addClass('wvpl-inactive');
    });

    var changeBackground = function(color) {
        $('#wvpl-sample-waveform').css('background-color', color);
    }

    $('.wvpl_delete_peaks').click(function(){
        event.preventDefault();
        var el = $(this);
        $.ajax({
            url: waveplayer_ajax.ajax_url,
            type: 'post',
            data: [ { name: 'action', value: 'waveplayer_delete_peaks' }, { name: 'mode', value: $(this).attr('data-elements') } ],
            dataType: 'json',
            success: function(result) {
                var status = result.success ? 'notice updated is-dismissible' : 'notice error is-dismissible';
                el.parent().append($('<div>', {class: status}).html($('<p>').html(result.data.message)));
            },
            error: function(response) {
                console.log(response.responseText);
            }
        });
    });

    // File inputs.
	$( '#woocommerce-product-data' ).on( 'click','.preview_files a.insert', function() {
		$( this ).closest( '.preview_files' ).find( 'tbody' ).append( $( this ).data( 'row' ) );
		return false;
	});
	$( '#woocommerce-product-data' ).on( 'click','.preview_files a.delete',function() {
		$( this ).closest( 'tr' ).remove();
		return false;
	});
    // Previews ordering.
	$( '.preview_files tbody' ).sortable({
		items: 'tr',
		cursor: 'move',
		axis: 'y',
		handle: 'td.sort',
		scrollSensitivity: 40,
		forcePlaceholderSize: true,
		helper: 'clone',
		opacity: 0.65
	});

    // Uploading files.
    var preview_file_frame;
    var file_path_field;

    $( document.body ).on( 'click', '.upload_preview_button', function( event ) {
        var $el = $( this );

        file_path_field = $el.closest( 'tr' ).find( 'td.file_url input' );

        event.preventDefault();

        // If the media frame already exists, reopen it.
        if ( preview_file_frame ) {
            preview_file_frame.open();
            return;
        }

        var preview_file_states = [
            // Main states.
            new wp.media.controller.Library({
                library:   wp.media.query(),
                multiple:  true,
                title:     $el.data('choose'),
                priority:  20,
                filterable: 'uploaded'
            })
        ];

        // Create the media frame.
        preview_file_frame = wp.media.frames.preview_file = wp.media({
            // Set the title of the modal.
            title: $el.data('choose'),
            library: {
                type: ''
            },
            button: {
                text: $el.data('update')
            },
            multiple: true,
            states: preview_file_states
        });

        // When an image is selected, run a callback.
        preview_file_frame.on( 'select', function() {
            var file_path = '';
            var selection = preview_file_frame.state().get( 'selection' );

            selection.map( function( attachment ) {
                attachment = attachment.toJSON();
                if ( attachment.url ) {
                    file_path = attachment.url;
                }
            });

            file_path_field.val( file_path ).change();
        });

        // Set post to 0 and set our custom type.
        preview_file_frame.on( 'ready', function() {
            preview_file_frame.uploader.options.uploader.params = {
                type: 'download_preview'
            };
        });

        // Finally, open the modal.
        preview_file_frame.open();
    });



    if ( $('#wvpl-sample-waveform').length ) {

        var sampleWaveform = _.extend({}, WaveSurfer);

        sampleWaveform.init({
            container:          '#wvpl-sample-waveform',
            waveColor:          $('[name=waveplayer_wave_color]').val(),
            waveColor2:         $('[name=waveplayer_wave_color_2]').val(),
            progressColor:      $('[name=waveplayer_progress_color]').val(),
            progressColor2:     $('[name=waveplayer_progress_color_2]').val(),
            cursorColor:        $('[name=waveplayer_cursor_color]').val(),
            cursorColor2:       $('[name=waveplayer_cursor_color_2]').val(),
            cursorWidth:        $('[name=waveplayer_cursor_width]').val(),
            hoverOpacity:       parseInt($('[name=waveplayer_hover_opacity]').val()),
            waveMode:           parseInt($('[name=waveplayer_wave_mode]').val()),
            gapWidth:           parseInt($('[name=waveplayer_gap_width]').val()),
            compression:        parseInt($('[name=waveplayer_wave_compression]').val()),
            asymmetry:          parseInt($('[name=waveplayer_wave_asymmetry]').val()),
            height:             $('#wvpl-sample-waveform').height() - parseInt($('#wvpl-sample-waveform').css("marginTop")) + parseInt($('#wvpl-sample-waveform').css("marginBottom")),
            normalize:          true,
            reflection:         true,
            hideScrollbar:      true,
        });

        var peaks;
        var loadPeaks = function() {
            var waveform = $('.wvpl-waveform');

            sampleWaveform.drawer.playing = true;

            $.ajax({
                url: waveplayer_ajax.ajax_url,
                type: 'post',
                data: [ {name: 'action', value: 'waveplayer_read_peaks'}, {name: 'id', value: 'sample'} ],
                dataType: 'json',
                success: function(result) {
                    if (result.success) {
                        peaks = result.data;
                        redraw();
                    }
                },
            });
        }

        var redraw = function() {
            var params = sampleWaveform.params;

            params.waveColor =          $('[name=waveplayer_wave_color]').val();
            params.waveColor2 =         $('[name=waveplayer_wave_color_2]').val();
            params.progressColor =      $('[name=waveplayer_progress_color]').val();
            params.progressColor2 =     $('[name=waveplayer_progress_color_2]').val();
            params.cursorColor =        $('[name=waveplayer_cursor_color]').val();
            params.cursorColor2 =       $('[name=waveplayer_cursor_color_2]').val();
            params.cursorWidth =        $('[name=waveplayer_cursor_width]').val();
            params.hoverOpacity =       parseInt($('[name=waveplayer_hover_opacity]').val());
            params.waveMode =           parseInt($('[name=waveplayer_wave_mode]').val());
            params.gapWidth =           parseInt($('[name=waveplayer_gap_width]').val());
            params.compression =        parseInt($('[name=waveplayer_wave_compression]').val());
            params.asymmetry =          parseInt($('[name=waveplayer_wave_asymmetry]').val());

            sampleWaveform.drawer.clearWave();
            sampleWaveform.drawer.drawPeaks(peaks, $('#wvpl-sample-waveform').width() * sampleWaveform.params.pixelRatio);

            sampleWaveform.drawer.progress = 0.333;
            sampleWaveform.drawer.updatePosition(0.667);

        }

        $('input[name^="waveplayer"], select[name^="waveplayer"]').change(function() {
            redraw();
        });

        $( '.wvpl_toggle_selection' ).on('click', function(event){
            event.preventDefault();
            var select = $(this).data('mode') == 'select',
                type = $(this).data('type');
            $('#waveplayer_music_'+type+' input:enabled').each(function(i,e){
                $(e).prop('checked', select);
            });
        })

        $('.wvpl_create_products').on('click', function(event){
            event.preventDefault();

            var type = $(this).data('type');
            var index = 0,
                itemsCount = $('#waveplayer_music_' + type + ' input:checked').length,
                progress = $('.wvpl_products_progress_'+type);
            progress.val(index).prop('max', itemsCount).show();

            $('#waveplayer_music_'+type+' input:checked').each(function(i,e){
                $.ajax({
                    url: waveplayer_ajax.ajax_url,
                    type: 'post',
                    data: [
                        { name: 'action',   value: 'waveplayer_create_product' },
                        { name: 'type',     value: type },
                        { name: 'id',       value: $(e).val() },
                        { name: 'price',    value: $('#waveplayer_woocommerce_price_'+type).val() },
                        { name: 'tracks',   value: $(e).data('tracks') },
                        { name: 'title',    value: $(e).data('title') },
                    ],
                    dataType: 'json',
                    success: function(result) {
                        index++;
                        progress.val(index);
                        $(e).prop('checked', false).prop('disabled', true).next().toggleClass('disabled', true);
                        if ( index == itemsCount ) {
                            progress.parent().append($('<div>', {class: 'notice updated is-dismissible'}).html($('<p>').html(itemsCount + ' products successfully created')));
                        }
                    },
                    error: function(response) {
                        console.log(response.responseText);
                    }
                });
            })
        });

        loadPeaks();

    }

})(jQuery);
