/**
 * wavesurfer.js
 *
 * https://github.com/katspaugh/wavesurfer.js
 *
 * This work is licensed under a Creative Commons Attribution 3.0 Unported License.
 */

'use strict'

var WaveSurfer = {
    defaultParams: {
        height              : 128,
        waveColor           : '#999',
        waveColor2          : '#999',
        progressColor       : '#555',
        progressColor2      : '#555',
        cursorColor         : '#333',
        cursorColor2        : '#333',
        hoverOpacity        : 50,
        cursorWidth         : 2,
        waveMode            : 0,
        gapWidth            : 1,
        minPxPerSec         : 20,
        pixelRatio          : window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI,
        normalize           : false,
        reflection          : false,
        compression         : 1,
        asymmetry           : 2,
        audioContext        : null,
        container           : null,
        backend             : 'WebAudio',
        mediaType           : 'audio',
    },

    init: function (params) {
        this.params = _.extend({}, this.defaultParams, params)

        this.container = 'string' == typeof params.container ?
            document.querySelector(this.params.container) :
            this.params.container

        if (!this.container) {
            throw new Error('Container element not found')
        }

        this.tmpEvents = []

        this.createDrawer()
        this.createBackend()
    },

    createDrawer: function () {
        var my = this

        this.drawer = _.extend({}, WaveSurfer.Drawer)
        this.drawer.init(this.container, this.params)

        this.drawer.on('redraw', function () {
            my.drawBuffer()
            my.drawer.progress(my.backend.getPlayedPercents())
        })

    },

    createBackend: function () {
        var my = this

        if (this.backend) {
            this.backend.destroy()
        }

        // Back compat
        if (this.params.backend == 'AudioElement') {
            this.params.backend = 'MediaElement';
        }

        if (this.params.backend == 'WebAudio' && !WaveSurfer.WebAudio.supportsWebAudio()) {
            this.params.backend = 'MediaElement';
        }

        this.backend = _.extend({}, WaveSurfer[this.params.backend]);
        this.backend.init(this.params);

        this.backend.on('finish', function () { my.fireEvent('finish') })
        this.backend.on('play', function () { my.fireEvent('play') })
        this.backend.on('pause', function () { my.fireEvent('pause') })

        this.backend.on('audioprocess', function (time) {
            my.drawer.progress(my.backend.getPlayedPercents())
            my.fireEvent('audioprocess', time)
        })
    },

    updateDrawer: function() {
        this.drawer.updateSize()
    },

    getDuration: function () {
        return this.backend.getDuration()
    },

    drawBuffer: function () {
        var nominalWidth = Math.round(
            this.getDuration() * this.params.minPxPerSec * this.params.pixelRatio
        )
        var width = this.drawer.getWidth()

        var peaks = this.backend.getPeaks(width)
        this.drawer.drawPeaks(peaks, width)
        this.fireEvent('redraw', peaks, width)
    },

    loadArrayBuffer: function (arraybuffer) {
        this.decodeArrayBuffer(arraybuffer, function (data) {
            this.loadDecodedBuffer(data)
        }.bind(this))
    },

    loadDecodedBuffer: function (buffer) {
        this.backend.load(buffer)
        this.drawBuffer()
        this.fireEvent('ready')
    },

    loadBlob: function (blob) {
        var my = this
        var reader = new FileReader()
        reader.addEventListener('progress', function (e) {
            my.onProgress(e)
        })
        reader.addEventListener('load', function (e) {
            my.loadArrayBuffer(e.target.result)
        })
        reader.addEventListener('error', function () {
            my.fireEvent('error', 'Error reading file')
        })
        reader.readAsArrayBuffer(blob)
        this.empty()
    },

    load: function (url) {
        switch (this.params.backend) {
            case 'WebAudio': return this.loadBuffer(url);
            case 'MediaElement': return this.loadMediaElement(url, peaks);
        }
    },

    loadBuffer: function (url) {
        this.empty()
        return this.getArrayBuffer(url, this.loadArrayBuffer.bind(this))
    },

    loadMediaElement: function (url, peaks) {
        this.empty();
        this.backend.load(url, this.mediaContainer, peaks);

        this.tmpEvents.push(
            this.backend.once('canplay', (function () {
                this.drawBuffer();
                this.fireEvent('ready');
            }).bind(this)),

            this.backend.once('error', (function (err) {
                this.fireEvent('error', err);
            }).bind(this))
        );


        // If no pre-decoded peaks provided, attempt to download the
        // audio file and decode it with Web Audio.
        if (!peaks && this.backend.supportsWebAudio()) {
            this.getArrayBuffer(url, (function (arraybuffer) {
                this.decodeArrayBuffer(arraybuffer, (function (buffer) {
                    this.backend.buffer = buffer;
                    this.drawBuffer();
                }).bind(this));
            }).bind(this));
        }
    },

    decodeArrayBuffer: function (arraybuffer, callback) {
        this.backend.decodeArrayBuffer(
            arraybuffer,
            this.fireEvent.bind(this, 'decoded'),
            this.fireEvent.bind(this, 'error', 'Error decoding audiobuffer')
        )
        this.tmpEvents.push(
            this.once('decoded', callback)
        )
    },

    getArrayBuffer: function (url, callback) {
        var my = this
        var ajax = this.ajax({
            url: url,
            responseType: 'arraybuffer'
        })
        this.tmpEvents.push(
            ajax.on('progress', function (e) {
                my.onProgress(e)
            }),
            ajax.on('success', callback),
            ajax.on('error', function (e) {
                my.fireEvent('error', 'XHR error: ' + e.target.statusText)
            })
        )
        return ajax
    },

    onProgress: function (e) {
        if (e.lengthComputable) {
            var percentComplete = e.loaded / e.total
        } else {
            percentComplete = e.loaded / (e.loaded + 1000000)
        }
        this.fireEvent('loading', Math.round(percentComplete * 100), e.target)
    },

    clearTmpEvents: function () {
        this.tmpEvents.forEach(function (e) { e.un() })
    },

    ajax: function (options) {
        var ajax = _.extend({}, WaveSurfer.Observer)
        var xhr = new XMLHttpRequest()
        var fired100 = false

        xhr.open(options.method || 'GET', options.url, true)
        xhr.responseType = options.responseType || 'json'

        xhr.addEventListener('progress', function (e) {
            ajax.fireEvent('progress', e)
            if (e.lengthComputable && e.loaded == e.total) {
                fired100 = true
            }
        })

        xhr.addEventListener('load', function (e) {
            if (!fired100) {
                ajax.fireEvent('progress', e)
            }
            ajax.fireEvent('load', e)

            if (200 == xhr.status || 206 == xhr.status) {
                ajax.fireEvent('success', xhr.response, e)
            } else {
                ajax.fireEvent('error', e)
            }
        })

        xhr.addEventListener('error', function (e) {
            ajax.fireEvent('error', e)
        })

        xhr.send()
        ajax.xhr = xhr
        return ajax
    },

    empty: function () {
        this.backend.disconnectSource()
        this.clearTmpEvents()
        this.drawer.clearWave()
    },

    destroy: function () {
        this.fireEvent('destroy')
        this.clearTmpEvents()
        this.unAll()
        this.backend.destroy()
        this.drawer.destroy()
    }
}

WaveSurfer.create = function (params) {
    var wavesurfer = _.extend({}, WaveSurfer)
    wavesurfer.init(params)
    return wavesurfer
}

WaveSurfer.Observer = {
    on: function (event, fn) {
        if (!this.handlers) { this.handlers = {} }

        var handlers = this.handlers[event]
        if (!handlers) {
            handlers = this.handlers[event] = []
        }
        handlers.push(fn)

        return {
            name: event,
            callback: fn,
            un: this.un.bind(this, event, fn)
        }
    },

    un: function (event, fn) {
        if (!this.handlers) { return }

        var handlers = this.handlers[event]
        if (handlers) {
            if (fn) {
                for (var i = handlers.length - 1; i >= 0; i--) {
                    if (handlers[i] == fn) {
                        handlers.splice(i, 1)
                    }
                }
            } else {
                handlers.length = 0
            }
        }
    },

    unAll: function () {
        this.handlers = null
    },

    once: function (event, handler) {
        var my = this
        var fn = function () {
            handler.apply(this, arguments)
            setTimeout(function () {
                my.un(event, fn)
            }, 0)
        }
        return this.on(event, fn)
    },

    fireEvent: function (event) {
        if (!this.handlers) { return }
        var handlers = this.handlers[event]
        var args = Array.prototype.slice.call(arguments, 1)
        handlers && handlers.forEach(function (fn) {
            fn.apply(null, args)
        })
    }
}

_.extend(WaveSurfer, WaveSurfer.Observer)


WaveSurfer.Drawer = {
    init: function (container, params) {
        this.container = container
        this.params = params
        this.progress = 0
        this.position = -1
        this.playing = false

        this.lastPos = 0
        this.createWrapper()
        this.createElements()
        this.centerLine = this.container.clientHeight / ( 1 + 1 / this.params.asymmetry )
    },

    createWrapper: function () {
        this.wrapper = this.container
        this.setupWrapperEvents()
    },

    handleEvent: function (e) {
        e.preventDefault()
        var bbox = this.wrapper.getBoundingClientRect()
        return ((e.clientX - bbox.left + this.wrapper.scrollLeft) / this.wrapper.scrollWidth) || 0
    },

    setupWrapperEvents: function () {
        var my = this

        this.wrapper.addEventListener('click', function a(e) {
            var scrollbarHeight = my.wrapper.height - my.wrapper.clientHeight
            if (scrollbarHeight != 0) {
                // scrollbar is visible.  Check if click was on it
                var bbox = my.wrapper.getBoundingClientRect()
                if (e.clientY >= bbox.bottom - scrollbarHeight) {
                    // ignore mousedown as it was on the scrollbar
                    return
                }
            }

            // my.updateProgress(e.offsetX / my.width)
            e.isCommentArea = e.offsetY > my.centerLine

            my.fireEvent( 'click', e, my.handleEvent(e) )
        })

        this.wrapper.addEventListener('mousemove', function a(e) {
            e.isCommentArea = e.offsetY > my.centerLine
            my.fireEvent('mousemove', e)
        })

        this.wrapper.addEventListener('mouseover', function a(e) {
            e.isCommentArea = e.offsetY > my.centerLine
            my.fireEvent( 'mouseover', e )
        })

        this.wrapper.addEventListener('mouseout', function a(e) {
            e.isCommentArea = e.offsetY > my.centerLine
            my.fireEvent( 'mouseout', e )
        })

        this.wrapper.addEventListener('scroll', function a(e) {
            my.fireEvent('scroll', e)
        })
    },

    drawPeaks: function (peaks, width) {
        this.resetScroll()
        this.setWidth(width)

        this.params.waveMode ?
            this.drawBars(peaks) :
            this.drawWave(peaks)
    },

    style: function (el, styles) {
        Object.keys(styles).forEach(function (prop) {
            if (el.style[prop] !== styles[prop]) {
                el.style[prop] = styles[prop]
            }
        })
        return el
    },

    resetScroll: function () {
        if (this.wrapper !== null) {
            this.wrapper.scrollLeft = 0
        }
    },

    getWidth: function () {
        return Math.round(this.container.clientWidth * this.params.pixelRatio)
    },

    setWidth: function (width) {
        if (width == this.width) { return }

        this.width = width

        this.updateSize()
    },

    setHeight: function (height) {
        if (height == this.height) { return }
        this.height = height
        this.style(this.wrapper, {
            height: ~~(this.height / this.params.pixelRatio) + 'px'
        })
        this.updateSize()
    },

    destroy: function () {
        this.unAll()
        if (this.wrapper) {
            this.container.removeChild(this.canvas)
            this.wrapper = null
        }
    },

    createElements: function () {

        this.canvas = this.wrapper.appendChild(
            this.style(document.createElement('canvas'), {
                height: '100%',
                width: '100%',
            })
        )
        this.centerLine = this.canvas.height / ( 1 + 1 / this.params.asymmetry)
        this.canvasCc = this.canvas.getContext('2d')

        var wave = document.createElement('canvas')
        this.waveCc = wave.getContext('2d')

        var progress = document.createElement('canvas')
        this.progressCc = progress.getContext('2d')

        var position = document.createElement('canvas')
        this.positionCc = position.getContext('2d')

    },

    updateSize: function () {
        this.height = this.params.height * this.params.pixelRatio
        this.canvasCc.canvas.width = this.waveCc.canvas.width = this.progressCc.canvas.width = this.positionCc.canvas.width = this.width
        this.canvasCc.canvas.height = this.waveCc.canvas.height = this.progressCc.canvas.height = this.positionCc.canvas.height = this.height
        this.centerLine = this.container.clientHeight / ( 1 + 1 / this.params.asymmetry )
    },

    clearWave: function () {
        this.progress = 0
        this.position = -1
        this.canvasCc.clearRect(0, 0, this.width, this.height)
        this.waveCc.clearRect(0, 0, this.width, this.height)
        this.progressCc.clearRect(0, 0, this.width, this.height)
        this.positionCc.clearRect(0, 0, this.width, this.height)
    },

    drawBars: function (peaks) {
        this.peaks = peaks

        if (typeof(peaks) == 'undefined') return false

        var width = this.width,
            height = this.params.height * this.params.pixelRatio,
            length = ~~(peaks.length / 2),
            bar = this.params.waveMode * this.params.pixelRatio,
            gap = this.params.gapWidth * this.params.pixelRatio, //Math.max(this.params.pixelRatio, ~~(bar / 2)),
            cl = this.params.pixelRatio,
            step = bar + gap,
            a = this.params.asymmetry,
            c = this.params.compression,
            scale = width / length,
            halfH = height / (1 + 1/a)

        var absmax = 1
        if (this.params.normalize) {
            var min, max
            max = Math.max.apply(Math, peaks)
            min = Math.min.apply(Math, peaks)
            absmax = max
            if (-min > absmax) {
                absmax = -min
            }
        }

        var waves = []
        waves.push({cc: this.waveCc, color1: this.params.waveColor, color2: this.params.waveColor2, alpha: 1 })
        waves.push({cc: this.progressCc, color1: this.params.progressColor, color2: this.params.progressColor2, alpha: 1 })
        waves.push({cc: this.positionCc, color1: this.params.progressColor, color2: this.params.progressColor2, alpha: this.params.hoverOpacity/100})

        waves.forEach(function (el) {

            var cc = el.cc,
                halfH = el.cc.canvas.height / ( 1 + 1 / this.params.asymmetry)
            if (!cc) { return }

            var grd = this.waveCc.createLinearGradient(0, 0, 0, halfH)
            grd.addColorStop(0, el.color1)
            grd.addColorStop(1, typeof(el.color2) != 'undefined' ? el.color2 : el.color1)
            cc.fillStyle = grd
            cc.globalAlpha = el.alpha

            for (var i = 0; i < width; i += step) {
                var peak = peaks[Math.floor(2 * i / scale)]
                peak = Math.pow(Math.abs(peak), 1/c)
                var h = halfH * peak / absmax
                h = Math.max(this.params.pixelRatio, Math.abs(h))
                cc.fillRect(i, halfH - h - cl/2, bar, h)
                cc.save()
                cc.fillStyle = el.color1
                cc.fillRect(i, halfH + cl/2, bar, h * 1/a)
                cc.restore()
            }
        }, this)
        if ( this.waveCc.canvas.width ) this.canvasCc.drawImage(this.waveCc.canvas, 0, 0)
        this.fireEvent('waveComplete')
    },

    drawWave: function (peaks, channelIndex) {
        if (typeof(peaks) == 'undefined') return false

        this.peaks = peaks

        var width = this.width,
            height = this.params.height * this.params.pixelRatio,
            length = ~~(peaks.length / 2),
            cl = this.params.pixelRatio,
            a = this.params.asymmetry,
            c = this.params.compression,
            scale = width / length,
            halfH = height / (1 + 1/a)

        var absmax = 1
        if (this.params.normalize) {
            var min, max
            max = Math.max.apply(Math, peaks)
            min = Math.min.apply(Math, peaks)
            absmax = max
            if (-min > absmax) {
                absmax = -min
            }
        }

        var waves = []
        waves.push({cc: this.waveCc, color1: this.params.waveColor, color2: this.params.waveColor2, alpha: 1 })
        waves.push({cc: this.progressCc, color1: this.params.progressColor, color2: this.params.progressColor2, alpha: 1 })
        waves.push({cc: this.positionCc, color1: this.params.progressColor, color2: this.params.progressColor2, alpha: this.params.hoverOpacity/100})

        waves.forEach(function (el) {
            if ( ! el.cc.canvas.height ) return
            var cc = el.cc,
                halfH = el.cc.canvas.height / ( 1 + 1 / this.params.asymmetry)
            if (!cc) { return }

            var grd = el.cc.createLinearGradient(0, 0, 0, halfH)
            grd.addColorStop(0, el.color1)
            grd.addColorStop(1, typeof(el.color2) != 'undefined' ? el.color2 : el.color1)
            cc.fillStyle = grd
            cc.globalAlpha = el.alpha

            if (this.params.reflection) {
                cc.beginPath()
                cc.moveTo(0, halfH)
                for (var i = 0; i < length; i++) {
                    var peak = peaks[2 * i]
                    peak = Math.pow(Math.abs(peak), 1/c)
                    var h = peak / absmax * halfH
                    cc.lineTo(i * scale, halfH - h)
                }
                cc.lineTo(el.cc.canvas.width, halfH)
                cc.closePath()
                cc.fill()
                cc.fillStyle = el.color1
                cc.beginPath()
                cc.moveTo(el.cc.canvas.width, halfH)
                for (var i = length - 1; i >= 0; i--) {
                    var peak = peaks[2 * i]
                    peak = Math.pow(Math.abs(peak), 1/c)
                    var h = peak / absmax * halfH
                    cc.lineTo(i * scale, halfH + h * 1/a)
                }
                cc.lineTo(0, halfH)
                cc.closePath()
                cc.fill()
            } else {
                cc.beginPath()
                cc.moveTo(0, halfH)
                for (var i = 0; i < length; i++) {
                    var peak = peaks[2 * i]
                    peak = Math.pow(Math.abs(peak), 1/c)
                    var h = peak / absmax * halfH
                    cc.lineTo(i * scale, halfH - h)
                }
                cc.closePath()
                cc.fill()
                cc.fillStyle = el.color1
                cc.beginPath()
                cc.moveTo(el.cc.canvas.width, halfH)
                for (var i = length - 1; i >= 0; i--) {
                    var peak = peaks[2 * i + 1]
                    peak = Math.pow(Math.abs(peak), 1/c)
                    var h = peak / absmax * halfH
                    cc.lineTo(i * scale, halfH + h * 1/a)
                }
                cc.closePath()
                cc.fill()
            }

        }, this)
        this.canvasCc.drawImage(this.waveCc.canvas, 0, 0)
        this.fireEvent('waveComplete')
    },

    updateProgress: function (progress) {
        this.progress = progress
        var prog = Math.round(this.width * this.progress)
        var pos = Math.round(this.width * this.position)
        this.redrawContexts(prog, pos)
    },

    updatePosition: function (position) {
        var prog = Math.round(this.width * this.progress)
        this.position = position
        var pos = Math.round(this.width * position)
        this.redrawContexts(prog, pos)
    },

    redrawContexts: function(prog, pos) {
        var self = this

        var redraw = function(s1, e1, s2, e2) {
            cc.save()
            cc.beginPath()
            cc.rect(s1, 0, e1, self.height)
            cc.clip()
            if ( self.progressCc.canvas.width ) cc.drawImage(self.progressCc.canvas, 0, 0)
            cc.restore()
            cc.save()
            cc.beginPath()
            cc.rect(s2, 0, e2, self.height)
            cc.clip()
            if ( self.positionCc.canvas.width ) cc.drawImage(self.positionCc.canvas, 0, 0)
            cc.restore()

        }

        var cc = this.canvasCc

        cc.clearRect(0, 0, this.width, this.height)
        if ( this.waveCc.canvas.width ) cc.drawImage(this.waveCc.canvas, 0, 0)
        if ( pos < 0 ) {
            redraw(0, prog, 0, 0)
        } else {
            if (prog > pos) {
                redraw(0, pos, pos, prog - pos)
            } else {
                redraw(0, prog, prog, pos - prog)
            }
        }
        this.drawCursor(prog)
    },

    drawCursor: function(prog) {
        if (this.playing) {
            var halfH = ( this.params.height * this.params.pixelRatio ) / (1 + 1 / this.params.asymmetry)
            var t = this.params.cursorWidth * this.params.pixelRatio
            var cc = this.canvasCc
            var grd = this.waveCc.createLinearGradient(0, 0, 0, this.height)
            grd.addColorStop(0, this.params.cursorColor)
            grd.addColorStop(1, typeof(this.params.cursorColor2) != 'undefined' ? this.params.cursorColor2 : this.params.cursorColor)
            cc.fillStyle = grd
            cc.fillRect(prog - t/2, 0, t, this.height)
        }
    },

    colorLuminance: function(hex, lum) {

        hex = String(hex).replace(/[^0-9a-f]/gi, '')
        if (hex.length < 6) {
            hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2]
        }
        lum = lum || 0

        var rgb = "#", c, i
        for (i = 0; i < 3; i++) {
            c = parseInt(hex.substr(i*2,2), 16)
            c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16)
            rgb += ("00"+c).substr(c.length)
        }

        return rgb
    }
}

_.extend(WaveSurfer.Drawer, WaveSurfer.Observer)



WaveSurfer.WebAudio = {
    scriptBufferSize: 256,

    supportsWebAudio: function () {
        return !!(window.AudioContext || window.webkitAudioContext)
    },

    getAudioContext: function () {
        // if (!WaveSurfer.WebAudio.audioContext) {
        //     WaveSurfer.WebAudio.audioContext = new (
        //         window.AudioContext || window.webkitAudioContext
        //     );
        // }
        // return WaveSurfer.WebAudio.audioContext;
        if (!document.wvplAudioContext) {
            var AudioContext = window.AudioContext || window.webkitAudioContext
            document.wvplAudioContext = new AudioContext
        }
        return document.wvplAudioContext
    },

    getOfflineAudioContext: function (sampleRate) {
        if (!this.offlineAudioContext) {
            this.offlineAudioContext = new (
                window.OfflineAudioContext || window.webkitOfflineAudioContext
            )(1, 2, sampleRate)
        }
        return this.offlineAudioContext
    },

    init: function (params) {
        this.params = params
        // if (!document.wvplAudioContext && this.supportsWebAudio()) {
        //     var AudioContext = window.AudioContext || window.webkitAudioContext
        //     document.wvplAudioContext = new AudioContext
        // }
        // this.ac = document.wvplAudioContext
        this.ac = params.audioContext || this.getAudioContext();

        this.lastPlay = this.ac.currentTime
        this.startPosition = 0
        this.scheduledPause = null

        this.createScriptNode()
        this.createAnalyserNode()

    },

    createScriptNode: function () {
        if (this.ac.createScriptProcessor) {
            this.scriptNode = this.ac.createScriptProcessor(this.scriptBufferSize)
        } else {
            this.scriptNode = this.ac.createJavaScriptNode(this.scriptBufferSize)
        }

        this.scriptNode.connect(this.ac.destination)
    },

    createAnalyserNode: function () {
        this.analyser = this.ac.createAnalyser()
    },

    decodeArrayBuffer: function (arraybuffer, callback, errback) {
        if (!this.offlineAc) {
            this.offlineAc = this.getOfflineAudioContext(this.ac ? this.ac.sampleRate : 44100)
        }
        this.offlineAc.decodeAudioData(arraybuffer, (function (data) {
            callback(data)
        }).bind(this), errback)
    },

    getPeaks: function (length) {
        var sampleSize = this.buffer.length / length
        var sampleStep = ~~(sampleSize / 10) || 1
        var channels = this.buffer.numberOfChannels
        var splitPeaks = []
        var mergedPeaks = []

        for (var c = 0; c < channels; c++) {
            var peaks = splitPeaks[c] = []
            var chan = this.buffer.getChannelData(c)

            for (var i = 0; i < length; i++) {
                var start = ~~(i * sampleSize)
                var end = ~~(start + sampleSize)
                var min = chan[0]
                var max = chan[0]

                for (var j = start; j < end; j += sampleStep) {
                    var value = chan[j]

                    if (value > max) {
                        max = value
                    }

                    if (value < min) {
                        min = value
                    }
                }

                peaks[2 * i] = max
                peaks[2 * i + 1] = min

                if (c == 0 || max > mergedPeaks[2 * i]) {
                    mergedPeaks[2 * i] = max
                }

                if (c == 0 || min < mergedPeaks[2 * i + 1]) {
                    mergedPeaks[2 * i + 1] = min
                }
            }
        }

        return this.params.splitChannels ? splitPeaks : mergedPeaks
    },

    disconnectSource: function () {
        if (this.source) {
            this.source.disconnect()
        }
    },

    destroy: function () {
        if (!this.isPaused()) {
            this.pause()
        }
        this.unAll()
        this.buffer = null
        this.disconnectFilters()
        this.disconnectSource()
        this.gainNode.disconnect()
        this.scriptNode.disconnect()
        this.analyser.disconnect()
    },

    load: function (buffer) {
        this.startPosition = 0
        this.lastPlay = this.ac.currentTime
        this.buffer = buffer
        this.createSource()
    },

    createSource: function () {
        this.disconnectSource()
        this.source = this.ac.createBufferSource()

        this.source.buffer = this.buffer
        this.source.connect(this.analyser)
    },

    getDuration: function () {
        if (!this.buffer) {
            return 0
        }
        return this.buffer.duration
    },

}

_.extend(WaveSurfer.WebAudio, WaveSurfer.Observer)



WaveSurfer.MediaElement = {
    init: function (params) {
        this.params = params;

        // Dummy media to catch errors
        this.media = {
            currentTime: 0,
            duration: 0,
            paused: true,
            playbackRate: 1,
            play: function () {},
            pause: function () {}
        };

        this.mediaType = params.mediaType.toLowerCase();
        this.elementPosition = params.elementPosition;
    },

    load: function (url, container, peaks) {
        var my = this;

        var media = document.createElement(this.mediaType);
        media.controls = this.params.mediaControls;
        media.autoplay = this.params.autoplay || false;
        media.preload = 'auto';
        media.src = url;
        media.style.width = '100%';

        media.addEventListener('error', function () {
            my.fireEvent('error', 'Error loading media element');
        });

        media.addEventListener('canplay', function () {
            my.fireEvent('canplay');
        });

        media.addEventListener('ended', function () {
            my.fireEvent('finish');
        });

        media.addEventListener('timeupdate', function () {
            my.fireEvent('audioprocess', my.getCurrentTime());
        });

        var prevMedia = container.querySelector(this.mediaType);
        if (prevMedia) {
            container.removeChild(prevMedia);
        }
        container.appendChild(media);

        this.media = media;
        this.peaks = peaks;
        this.onPlayEnd = null;
        this.buffer = null;
        this.setPlaybackRate(this.playbackRate);
    },

    isPaused: function () {
        return !this.media || this.media.paused;
    },

    getDuration: function () {
        var duration = this.media.duration;
        if (duration >= Infinity) { // streaming audio
            duration = this.media.seekable.end();
        }
        return duration;
    },

    getCurrentTime: function () {
        return this.media && this.media.currentTime;
    },

    getPlayedPercents: function () {
        return (this.getCurrentTime() / this.getDuration()) || 0;
    },

    /**
     * Set the audio source playback rate.
     */
    setPlaybackRate: function (value) {
        this.playbackRate = value || 1;
        this.media.playbackRate = this.playbackRate;
    },

    seekTo: function (start) {
        if (start != null) {
            this.media.currentTime = start;
        }
        this.clearPlayEnd();
    },

    /**
     * Plays the loaded audio region.
     *
     * @param {Number} start Start offset in seconds,
     * relative to the beginning of a clip.
     * @param {Number} end End offset in seconds,
     * relative to the beginning of a clip.
     */
    play: function (start, end) {
        this.seekTo(start);
        this.media.play();
        end && this.setPlayEnd(end);
        this.fireEvent('play');
    },

    /**
     * Pauses the loaded audio.
     */
    pause: function () {
        this.media && this.media.pause();
        this.clearPlayEnd();
        this.fireEvent('pause');
    },

    setPlayEnd: function (end) {
        var my = this;
        this.onPlayEnd = function (time) {
            if (time >= end) {
                my.pause();
                my.seekTo(end);
            }
        };
        this.on('audioprocess', this.onPlayEnd);
    },

    clearPlayEnd: function () {
        if (this.onPlayEnd) {
            this.un('audioprocess', this.onPlayEnd);
            this.onPlayEnd = null;
        }
    },

    getPeaks: function (length) {
        if (this.buffer) {
            return WaveSurfer.WebAudio.getPeaks.call(this, length);
        }
        return this.peaks || [];
    },

    getVolume: function () {
        return this.media.volume;
    },

    setVolume: function (val) {
        this.media.volume = val;
    },

    destroy: function () {
        this.pause();
        this.unAll();
        this.media && this.media.parentNode && this.media.parentNode.removeChild(this.media);
        this.media = null;
    }
}

//For backwards compatibility
_.extend(WaveSurfer.MediaElement, WaveSurfer.Observer)
_.extend(WaveSurfer.AudioElement, WaveSurfer.MediaElement)
