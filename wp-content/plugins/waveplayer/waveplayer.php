<?php
/*
Plugin Name: WavePlayer
Plugin URI: http://waveplayer.luigipulcini.com
Description: Wave Player is an audio player that uses the waveform of each track as the main timeline for the user to interact with.
Author: Luigi Pulcini
Version: 2.0.12
Author URI: http://www.luigipulcini.com
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define ('WAVEPLAYER_VERSION', '2.0.12');
define ('WAVESURFER_VERSION', '1.0.47.212');

$waveplayer_options = array();

if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
    require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
}

if ( queryloop_is_active( 'woocommerce/woocommerce.php' ) || class_exists( 'WooCommerce' ) ) {
    define( 'WOOCOMMERCE_ACTIVE', true );
}

/**
*
* Defines a function to get WavePlayer options
* based on the fact the plugin is Network Activated or activated site by site
*
*/
function get_waveplayer_options() {
    $waveplayer_default_options = array(
        'size'                              => 'md',
        'style'                             => 'light',
        'show_list'                         => 0,
        'shape'                             => 'circle',
        'autoplay'                          => 0,
        'repeat_all'                        => 0,
        'shuffle'                           => 0,
        'wave_color'                        => '#999',
        'wave_color_2'                      => '#666',
        'progress_color'                    => '#444',
        'progress_color_2'                  => '#00e2',
        'cursor_color'                      => '#ee2',
        'cursor_color_2'                    => '#d93',
        'cursor_width'                      => 2,
        'hover_opacity'                     => 40,
        'wave_mode'                         => 4,
        'gap_width'                         => 1,
        'wave_compression'                  => 2,
        'wave_asymmetry'                    => 2,
        'template'                          => '“%title%” by %artist%',
        'custom_css'                        => '',
        'custom_js'                         => '',
        'default_thumbnail'                 => plugins_url( 'assets/img/music_file.png' , __FILE__ ),
        'audio_override'                    => 1,
        'jump'                              => 1,
        'delete_settings'   	            => 0,
        'info'                              => 'bar',
        'playlist_template'                 => '%cart% %likes%',
        'full_width_playlist'               => 10,
        'woocommerce_shop_player'           => 'after',
        'woocommerce_shop_player_size'      => 'default',
        'woocommerce_shop_player_info'      => 'none',
        'woocommerce_remove_shop_image'     => 0,
        'woocommerce_product_player'        => 'after',
        'woocommerce_product_player_size'   => 'default',
        'woocommerce_product_player_info'   => 'none',
        'woocommerce_remove_product_image'  => 0,
        'woocommerce_replace_product_image' => 0,
        'woocommerce_music_type_filter'     => 0,
        'media_library_title'               => 1,
        'ajax_containers'                   => '',
        'version'                           => WAVEPLAYER_VERSION
     );

    if ( is_plugin_active_for_network('waveplayer/waveplayer.php') ) {
        return get_site_option('waveplayer_options', $waveplayer_default_options);
    } else {
        return get_option('waveplayer_options', $waveplayer_default_options);
    }
}

// Wordpress function 'update_site_option' and 'update_option'
function update_waveplayer_options() {
    global $waveplayer_options;

    if ( is_plugin_active_for_network('waveplayer/waveplayer.php') ) {
        return update_site_option('waveplayer_options', $waveplayer_options);
    } else {
        return update_option('waveplayer_options', $waveplayer_options);
    }
}

/**
*
* Activates the plugin for both singlesite and multisite installation of WordPress
*
*/
register_activation_hook( __FILE__, 'waveplayer_activation' );
function waveplayer_activation($networkwide) {
    global $wpdb;

    if (function_exists('is_multisite') && is_multisite()) {
        // check if it is a network activation - if so, run the activation function for each blog id
        if ($networkwide) {
            $old_blog = $wpdb->blogid;
            // Get all blog ids
            $blogids = $wpdb->get_col("SELECT blog_id FROM $wpdb->blogs");
            foreach ($blogids as $blog_id) {
                switch_to_blog($blog_id);
                _waveplayer_activation();
            }
            switch_to_blog($old_blog);
            return;
        }
    }
}

/**
*
* Activates a single site (called multiple times for multisite installations of WordPress)
*
*/
function _waveplayer_activation() {
    global $waveplayer_options;
    $waveplayer_options = get_waveplayer_options();
    $waveplayer_options['delete_settings'] = false;
	if (!$waveplayer_options) {
		waveplayer_update_version();
	}
}


/**
*
* Updates the waveplayer options based on the version that was previously installed
*
*/
function waveplayer_update_version() {
	global $waveplayer_options;

    $waveplayer_options = get_waveplayer_options();

    switch ( true ) {
        case ( version_compare( $waveplayer_options['version'], '1.3.0', '<' ) ):
            $waveplayer_options['default_thumbnail'] = plugins_url( '/img/music_file.png' , __FILE__ );
        case ( version_compare( $waveplayer_options['version'], '1.4.0', '<' ) ):
            $waveplayer_options['audio_override'] = 1;
        case ( version_compare( $waveplayer_options['version'], '2.0.0', '<' ) ):
            $waveplayer_options['info'] = 'playlist';
            $waveplayer_options['custom_js'] = '';
            $waveplayer_options['playlist_template'] = '%cart% %likes%';
            $waveplayer_options['full_width_playlist'] = 0;
            $waveplayer_options['woocommerce_shop_player'] = 'after';
            $waveplayer_options['woocommerce_shop_player_size'] = 'default';
            $waveplayer_options['woocommerce_shop_player_info'] = 'none';
            $waveplayer_options['woocommerce_remove_shop_image'] = 0;
            $waveplayer_options['woocommerce_product_player'] = 'after';
            $waveplayer_options['woocommerce_product_player_size'] = 'default';
            $waveplayer_options['woocommerce_product_player_info'] = 'none';
            $waveplayer_options['woocommerce_remove_product_image'] = 0;
            $waveplayer_options['woocommerce_replace_product_image'] = 0;
            $waveplayer_options['woocommerce_music_type_filter'] = 0;
            $waveplayer_options['delete_settings'] = 0;
            $waveplayer_options['media_library_title'] = 1;
        case ( version_compare( $waveplayer_options['version'], '2.0.7', '<' ) ):
            $waveplayer_options['jump'] = 1;
        case ( version_compare( $waveplayer_options['version'], '2.0.12', '<' ) ):
            $waveplayer_options['ajax_containers'] = '';
        case ( version_compare( $waveplayer_options['version'], '999.999.9999', '<' ) ):
		default:
			$waveplayer_options['version'] = WAVEPLAYER_VERSION;
			break;
	}
	update_waveplayer_options();
}
add_action( 'plugins_loaded', 'waveplayer_update_version' );





function waveplayer_init() {
    global $waveplayer_options;
    global $waveplayer_nonce;

    $waveplayer_options = get_waveplayer_options();
    $waveplayer_nonce = wp_create_nonce( 'waveplayer' );

    $upload_dir = wp_upload_dir();
    define( 'PEAK_FOLDER', trailingslashit( $upload_dir['basedir'] ) . 'peaks/' );
    if ( !file_exists( PEAK_FOLDER ) ) {
        mkdir( PEAK_FOLDER );
    }
    if ( defined( 'WOOCOMMERCE_ACTIVE' ) ) waveplayer_woocommerce_init();

    add_image_size( 'waveplayer-playlist-thumb', 48, 48, true );
}
add_action( 'init', 'waveplayer_init');



function register_music_genre_taxonomies() {

	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Music Genres', 'taxonomy general name' ),
		'singular_name'     => _x( 'Music Genre', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Music Genres' ),
		'all_items'         => __( 'All Music Genres' ),
		'parent_item'       => __( 'Parent Music Genre' ),
		'parent_item_colon' => __( 'Parent Music Genre:' ),
		'edit_item'         => __( 'Edit Music Genre' ),
		'update_item'       => __( 'Update Music Genre' ),
		'add_new_item'      => __( 'Add New Music Genre' ),
		'new_item_name'     => __( 'New Music Genre Name' ),
		'menu_name'         => __( 'Music Genre' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'music_genre' ),
	);

	register_taxonomy( 'music_genre', array( 'attachment' ), $args );

}
add_action( 'init', 'register_music_genre_taxonomies');

function queryloop_is_active( $plugin ) {
	$network_active = false;
	if ( is_multisite() ) {
		$plugins = get_site_option( 'active_sitewide_plugins' );
		if ( isset( $plugins[$plugin] ) ) {
			$network_active = true;
		}
	}
	return in_array( $plugin, get_option( 'active_plugins' ) ) || $network_active;
}

/**
* Retrieve the ID of an audio attachment given its URL
*
* @param string $url, the URL of the audio file
*
* @return int The ID of the attachment on success, 0 on failure
*/
function waveplayer_get_audio_attachment_id( $url ) {
	$attachment_id = 0;
	$dir = wp_upload_dir();
	if ( false !== strpos( $url, $dir['baseurl'] ) ) {
		$file = str_replace($dir['baseurl'].'/', '', $url);

		$query_args = array(
			'post_type'   => 'attachment',
			'meta_key'    => '_wp_attached_file',
			'meta_value'  => $file,
		 );
		$posts = get_posts( $query_args );
		if ( $posts ) $attachment_id = $posts[0]->ID;
	}
	return $attachment_id;
}

function debug_log($var) {
    error_log( print_r( $var, true ) );
}

require 'includes/plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = PucFactory::buildUpdateChecker(
    'http://www.waveplayer.info/rep/waveplayer.json',
    __FILE__
);

include_once('includes/waveplayer-scripts.php');
include_once('includes/waveplayer-admin.php');
include_once('includes/waveplayer-media-templates.php');
include_once('includes/waveplayer-shortcodes.php');
include_once('includes/waveplayer-woocommerce.php');
include_once('includes/waveplayer-ajax-actions.php');
include_once('includes/waveplayer-mce-views.php');

?>
