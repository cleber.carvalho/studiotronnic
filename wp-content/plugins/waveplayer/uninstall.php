<?php

/**
 * WavePlayer Uninstall Procedure v2.0.12
 */

if ( ! defined( 'ABSPATH' ) ) exit;

if ( !defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

$option_name = 'waveplayer_options';

if ( is_multisite() ) {
    $options = get_site_option( $option_name );
} else {
    $options = get_option( $option_name );
}

if ( $options['delete_settings'] ) {
    if ( !is_multisite() ) {
        $upload_dir = wp_upload_dir();
        $upload_subdir = trailingslashit( $upload_dir['basedir'] ) . 'peaks';
        if ( $dir = @opendir( $upload_subdir ) ) {
            while ( false !== ( $file = readdir( $dir ) ) ) {
                if ( is_file( $upload_subdir . $file ) ) @unlink( $upload_subdir . $file );
            }
            closedir( $dir );
            @rmdir( $upload_subdir );
        }
        delete_option( $option_name );
    } else {
        global $wpdb;

        $blog_ids = $wpdb->get_col( "SELECT blog_id FROM $wpdb->blogs" );
        $original_blog_id = get_current_blog_id();

        foreach ( $blog_ids as $blog_id ) {
            switch_to_blog( $blog_id );
            delete_option( $option_name );
            $upload_dir = wp_upload_dir();
            $upload_subdir = trailingslashit( $upload_dir['basedir'] ) . 'peaks';
            if ( $dir = @opendir( $upload_subdir ) ) {
                while ( false !== ( $file = readdir( $dir ) ) ) {
                    if ( is_file( $upload_subdir . $file ) ) @unlink( $upload_subdir . $file );
                }
            }
            closedir( $dir );
            @rmdir( $upload_subdir );
        }

        switch_to_blog( $original_blog_id );
        delete_site_option( $option_name );
    }
}

?>
