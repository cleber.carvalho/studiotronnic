<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
*
* Registers some strings that will be used by WavePlayer for menus and buttons in WordPress Media Manager
*
*/
function waveplayer_media_string($strings,  $post){
    $strings['createNewWaveplayer'] = __('Create a new WavePlayer', 'waveplayer');
    $strings['waveplayerDragInfo'] = __('Drag and drop to reorder tracks.', 'waveplayer');
    $strings['createWaveplayerTitle'] = __('Create WavePlayer', 'waveplayer');
    $strings['editWaveplayerTitle']   = __('Edit WavePlayer', 'waveplayer');
    $strings['cancelWaveplayerTitle'] = __( '&#8592; Cancel WavePlayer' );
    $strings['insertWaveplayer']      = __( 'Insert WavePlayer' );
    $strings['updateWaveplayer']      = __( 'Update WavePlayer' );
    $strings['addToWaveplayer']       = __( 'Add to WavePlayer' );
    $strings['addToWaveplayerTitle']  = __( 'Add to WavePlayer' );
    $strings['createWaveplayerFromURLTitle']  = __( 'Create WavePlayer from URL' );
    $strings['editWaveplayerFromURLTitle']  = __( 'Edit WavePlayer URL' );
    $strings['addWaveplayerFromURLTitle']  = __( 'Add from URL' );
    return $strings;
}
add_filter('media_view_strings', 'waveplayer_media_string', 10, 2);


/**
*
* Creates a template for WavePlayer Settings in WordPress Media Manager
*
*/
function waveplayer_media_template(){
    global $waveplayer_options;
?>

	<script type="text/html" id="tmpl-waveplayer-url-settings">
	</script>

	<script type="text/html" id="tmpl-waveplayer-settings">
		<h2><?php _e( 'WavePlayer Settings', 'waveplayer' ); ?></h2>

		<label class="setting">
            <span><?php _e( 'Size', 'waveplayer' ); ?></span>
            <select data-setting="size" class="setting">
                <option name="size_large"  value="lg" <?php selected( $waveplayer_options['size'] == 'lg' );?>><?php _e('Large', 'waveplayer'); ?></option>
                <option name="size_medium"  value="md" <?php selected( $waveplayer_options['size'] == 'md' );?>><?php _e('Medium', 'waveplayer'); ?></option>
                <option name="size_small"  value="sm" <?php selected( $waveplayer_options['size'] == 'sm' );?>><?php _e('Small', 'waveplayer'); ?></option>
                <option name="size_xsmall"  value="xs" <?php selected( $waveplayer_options['size'] == 'xs' );?>><?php _e('Extra Small', 'waveplayer'); ?></option>
            </select>
		</label>

		<label class="setting">
			<span><?php _e( 'Style', 'waveplayer' ); ?></span>
            <select data-setting="style" class="setting">
                <option name="style_light" value="light" <?php selected( $waveplayer_options['style'] == 'light' );?>>Light</option>
                <option name="style_dark" value="dark" <?php selected( $waveplayer_options['style'] == 'dark' );?>>Dark</option>
            </select>
		</label>

		<label class="setting">
			<span><?php _e( 'Shape', 'waveplayer' ); ?></span>
            <select data-setting="shape" class="setting">
                <option name="shape_square" value="square" <?php selected( $waveplayer_options['shape'] == 'square' );?>><?php _e('Square', 'waveplayer'); ?></option>
                <option name="shape_circle" value="circle" <?php selected( $waveplayer_options['shape'] == 'circle' );?>><?php _e('Circle', 'waveplayer'); ?></option>
                <option name="shape_rounded" value="rounded" <?php selected( $waveplayer_options['shape'] == 'rounded' );?>><?php _e('Rounded', 'waveplayer'); ?></option>
            </select>
		</label>

        <label class="setting">
            <span><?php _e( 'Info display', 'waveplayer' ); ?></span>
            <select data-setting="info" class="setting">
                <option name="waveplayer_info_none" value="none" <?php selected( $waveplayer_options['info'] == 'none' );?>><?php _e('None', 'waveplayer'); ?></option>
                <option name="waveplayer_info_bar" value="bar" <?php selected( $waveplayer_options['info'] == 'bar' );?>><?php _e('Info bar', 'waveplayer'); ?></option>
                <option name="waveplayer_info_playlist" value="playlist" <?php selected( $waveplayer_options['info'] == 'playlist' );?>><?php _e('Info bar and Playlist', 'waveplayer'); ?></option>
            </select>
		</label>

        <label class="setting">
			<span><?php _e( 'Autoplay', 'waveplayer' ); ?></span>
            <input type="checkbox" data-setting="autoplay" value="true" <?php echo $waveplayer_options['autoplay'] ? 'checked' : '';?> />
		</label>

		<label class="setting">
			<span><?php _e( 'Repeat All', 'waveplayer' ); ?></span>
            <input type="checkbox" data-setting="repeat_all" value="true" <?php echo $waveplayer_options['repeat_all'] ? 'checked' : '';?> />
		</label>

		<label class="setting">
			<span><?php _e( 'Shuffle', 'waveplayer' ); ?></span>
            <input type="checkbox" data-setting="shuffle" value="true" <?php echo $waveplayer_options['shuffle'] ? 'checked' : '';?> />
		</label>

		<label class="setting">
            <span><?php _e( 'Wave Color', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="wave_color" value="<?php echo $waveplayer_options['wave_color']; ?>" />
        </label>

		<label class="setting">
            <span><?php _e( 'Wave Color 2', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="wave_color_2" value="<?php echo $waveplayer_options['wave_color_2']; ?>" />
		</label>

		<label class="setting">
            <span><?php _e( 'Prog. Color', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="progress_color" value="<?php echo $waveplayer_options['progress_color']; ?>" />
		</label>

		<label class="setting">
            <span><?php _e( 'Prog. Color 2', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="progress_color_2" value="<?php echo $waveplayer_options['progress_color_2']; ?>" />
		</label>

		<label class="setting">
            <span><?php _e( 'Hover opacity', 'waveplayer' ); ?></span>
            <select data-setting="hover_opacity" class="setting">
                <option name="waveplayer_wave_hover_opacity_100" value="100" <?php selected( $waveplayer_options['hover_opacity'] == '100' );?>>100%</option>
                <option name="waveplayer_wave_hover_opacity_90" value="90" <?php selected( $waveplayer_options['hover_opacity'] == '90' );?>>90%</option>
                <option name="waveplayer_wave_hover_opacity_80" value="80" <?php selected( $waveplayer_options['hover_opacity'] == '80' );?>>80%</option>
                <option name="waveplayer_wave_hover_opacity_70" value="70" <?php selected( $waveplayer_options['hover_opacity'] == '70' );?>>70%</option>
                <option name="waveplayer_wave_hover_opacity_60" value="60" <?php selected( $waveplayer_options['hover_opacity'] == '60' );?>>60%</option>
                <option name="waveplayer_wave_hover_opacity_50" value="50" <?php selected( $waveplayer_options['hover_opacity'] == '50' );?>>50%</option>
                <option name="waveplayer_wave_hover_opacity_40" value="40" <?php selected( $waveplayer_options['hover_opacity'] == '40' );?>>40%</option>
                <option name="waveplayer_wave_hover_opacity_30" value="30" <?php selected( $waveplayer_options['hover_opacity'] == '30' );?>>30%</option>
                <option name="waveplayer_wave_hover_opacity_20" value="20" <?php selected( $waveplayer_options['hover_opacity'] == '20' );?>>20%</option>
                <option name="waveplayer_wave_hover_opacity_10" value="10" <?php selected( $waveplayer_options['hover_opacity'] == '10' );?>>10%</option>
                <option name="waveplayer_wave_hover_opacity_0" value="0" <?php selected( $waveplayer_options['hover_opacity'] == '0' );?>>0%</option>
            </select>
		</label>

		<label class="setting">
            <span><?php _e( 'Cursor color', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="cursor_color" value="<?php echo $waveplayer_options['cursor_color']; ?>" />
		</label>

		<label class="setting">
            <span><?php _e( 'Cursor color 2', 'waveplayer' ); ?></span>
            <input type="text" class="setting waveplayer-color-picker" data-setting="cursor_color_2" value="<?php echo $waveplayer_options['cursor_color_2']; ?>" />
		</label>

		<label class="setting">
            <span><?php _e( 'Cursor width', 'waveplayer' ); ?></span>
            <select data-setting="cursor_width" class="setting">
                <option name="waveplayer_wave_cursor_width_0" value="0" <?php selected( $waveplayer_options['cursor_width'] == '0' );?>><?php _e('Invisible', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_cursor_width_1" value="1" <?php selected( $waveplayer_options['cursor_width'] == '1' );?>><?php _e('Thin (1px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_cursor_width_2" value="2" <?php selected( $waveplayer_options['cursor_width'] == '2' );?>><?php _e('Normal (2px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_cursor_width_4" value="4" <?php selected( $waveplayer_options['cursor_width'] == '4' );?>><?php _e('Thick (4px)', 'waveplayer'); ?></option>
            </select>
		</label>

		<label class="setting">
            <span><?php _e( 'Wave mode', 'waveplayer' ); ?></span>
            <select data-setting="wave_mode" class="setting">
                <option name="waveplayer_wave_mode_0" value="0" <?php selected( $waveplayer_options['wave_mode'] == '0' );?>><?php _e('Continuous', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_1" value="1" <?php selected( $waveplayer_options['wave_mode'] == '1' );?>><?php _e('Bars (1px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_2" value="2" <?php selected( $waveplayer_options['wave_mode'] == '2' );?>><?php _e('Bars (2px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_3" value="3" <?php selected( $waveplayer_options['wave_mode'] == '3' );?>><?php _e('Bars (3px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_4" value="4" <?php selected( $waveplayer_options['wave_mode'] == '4' );?>><?php _e('Bars (4px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_5" value="5" <?php selected( $waveplayer_options['wave_mode'] == '5' );?>><?php _e('Bars (5px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_6" value="6" <?php selected( $waveplayer_options['wave_mode'] == '6' );?>><?php _e('Bars (6px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_7" value="7" <?php selected( $waveplayer_options['wave_mode'] == '7' );?>><?php _e('Bars (7px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_8" value="8" <?php selected( $waveplayer_options['wave_mode'] == '8' );?>><?php _e('Bars (8px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_9" value="9" <?php selected( $waveplayer_options['wave_mode'] == '9' );?>><?php _e('Bars (9px)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_mode_10" value="10" <?php selected( $waveplayer_options['wave_mode'] == '10' );?>><?php _e('Bars (10px)', 'waveplayer'); ?></option>
            </select>
		</label>

        <label class="setting">
            <span><?php _e( 'Gap width', 'waveplayer' ); ?></span>
            <select data-setting="gap_width" class="setting">
                <option name="waveplayer_gap_width_0" value="0" <?php selected( $waveplayer_options['gap_width'] == '0' );?>>0px</option>
                <option name="waveplayer_gap_width_1" value="1" <?php selected( $waveplayer_options['gap_width'] == '1' );?>>1px</option>
                <option name="waveplayer_gap_width_2" value="2" <?php selected( $waveplayer_options['gap_width'] == '2' );?>>2px</option>
                <option name="waveplayer_gap_width_3" value="3" <?php selected( $waveplayer_options['gap_width'] == '3' );?>>3px</option>
                <option name="waveplayer_gap_width_4" value="4" <?php selected( $waveplayer_options['gap_width'] == '4' );?>>4px</option>
                <option name="waveplayer_gap_width_5" value="5" <?php selected( $waveplayer_options['gap_width'] == '5' );?>>5px</option>
                <option name="waveplayer_gap_width_6" value="6" <?php selected( $waveplayer_options['gap_width'] == '6' );?>>6px</option>
                <option name="waveplayer_gap_width_7" value="7" <?php selected( $waveplayer_options['gap_width'] == '7' );?>>7px</option>
                <option name="waveplayer_gap_width_8" value="8" <?php selected( $waveplayer_options['gap_width'] == '8' );?>>8px</option>
                <option name="waveplayer_gap_width_9" value="9" <?php selected( $waveplayer_options['gap_width'] == '9' );?>>9px</option>
                <option name="waveplayer_gap_width_10" value="10" <?php selected( $waveplayer_options['gap_width'] == '10' );?>>10px</option>
            </select>
		</label>

		<label class="setting">
            <span><?php _e( 'Wave comp.', 'waveplayer' ); ?></span>
            <select data-setting="wave_compression" class="setting">
                <option name="waveplayer_wave_compression_linear" value="1" <?php selected( $waveplayer_options['wave_compression'] == '1' );?>><?php _e('None (linear)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_compression_square" value="2" <?php selected( $waveplayer_options['wave_compression'] == '2' );?>><?php _e('Moderate (square)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_compression_cubic" value="3" <?php selected( $waveplayer_options['wave_compression'] == '3' );?>><?php _e('High (cubic)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_compression_4th" value="4" <?php selected( $waveplayer_options['wave_compression'] == '4' );?>><?php _e('Very high (4th order)', 'waveplayer'); ?></option>
                <option name="waveplayer_wave_compression_5th" value="5" <?php selected( $waveplayer_options['wave_compression'] == '5' );?>><?php _e('Extreme (5th order)', 'waveplayer'); ?></option>
            </select>
		</label>

		<label class="setting">
            <span><?php _e( 'Wave asym.', 'waveplayer' ); ?></span>
            <select data-setting="wave_asymmetry" class="setting">
                <option name="waveplayer_wave_asymmetry_1" value="1" <?php selected( $waveplayer_options['wave_asymmetry'] == '1' );?>>1/2 + 1/2</option>
                <option name="waveplayer_wave_asymmetry_2" value="2" <?php selected( $waveplayer_options['wave_asymmetry'] == '2' );?>>2/3 + 1/3</option>
                <option name="waveplayer_wave_asymmetry_3" value="3" <?php selected( $waveplayer_options['wave_asymmetry'] == '3' );?>>3/4 + 1/4</option>
                <option name="waveplayer_wave_asymmetry_4" value="4" <?php selected( $waveplayer_options['wave_asymmetry'] == '4' );?>>4/5 + 1/5</option>
                <option name="waveplayer_wave_asymmetry_5" value="5" <?php selected( $waveplayer_options['wave_asymmetry'] == '5' );?>>5/6 + 1/6</option>
            </select>
		</label>

	</script>

    <script type="text/html" id="tmpl-attachment">
		<div class="attachment-preview js--select-attachment type-{{ data.type }} subtype-{{ data.subtype }} {{ data.orientation }}">
			<div class="thumbnail">
				<# if ( data.uploading ) { #>
					<div class="media-progress-bar"><div style="width: {{ data.percent }}%"></div></div>
				<# } else if ( 'image' === data.type && data.sizes ) { #>
					<div class="centered">
						<img src="{{ data.size.url }}" draggable="false" alt="" />
					</div>
				<# } else { #>
					<div class="centered">
						<# if ( data.image && data.image.src && data.image.src !== data.icon ) { #>
							<img src="{{ data.image.src }}" class="thumbnail" draggable="false" alt="" />
						<# } else { #>
							<img src="{{ data.icon }}" class="icon" draggable="true" alt="" />
						<# } #>
					</div>
					<div class="filename">
                <?php if ( $waveplayer_options['media_library_title'] ) { ?>
						<div>{{ data.title }}</div>
                <?php } else { ?>
                        <div>{{ data.filename }}</div>
                <?php } ?>
					</div>
                    <# if ( 'audio' === data.type ) { #>
<!--
                        <div class="audio">
                            <a class="play-button" data-id="{{data.id}}">PLAY</a>
                            <audio id="{{data.id}}" src="{{ data.url }}"/>
                        </div>
-->
                    <# } #>
				<# } #>
			</div>
			<# if ( data.buttons.close ) { #>
				<button type="button" class="button-link attachment-close media-modal-icon"><span class="screen-reader-text"><?php _e( 'Remove' ); ?></span></button>
			<# } #>
		</div>
		<# if ( data.buttons.check ) { #>
			<button type="button" class="button-link check" tabindex="-1"><span class="media-modal-icon"></span><span class="screen-reader-text"><?php _e( 'Deselect' ); ?></span></button>
		<# } #>
		<#
		var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly';
		if ( data.describe ) {
			if ( 'image' === data.type ) { #>
				<input type="text" value="{{ data.caption }}" class="describe" data-setting="caption"
					placeholder="<?php esc_attr_e('Caption this image&hellip;'); ?>" {{ maybeReadOnly }} />
			<# } else { #>
				<input type="text" value="{{ data.title }}" class="describe" data-setting="title"
					<# if ( 'video' === data.type ) { #>
						placeholder="<?php esc_attr_e('Describe this video&hellip;'); ?>"
					<# } else if ( 'audio' === data.type ) { #>
						placeholder="<?php esc_attr_e('Describe this audio file&hellip;'); ?>"
					<# } else { #>
						placeholder="<?php esc_attr_e('Describe this media file&hellip;'); ?>"
					<# } #> {{ maybeReadOnly }} />
			<# }
		} #>
	</script>
<?php
}
add_action('admin_footer', 'waveplayer_media_template');

?>
