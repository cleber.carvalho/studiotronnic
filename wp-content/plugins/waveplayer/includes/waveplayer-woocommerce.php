<?php

if ( ! defined( 'ABSPATH' ) ) exit;

if ( defined( 'WOOCOMMERCE_ACTIVE') ) {

    function waveplayer_woocommerce_init() {
        global $waveplayer_options;

        $shop_player = $waveplayer_options['woocommerce_shop_player'];
        $product_player = $waveplayer_options['woocommerce_product_player'];

        if ( 'none' != $product_player ) {
            switch ( $product_player ) {
                case 'before':
                    add_action( 'woocommerce_single_product_summary', 'waveplayer_woocommerce_add_player', 4 );
                    break;
                case 'after':
                    add_action( 'woocommerce_single_product_summary', 'waveplayer_woocommerce_add_player', 6 );
                    break;
                case 'after_summary';
                    add_action( 'woocommerce_after_single_product_summary', 'waveplayer_woocommerce_add_player', 5 );
                    break;
            }
        }

        if ( $waveplayer_options['woocommerce_replace_product_image'] ) {
            add_filter( 'woocommerce_single_product_image_thumbnail_html', 'waveplayer_get_product_thumbnail', 30 );
            add_filter( 'woocommerce_cart_item_thumbnail', 'waveplayer_woocommerce_cart_item_thumbnail', 10, 3 );
        }

        if ( 'none' != $shop_player ) {
            remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open' );
            remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close' );

            add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 9 );
            add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 11 );
            add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 9 );
            add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_close', 11 );

            add_action( 'woocommerce_shop_loop_item_title', 'waveplayer_woocommerce_add_player', $shop_player == 'before' ? 5 : 15 );
        }

        // if ( $waveplayer_options['woocommerce_remove_shop_image'] ) {
        //     remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
        //     add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
        // }
        if ( $waveplayer_options['woocommerce_music_type_filter'] ) {
            add_action( 'woocommerce_archive_description', 'waveplayer_woocommerce_add_music_type' );
            add_action( 'woocommerce_product_query', 'waveplayer_woocommerce_music_type_filter' );
        }
    }

    if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
        function woocommerce_template_loop_product_thumbnail() {
            global $post, $waveplayer_options;
            $preview_files = get_post_meta( $post->ID, '_preview_files', true);
            echo waveplayer_get_product_thumbnail();
        }
    }

    function waveplayer_get_product_thumbnail( $post_id = 0, $size = 'shop_catalog' ) {
        global $post;
        if (0 == $post_id) {
            $post_id = $post->ID;
        } else {
            $post = get_post($post_id);
        }
        $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
        if ( wc_placeholder_img_src() ) {
            $image = wc_placeholder_img( $image_size );
        }
        $preview_files = get_post_meta( $post_id, '_preview_files', true);
        if ( $preview_files ) {
            if ( !is_array( $preview_files ) ) {
                $files = explode( ',', $preview_files );
                foreach( $files as $file ) {
                    $id = waveplayer_get_audio_attachment_id( $file );
                    if ( $thumb = get_the_post_thumbnail( $id, $size ) ) {
                        $image = $thumb;
                        break;
                    }
                }
            } else {
                foreach( $preview_files as $key => $file ) {
                    $id = waveplayer_get_audio_attachment_id( $file['file'] );
                    if ( $thumb = get_the_post_thumbnail( $id, $size ) ) {
                        $image = $thumb;
                        break;
                    }
                }
            }
        } else {
            if ( has_post_thumbnail() ) {
                $props = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
                $image = get_the_post_thumbnail( $post_id, $image_size, array(
                    'title'  => $props['title'],
                    'alt'    => $props['alt'],
                ) );
            }
        }
        return $image;
    }

    function waveplayer_woocommerce_cart_item_thumbnail( $content, $cart_item, $cart_item_key ) {
        return waveplayer_get_product_thumbnail($cart_item['product_id'], 'thumbnail');
    }

    function waveplayer_woocommerce_replace_image($content) {
        return waveplayer_get_product_thumbnail();
    }

    function waveplayer_woocommerce_add_preview_files() {
        global $post;
    ?>
        <p class="form-field _music_type_field">
            <label for="_music_type">Music type</label>
            <select id="_music_type" name="_music_type" class="select short" style="">
                <option value="single" <?php selected(get_post_meta( $post->ID, '_music_type', true ) == 'single'); ?>>Single</option>
                <option value="album" <?php selected(get_post_meta( $post->ID, '_music_type', true ) == 'album'); ?>>Album</option>
            </select>
            <span class="description">Choose a music type.</span>
        </p>
        <div class="form-field preview_files">
            <label>Preview files</label>
            <table class="widefat">
                <thead>
                    <tr>
                        <th class="sort">&nbsp;</th>
                        <th>Name <span class="woocommerce-help-tip"></span></th>
                        <th colspan="2">File URL <span class="woocommerce-help-tip"></span></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody class="ui-sortable">
                    <?php
                    $preview_files = get_post_meta( $post->ID, '_preview_files', true );

                    if ( $preview_files ) {
                        if ( !is_array( $preview_files ) ) { ?>
        	                    <tr>
                	            	<td class="sort"></td>
                        	    	<td class="file_name"><input type="text" class="input_text" placeholder="<?php esc_attr_e( 'File Name', 'woocommerce' ); ?>" name="_wc_preview_file_names[]" value="" /></td>
                        	    	<td class="file_url"><input type="text" class="input_text" placeholder="<?php esc_attr_e( "http://", 'woocommerce' ); ?>" name="_wc_preview_file_urls[]" value="<?php echo esc_attr( $preview_files ); ?>" /></td>
                            		<td class="file_url_choose" width="1%"><a href="#" class="button upload_preview_button" data-choose="<?php esc_attr_e( 'Choose file', 'woocommerce' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', 'woocommerce' ); ?>"><?php echo str_replace( ' ', '&nbsp;', __( 'Choose file', 'woocommerce' ) ); ?></a></td>
                            		<td width="1%"><a href="#" class="delete"><?php _e( 'Delete', 'woocommerce' ); ?></a></td>
                            	</tr>
                        <?php } else {
	                        foreach ( $preview_files as $key => $file ) { ?>
        	                    <tr>
                	            	<td class="sort"></td>
                        	    	<td class="file_name"><input type="text" class="input_text" placeholder="<?php esc_attr_e( 'File Name', 'woocommerce' ); ?>" name="_wc_preview_file_names[]" value="<?php echo esc_attr( $file['name'] ); ?>" /></td>
                        	    	<td class="file_url"><input type="text" class="input_text" placeholder="<?php esc_attr_e( "http://", 'woocommerce' ); ?>" name="_wc_preview_file_urls[]" value="<?php echo esc_attr( $file['file'] ); ?>" /></td>
                            		<td class="file_url_choose" width="1%"><a href="#" class="button upload_preview_button" data-choose="<?php esc_attr_e( 'Choose file', 'woocommerce' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', 'woocommerce' ); ?>"><?php echo str_replace( ' ', '&nbsp;', __( 'Choose file', 'woocommerce' ) ); ?></a></td>
                            		<td width="1%"><a href="#" class="delete"><?php _e( 'Delete', 'woocommerce' ); ?></a></td>
                            	</tr>
	                        <?php }
			            }
                    } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="5">
                            <a href="#" class="button insert" data-row="<tr>
    <td class=&quot;sort&quot;></td>
    <td class=&quot;file_name&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;File Name&quot; name=&quot;_wc_preview_file_names[]&quot; value=&quot;&quot; /></td>
    <td class=&quot;file_url&quot;><input type=&quot;text&quot; class=&quot;input_text&quot; placeholder=&quot;http://&quot; name=&quot;_wc_preview_file_urls[]&quot; value=&quot;&quot; /></td>
    <td class=&quot;file_url_choose&quot; width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;button upload_preview_button&quot; data-choose=&quot;Choose file&quot; data-update=&quot;Insert file URL&quot;>Choose&nbsp;file</a></td>
    <td width=&quot;1%&quot;><a href=&quot;#&quot; class=&quot;delete&quot;>Delete</a></td>
    </tr>">Add File</a>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    <?php
    }
    add_action('woocommerce_product_options_downloads', 'waveplayer_woocommerce_add_preview_files' );

    function waveplayer_woocommerce_save_preview_files( $post_id ) {
        global $wpdb;

    	// file paths will be stored in an array keyed off md5(file path)
    	$files = array();

    	if ( isset( $_REQUEST['_wc_preview_file_urls'] ) ) {
    		$file_names         = isset( $_REQUEST['_wc_preview_file_names'] ) ? $_REQUEST['_wc_preview_file_names'] : array();
    		$file_urls          = isset( $_REQUEST['_wc_preview_file_urls'] )  ? wp_unslash( array_map( 'trim', $_REQUEST['_wc_preview_file_urls'] ) ) : array();
    		$file_url_size      = sizeof( $file_urls );

    		for ( $i = 0; $i < $file_url_size; $i ++ ) {
    			if ( ! empty( $file_urls[ $i ] ) ) {
    				// Find type and file URL
    				if ( 0 === strpos( $file_urls[ $i ], 'http' ) ) {
    					$file_is  = 'absolute';
    					$file_url = esc_url_raw( $file_urls[ $i ] );
    				} elseif ( '[' === substr( $file_urls[ $i ], 0, 1 ) && ']' === substr( $file_urls[ $i ], -1 ) ) {
    					$file_is  = 'shortcode';
    					$file_url = wc_clean( $file_urls[ $i ] );
    				} else {
    					$file_is = 'relative';
    					$file_url = wc_clean( $file_urls[ $i ] );
    				}

    				$file_name = wc_clean( $file_names[ $i ] );
    				$file_hash = md5( $file_url );

    				// Validate the file exists
    				if ( 'relative' === $file_is ) {
    					$_file_url = $file_url;
    					if ( '..' === substr( $file_url, 0, 2 ) || '/' !== substr( $file_url, 0, 1 ) ) {
    						$_file_url = realpath( ABSPATH . $file_url );
    					}

    					if ( ! apply_filters( 'woocommerce_downloadable_file_exists', file_exists( $_file_url ), $file_url ) ) {
    						WC_Admin_Meta_Boxes::add_error( sprintf( __( 'The downloadable file %s cannot be used as it does not exist on the server.', 'woocommerce' ), '<code>' . $file_url . '</code>' ) );
    						continue;
    					}
    				}

    				$files[ $file_hash ] = array(
    					'name' => $file_name,
    					'file' => $file_url
    				);
    			}
    		}
    	}

    	update_post_meta( $post_id, '_preview_files', $files );
    }
    add_action('woocommerce_process_product_meta_simple', 'waveplayer_woocommerce_save_preview_files');

    function waveplayer_woocommerce_product_bulk_edit_save( $product ) {
        //waveplayer_woocommerce_save_preview_files( $product->get_id() );
    }
    add_action( 'woocommerce_product_quick_edit_save', 'waveplayer_woocommerce_product_bulk_edit_save', 10, 1 );

    function waveplayer_woocommerce_product_quick_edit_save( $product ) {
        //waveplayer_woocommerce_save_preview_files( $product->get_id() );
    }
    add_action( 'woocommerce_product_quick_edit_save', 'waveplayer_woocommerce_product_quick_edit_save', 10, 1 );

    function waveplayer_woocommerce_save_music_type( $post_id ) {

        if ( ! isset( $_REQUEST['_music_type'] ) ) return;

        update_post_meta( $post_id, '_music_type', $_REQUEST['_music_type'] );
    }
    add_action( 'woocommerce_process_product_meta_simple', 'waveplayer_woocommerce_save_music_type' );


    function waveplayer_woocommerce_add_player() {
        global $post;
        global $waveplayer_options;

        $product = wc_get_product( $post );
        $products = array();
        $file_ids = array();

        if ( $product->is_type( 'grouped' ) ) {
            $products = $product->get_children();
        } else {
            $products[] = $post->ID;
        }
        foreach( $products as $id ) {
            $preview_files = get_post_meta( $id, '_preview_files', true);
            $size = $waveplayer_options['woocommerce_product_player_size'];
            $info = $waveplayer_options['woocommerce_product_player_info'];
            if ( is_shop() || is_product_category() ) {
                $size = $waveplayer_options['woocommerce_shop_player_size'];
                $info = $waveplayer_options['woocommerce_shop_player_info'];
            }
            if ( $preview_files ) {
        	    if ( !is_array( $preview_files ) ) {
                    $file_id = waveplayer_get_audio_attachment_id( $preview_files );
                    $file_ids[] = $file_id;
        	    } else {
                    foreach ( $preview_files as $key => $value ) {
                        $file_id = waveplayer_get_audio_attachment_id( $value['file'] );
                        $file_ids[] = $file_id;
                    }
        	    }
            }
        }
        if ( $file_ids ) {
            $ids = implode( ',', $file_ids );
            echo do_shortcode("[waveplayer size='$size' info='$info' ids='$ids']");
        }
    }


    /**
    *
    * Updates the number of likes in the metadata for the audio track corresponding to the provided post ID
    *
    */
    function waveplayer_ajax_add_to_cart() {
        ob_start();

        $product_id        = $_POST['product'];
        $quantity          = 1;
        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
        $product_status    = get_post_status( $product_id );

        if ( $passed_validation && WC()->cart->add_to_cart( $product_id, $quantity ) && 'publish' === $product_status ) {
            do_action( 'woocommerce_ajax_added_to_cart', $product_id );
            $msg = wc_add_to_cart_message( $product_id, false, true );

            wp_send_json_success( array( 'message' => $msg ) );
        } else {
            $data = array(
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id ),
                'product_id'  => $product_id
            );
            wp_send_json_error( $data );

        }

    }
    add_action( 'wp_ajax_waveplayer_add_to_cart', 'waveplayer_ajax_add_to_cart' );
    add_action( 'wp_ajax_nopriv_waveplayer_add_to_cart', 'waveplayer_ajax_add_to_cart' );

    /**
    *
    * Updates the number of likes in the metadata for the audio track corresponding to the provided post ID
    *
    */
    function waveplayer_ajax_create_product() {

        $type = $_POST['type'] == 'tracks' ? 'single' : 'album';
        $files = array();
        switch( $type ) {
            case 'single':
                $id = $_POST['id'];
                $price = isset($_POST['price']) ? $_POST['price'] : '0.99';
                $track = wp_get_attachment_metadata( $id );
                $post = get_post( $id );
                if ( $post->post_title ) $track['title'] = $post->post_title;
                $file_name = $track['title'];
                $file_name = wc_clean( $file_name );
                $file_url = wp_get_attachment_url($id);
                $file_hash = md5( $file_url );
                $files[ $file_hash ] = array(
                    'name' => $file_name,
                    'file' => $file_url
                );
                $title = $file_name;
                break;
            case 'album':
                $price = isset($_POST['price']) ? $_POST['price'] : '9.99';
                $ids = isset($_POST['tracks']) ? $_POST['tracks'] : '';
                $ids = explode( ',', $ids );
                foreach( $ids as $id ) {
                    $track = wp_get_attachment_metadata( $id );
                    $post = get_post( $id );
                    if ( $post->post_title ) $track['title'] = $post->post_title;
                    $file_name = $track['title'];
                    $file_name = wc_clean( $file_name );
                    $file_url = wp_get_attachment_url($id);
                    $file_hash = md5( $file_url );
                    $files[ $file_hash ] = array(
                        'name' => $file_name,
                        'file' => $file_url
                    );
                }
                $title = isset($_POST['title']) ? $_POST['title'] : _e('New Album', 'waveplayer');
                break;
            default:
                break;
        }

        $post_id = wp_insert_post( array(
            'post_title' => $title,
            'post_status' => 'draft',
            'post_type' => 'product',
        ) );
        wp_set_object_terms( $post_id, 'simple', 'product_type' );
        update_post_meta( $post_id, '_visibility', 'visible' );
        update_post_meta( $post_id, '_stock_status', 'instock');
        update_post_meta( $post_id, 'total_sales', '0' );
        update_post_meta( $post_id, '_virtual', 'yes' );
        update_post_meta( $post_id, '_downloadable', 'yes' );
        update_post_meta( $post_id, '_regular_price', $price );
        update_post_meta( $post_id, '_featured', 'no' );
        update_post_meta( $post_id, '_price', $price );
        update_post_meta( $post_id, '_sold_individually', 'yes' );
        update_post_meta( $post_id, '_preview_files', $files );
        update_post_meta( $post_id, '_music_type', $type );

        wp_send_json_success();
    }
    add_action( 'wp_ajax_waveplayer_create_product', 'waveplayer_ajax_create_product' );
    add_action( 'wp_ajax_nopriv_waveplayer_create_product', 'waveplayer_ajax_create_product' );


    function waveplayer_get_audio_attachments() {
        global $wpdb;

        $tracks = array();
        $albums = array();

        $attachments = $wpdb->get_results("SELECT ID from {$wpdb->posts} WHERE post_type = 'attachment' and post_mime_type LIKE 'audio%';");

        foreach( $attachments as $attachment ) {
            $id = $attachment->ID;
            $track = wp_get_attachment_metadata( $id );
            $track['id'] = $id;
            $post = get_post( $id );
            if ( $post->post_title ) $track['title'] = $post->post_title;
            $track_file = wp_get_attachment_url($id);
            $track['file'] = $track_file;

            $track['product'] = waveplayer_woocommerce_get_product_id( 'single', $track_file );

            $tracks[] = $track;
            if ( isset($track['album']) && $key = $track['album'] ) {
                if ( ! isset( $albums[$key] ) ) {
                    $albums[$key] = array();
                    $albums[$key]['count'] = 0;
                    $albums[$key]['tracks'] = array();
                }
                $albums[$key]['count']++;
                $albums[$key]['tracks'][] = $track['id'];
            }
        }
        foreach( $albums as $title => $album ) {
            $album['product'] = waveplayer_woocommerce_get_product_id( 'album', $title );
            $album['tracks'] = implode( ',', $album['tracks'] );
            $albums[$title] = $album;
        }

        return array( 'tracks' => $tracks, 'albums' => $albums );
    }


    function waveplayer_woocommerce_music_inputs() {
        if ( $result = waveplayer_get_audio_attachments() ) {
            $albums = $result['albums'];
            $tracks = $result['tracks'];
        } else {
            return;
        }
        $track_inputs = '';
        $album_inputs = '';
        foreach($tracks as $track) {
            $id = $track['id'];
            $title = $track['title'];
            $length = $track['length_formatted'];
            $file = basename( $track['file'] );
            $product = $track['product'];
            $disabled = $product > 0 ? "disabled" : "";

            $track_inputs .= "<input type='checkbox' name='music_track_$id' value='$id' $disabled data-title='$title' /><span class='$disabled'>$id. <strong>$title</strong> – $length ($file)</span><br/>";
        }
        $j = 0;
        foreach($albums as $title => $album) {
            $count = $album['count'];
            $plural = $count == 1 ? '' : 's';
            $ids = $album['tracks'];
            $product = isset($album['product']) ? $album['product'] : 0;
            $disabled = $product > 0 ? "disabled" : "";
            $album_inputs .= "<input type='checkbox' name='music_album_$j' value='$j' data-tracks='$ids' data-title='$title' $disabled /><span class='$disabled'><strong>$title</strong> ($count track$plural)</span><br/>";
            $j++;
        }
        return array( 'track_inputs' => $track_inputs, 'album_inputs' => $album_inputs );
    }


    function waveplayer_woocommerce_get_product_id( $music_type, $data = '' ) {

        $product_id = 0;

        $args = array(
            'number_posts'    => 1,
            'post_status'     => 'any',
            'post_type'       => 'product',
        );

        $meta_query = array();
        $meta_query[] = array(
            'key'     => '_music_type',
            'value'   => $music_type,
            'compare' => '='
        );
        if ( $music_type == 'single' ) {
            $meta_query['relation'] = 'AND';
            $meta_query[] = array(
                'key'     => '_preview_files',
                'value'   => $data,
                'compare' => 'LIKE'
            );
        } else {
            $args['title'] = $data;
        }
        $args['meta_query'] = $meta_query;

        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {
            $query->the_post();
            $product_id = get_the_ID();
        }

        return $product_id;
    }

    function waveplayer_woocommerce_add_music_type() {
        $music_type = isset($_GET['music_type']) ? $_GET['music_type'] : 'single';
        ob_start();
?>
        <form class="woocommerce-ordering woocommerce-music-type" method="get">
            <select name="music_type" class="orderby">
                <option value="single" <?php selected( $music_type, 'single' ); ?>>Singles</option>
                <option value="album" <?php selected( $music_type, 'album' ); ?>>Albums</option>
            </select>
        </form>
<?php
        echo ob_get_clean();
    }


    function shop_filter_cat($query) {
        global $post;
        $music_type = isset($_GET['music_type']) ? $_GET['music_type'] : 'single';
        debug_log(array($music_type, $post->post_title));

        if ( !is_admin() && is_post_type_archive( 'product' ) && $query->is_main_query()) {
            $query->set(
                'meta_query',
                array(
                    array (
                        'key' => '_music_type',
                        'value' => $music_type,
                        'compare'   => '='
                    )
                )
            );
        }
    }

    function waveplayer_woocommerce_music_type_filter( $q ){
        $music_type = isset($_GET['music_type']) ? $_GET['music_type'] : 'single';
        $meta_query = $q->get( 'meta_query' );

        $meta_query[] = array(
            'key' => '_music_type',
            'value' => $music_type,
            'compare'   => '='
        );

        $q->set( 'meta_query', $meta_query );
    }

}

?>
