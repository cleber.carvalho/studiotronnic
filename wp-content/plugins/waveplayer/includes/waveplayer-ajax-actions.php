<?php

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'PLAYED_PERCENTAGE', 0.75);

class WVPLStats {
    public $play_count = 0;
    public $runtime = 0;
    public $downloads = 0;
    public $likes = array();

    public function __constructor( $new_play_count = 0, $new_runtime = 0, $new_downloads = 0, $new_likes = array() ) {
        $this->play_count = $new_play_count;
        $this->runtime = $new_runtime;
        $this->downloads = $new_downloads;
        $this->likes = $new_likes;
    }

    public function liked_by( $user_id ) {
        if (!$user_id) return false;
        return in_array( $user_id, $this->likes );
    }

}


/**
*
* Generates an array of tracks based on the audio attachment ids provided
*
* @param    $ids, the list of audio track IDs
*
*/
function waveplayer_create_playlist( $ids ) {
    global $waveplayer_options, $wpdb;

    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;

    $ids_array = array_map('trim', explode(',', $ids));

    $tracks_array = array();
    $i = 0;
    foreach ($ids_array as $id) {
        $track = wp_get_attachment_metadata( $id );
        $stats = get_post_meta( $id, 'wvpl_stats', true );
        if (!$stats) $stats = new WVPLStats();
        $liked = $stats->liked_by( $user_id );

        $post = get_post( $id );
        if ( $post->post_title ) $track['title'] = $post->post_title;
        if ( $post->post_content ) $track['description'] = $post->post_content;
        if ( $post->post_excerpt ) $track['caption'] = $post->post_excerpt;
        $track['post_url'] = str_replace( array( 'https:', 'http:' ), '', get_permalink( $id ) );

        $track['id'] = $id;
        $track['index'] = $i;
        $track_file = wp_get_attachment_url($id);
        $track['file'] = str_replace( array( 'https:', 'http:' ), '', $track_file );
        $poster = str_replace( array( 'https:', 'http:' ), '', wp_get_attachment_thumb_url( get_post_thumbnail_id( $id ) ) );
        $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'waveplayer-playlist-thumb' );
        $thumbnail = str_replace( array( 'https:', 'http:' ), '', $thumb[0] );
        $track['poster'] = is_string($poster) ? $poster : $waveplayer_options['default_thumbnail'];
        $track['thumbnail'] = $thumb ? $thumbnail : $waveplayer_options['default_thumbnail'];
        if ( $peaks = waveplayer_read_peaks( $id ) ) $track['peaks'] = $peaks;

        $track['stats'] = $stats;
        $track['liked'] = $liked;

        if ( defined('WOOCOMMERCE_ACTIVE') ) {
            $track['in_cart'] = 0;
            $args = array(
                'number_posts'    => 1,
                'post_status'     => 'any',
            	'post_type'       => 'product',
            	'meta_query'      => array(
                    'relation'    => 'AND',
                    array(
            			'key'     => '_music_type',
            			'value'   => 'single',
            			'compare' => '='
            		),
                    array(
            			'key'     => '_preview_files',
            			'value'   => $track_file,
            			'compare' => 'LIKE'
            		)
            	)
            );
            $query = new WP_Query( $args );
            if ( $query->have_posts() ) {
                $query->the_post();
                $track['product'] = get_the_ID();
                foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
            		$_product = $values['data'];

            		if( $track['product'] == $_product->id ) {
                        $track['in_cart'] = 1;
                        break;
                	}
                }
            }
        }

        $tracks_array[] = $track;
        $i++;
    }
    return $tracks_array;

}


function runtime_formatted( $seconds ) {
    if (!$seconds) $seconds = 0;
    $seconds = intval( $seconds );
    $s = $seconds % 60;
    $m = floor( $seconds / 60 ) % 60;
    $h = floor( $seconds / 3600);
    $info = $h > 0 ? sprintf( '%d:%02d:%02d', $h, $m, $s ) : sprintf( '%d:%02d', $m, $s );
    return $info;
}


/**
*
* Sends a JSON array of the audio tracks to waveplayer.js through the AJAX call
*
* $_POST['param'] contains the list of ids in the playlist
*
*/
function waveplayer_ajax_load_playlist() {

    $data = waveplayer_create_playlist( $_POST['ids'] );
	wp_send_json_success( $data );

}
add_action( 'wp_ajax_waveplayer_load_playlist', 'waveplayer_ajax_load_playlist' );
add_action( 'wp_ajax_nopriv_waveplayer_load_playlist', 'waveplayer_ajax_load_playlist' );


/**
*
* Sends a JSON array of the audio tracks to waveplayer.js through the AJAX call
*
* $_POST['param'] contains the list of ids in the playlist
*
*/
function waveplayer_ajax_load_page_playlist() {
    global $waveplayer_options, $current_user;

    $list_data = json_decode( stripslashes( html_entity_decode( $_POST['list_data'] ) ) );
    $data_array = array();

    $options = $waveplayer_options;
    $options['site'] = get_bloginfo();
    if ( defined( 'WOOCOMMERCE_ACTIVE') ) $options['cartURL'] = WC()->cart->get_cart_url();

    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;

    foreach ( $list_data as $list ) {
        $data = '';
        if ( $list->ids ) {
            $data = waveplayer_create_playlist( $list->ids );
        } else if ( $list->url ) {
            $data = waveplayer_read_mp3_tags( $list->url );
        }
        $data_array[] = $data;
    }

	wp_send_json_success( array( 'players' => $data_array, 'options' => $options, 'user' => $user_id ) );

}
add_action( 'wp_ajax_waveplayer_load_page_playlist', 'waveplayer_ajax_load_page_playlist' );
add_action( 'wp_ajax_nopriv_waveplayer_load_page_playlist', 'waveplayer_ajax_load_page_playlist' );


/**
*
* Delete peak files of an audio track and sends an array of the peaks to waveplayer.js through the AJAX call
*
* $_POST['mode'] contains the mode of the deletion:
*                   - if 'all', deletes all the files in the peak subfolder of the plugin
*                   - if 'orphan', deletes the peak files corresponding to missing attachments
*
*/
function waveplayer_ajax_delete_peaks() {

    $mode = $_POST['mode'];

    $num = 0;
    $files = array();
    $dir_pointer = opendir( PEAK_FOLDER );
    switch ($mode) {
        case 'orphan':
            while ( false !== ( $file = readdir($dir_pointer) ) ) {
                if ( 'peaks' == pathinfo( $file, PATHINFO_EXTENSION ) && is_numeric( pathinfo( $file, PATHINFO_FILENAME ) ) ) {
                    $id = pathinfo($file, PATHINFO_FILENAME);
                    if ( get_post_mime_type( $id ) !== 'audio/mpeg' ) {
                        $num++;
                        $files[] = $file;
                        unlink( PEAK_FOLDER . $file );
                    }
                }
            }
            break;
        case 'all-internal':
            while ( false !== ( $file = readdir($dir_pointer) ) ) {
                if ( 'peaks' == pathinfo( $file, PATHINFO_EXTENSION ) && is_numeric( pathinfo( $file, PATHINFO_FILENAME ) ) ) {
                    $num++;
                    $files[] = $file;
                    unlink( PEAK_FOLDER . $file );
                }
            }
            break;
        case 'all':
            while ( false !== ( $file = readdir($dir_pointer) ) ) {
                if( is_file( PEAK_FOLDER . $file ) ) {
                    $num++;
                    $files[] = $file;
                    unlink( PEAK_FOLDER . $file );
                }
            }
            break;
        default:
    }

    if ($num) {
        $message = $num == 1 ? $num . ' ' . __( 'file was successfully deleted.', 'waveplayer' )
                             : $num . ' ' . __( 'files were successfully deleted.', 'waveplayer' );
        wp_send_json_success( array( 'message' => $message, 'files' => $files ) );
    } else {
        $message = __( 'No files needed to be deleted.', 'waveplayer' );
        wp_send_json_error( array( 'message' => $message ) );
    }
}
add_action( 'wp_ajax_waveplayer_delete_peaks', 'waveplayer_ajax_delete_peaks' );
add_action( 'wp_ajax_nopriv_waveplayer_delete_peaks', 'waveplayer_ajax_delete_peaks' );


/**
*
* Reads the peak file of an audio track
* and sends an array of the peaks to waveplayer.js through the AJAX call
*
* $id contains the id of the audio track
*
*/
function waveplayer_read_peaks( $id ) {

    $current_user = wp_get_current_user();

    $peak_file = $id == 'sample' ? dirname( __FILE__ ) . '/../assets/peaks/sample.peaks' : trailingslashit( PEAK_FOLDER ) . $id . '.peaks';

    if ( file_exists( $peak_file ) ) {
        $peaks = file_get_contents( $peak_file );

        $peak_array = array_map('floatval', explode(',', $peaks));

        return $peak_array;
    }
    return false;
}


/**
*
* Sends a JSON array of the audio peaks through an AJAX call
*
*/
function waveplayer_ajax_read_peaks() {

    $data = waveplayer_read_peaks( $_POST['id'] );

	wp_send_json_success( $data );

}
add_action( 'wp_ajax_waveplayer_read_peaks', 'waveplayer_ajax_read_peaks' );
add_action( 'wp_ajax_nopriv_waveplayer_read_peaks', 'waveplayer_ajax_read_peaks' );


/**
*
* Writes the peak file of an audio track only if not already present in the peak subfolder
* and sends a message to waveplayer.js through the AJAX call
*
* $_POST['id'] contains the id of the audio track
*
* $result is 'new' if the peak file was not already present in the peak subfolder, 'old' otherwise
*
*/
function waveplayer_ajax_write_peaks() {

    $id = $_POST['id'];

    if ( !file_exists( PEAK_FOLDER . $id . '.peaks' ) ) {
        $peaks = $_POST['peaks'];
        $peaks_array = array_map( 'floatval', explode( ',', $peaks ) );
        $peaks_array = array_map( function($num){return round($num,3);}, $peaks_array );
        file_put_contents( PEAK_FOLDER . $id . '.peaks', implode( ',', $peaks_array ) );

        if ( ! is_numeric( $id ) && $_POST['temp_file'] ) {
            $upload_dir = wp_upload_dir();
            $temp_file = str_replace( $upload_dir['baseurl'], $upload_dir['basedir'], $_POST['temp_file'] );
            unlink( $temp_file );
        }
        wp_send_json_success( array( 'peaks' => $peaks_array ) );
    }
    wp_send_json_error();
}
add_action( 'wp_ajax_waveplayer_write_peaks', 'waveplayer_ajax_write_peaks' );
add_action( 'wp_ajax_nopriv_waveplayer_write_peaks', 'waveplayer_ajax_write_peaks' );


/**
*
* Reads ID3 tags of an MP3 file and sends an array of tags to waveplayer.js through the AJAX call
*
* $url contains the URL to an audio file
*
*/
function waveplayer_read_mp3_tags( $url ) {
    global $waveplayer_options;

    if ( $id = waveplayer_get_audio_attachment_id( $url ) ) {
        $data = wp_get_attachment_metadata( $id );
        $data['peaks'] = waveplayer_read_peaks( $id );
        $poster = wp_get_attachment_thumb_url( get_post_thumbnail_id( $id ) );
        $data['poster'] = is_string($poster) ? $poster : $waveplayer_options['default_thumbnail'];
        $data['id'] = $id;
    } else {
        $parsed_url = parse_url( $url );
        $local_url = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $local_url .= isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $local_url .= isset($parsed_url['path']) ? $parsed_url['path'] : '';

        $local_url = PEAK_FOLDER . preg_replace( '/[\/\:]/', '_', $local_url );
        $id = pathinfo( $local_url, PATHINFO_FILENAME );

        $info_file = PEAK_FOLDER . $id . '.info';

        if ( file_exists( $info_file ) ) {
            $data = json_decode( file_get_contents( $info_file ), true );
            $data['peaks'] = waveplayer_read_peaks( $id );
			$data['id'] = $id;
        } else {
            $contextOptions = array(
                 "ssl" => array(
                     "verify_peer"      => false,
                     "verify_peer_name" => false,
                 ),
            );
            if ( copy( $url, $local_url, stream_context_create( $contextOptions ) ) ) {
                $upload_dir = wp_upload_dir();
                $peak_url = trailingslashit($upload_dir['baseurl']) . 'peaks/';
                $data = wp_read_audio_metadata( $local_url );
                if ( array_key_exists ( 'image', $data ) && $data['image'] != null ) {
                    $image_ext = '.' . preg_replace( '/.*\/(.*)/', '$1', $data['image']['mime'] );
                    $image_file = PEAK_FOLDER . $id . $image_ext;
                    file_put_contents( $image_file, $data['image']['data'] );
                    $data['poster'] = $peak_url . $id . $image_ext;
                    unset($data['image']);
                }
                file_put_contents( $info_file, json_encode( $data ) );
                $local_file = $peak_url . basename($local_url);
                $data['id'] = $id;
                $data['temp_file'] = $local_file;
            }
        }
        $data['file'] = $url;
        return array($data);
    }
    return false;
}


/**
*
* Sends a JSON array of the audio tags through an AJAX call
*
*/
function waveplayer_ajax_read_mp3_tags() {

    $data = waveplayer_read_mp3_tags( $_POST['url'] );

	wp_send_json_success( $data );

}
add_action( 'wp_ajax_waveplayer_read_mp3_tags', 'waveplayer_ajax_read_mp3_tags' );
add_action( 'wp_ajax_nopriv_waveplayer_read_mp3_tags', 'waveplayer_ajax_read_mp3_tags' );


/**
*
* Reads the waveplayer options and returns an array
*
*/
function waveplayer_load_options() {
    global $waveplayer_options;

//    echo json_encode( $waveplayer_options );
    $user_id = get_current_user_id();
    $user_info = get_userdata( $user_id );
    if ( !$user_info ) {
        $user_info['data']['ID'] = '0';
        $user_info['data']['avatar'] = get_avatar( $user_id );
    } else {
        $user_info->avatar = get_avatar( $user_id );
    }
    $options = $waveplayer_options;
    $options['currentUser'] = $user_info;

	return $options;
}


/**
*
* Sends a JSON array of the waveplayer options through an AJAX call
*
*/
function waveplayer_ajax_load_options() {

    $options = waveplayer_load_options();
	wp_send_json_success( array( 'options' => $options ) );

}
add_action( 'wp_ajax_waveplayer_load_options', 'waveplayer_ajax_load_options' );
add_action( 'wp_ajax_nopriv_waveplayer_load_options', 'waveplayer_ajax_load_options' );


/**
*
* Updates the runtime and play count metadata for the audio track corresponding to the provided post ID
*
*/
function waveplayer_ajax_update_statistics() {

    $post_id = $_POST['id'];
    $runtime = round( intval( $_POST['runtime'] ) / 1000 );
    $played = $runtime >= PLAYED_PERCENTAGE * intval( $_POST['length'] ) ? 1 : 0;

    $stats = get_post_meta( $post_id, 'wvpl_stats', true );
    if ( !$stats ) $stats = new WVPLStats();

    $stats->runtime += $runtime;
    $stats->play_count += $played;

    if (false === update_post_meta($post_id, 'wvpl_stats', $stats) ) {
        wp_send_json_error( array( 'message' => 'Error updating the statistics', 'id'=>$post_id, 'runtime'=>$runtime, 'playCount' => $play_count ) );
    }
	wp_send_json_success( array( 'id'=>$post_id, 'stats' => $stats ) );

}
add_action( 'wp_ajax_waveplayer_update_statistics', 'waveplayer_ajax_update_statistics' );
add_action( 'wp_ajax_nopriv_waveplayer_update_statistics', 'waveplayer_ajax_update_statistics' );


/**
*
* Updates the number of likes in the metadata for the audio track corresponding to the provided post ID
*
*/
function waveplayer_ajax_update_likes() {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $post_id = $_POST['id'];

    $stats = get_post_meta( $post_id, 'wvpl_stats', true );
    if (!$stats) $stats = new WVPLStats();

    if ($user_id == 0) {
        wp_send_json_error( array( 'message' => 'Visitors cannot like tracks', 'id' => $post_id, 'stats' => $stats ) );
    }

    $like = $stats->liked_by( $user_id );

    if ( !$like ) {
        $stats->likes[] = $user_id;
    } else {
        $key = array_search( $user_id, $stats->likes );
        if ( false !== $key ) unset( $stats->likes[$key] );
        debug_log(array($user_id, $key, $stats->likes));
    }
    if ( false === update_post_meta($post_id, 'wvpl_stats', $stats ) ) {
        wp_send_json_error( array( 'message' => 'Error updating the likes', 'id' => $post_id, 'stats' => $stats ) );
    }

	wp_send_json_success( array( 'id'=>$post_id, 'stats' => $stats, 'liked' => !$like ) );

}
add_action( 'wp_ajax_waveplayer_update_likes', 'waveplayer_ajax_update_likes' );
add_action( 'wp_ajax_nopriv_waveplayer_update_likes', 'waveplayer_ajax_update_likes' );


/**
*
* Updates the number of likes in the metadata for the audio track corresponding to the provided post ID
*
*/
function waveplayer_ajax_update_downloads() {
    $post_id = $_POST['id'];
    $stats = get_post_meta( $post_id, 'wvpl_stats', true );
    if (!$stats) $stats = new WVPLStats;
    $stats->downloads++;

    if (false === update_post_meta($post_id, 'wvpl_stats', $stats ) ) {
        wp_send_json_error( array( 'message' => 'Error updating the statistics', 'id' => $post_id, 'stats' => $stats ) );
    }

	wp_send_json_success( array( 'id'=>$post_id, 'stats' => $stats ) );

}
add_action( 'wp_ajax_waveplayer_update_downloads', 'waveplayer_ajax_update_downloads' );
add_action( 'wp_ajax_nopriv_waveplayer_update_downloads', 'waveplayer_ajax_update_downloads' );


?>
