<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
*
* Registers the 'waveplayer' shortcode
*
*/
function waveplayer_shortcode( $atts, $content = '' ) {
    global $waveplayer_options;

    $args = shortcode_atts( array(
        'ids'                   =>  '',
        'url'                   =>  '',
        'music_genre'           =>  '',
        'limit'                 =>  '0',
        'size'                  =>  $waveplayer_options['size'],
        'style'                 =>  $waveplayer_options['style'],
        'info'                  =>  $waveplayer_options['info'],
        'shape'                 =>  $waveplayer_options['shape'],
        'autoplay'              =>  $waveplayer_options['autoplay'],
        'repeat_all'            =>  $waveplayer_options['repeat_all'],
        'shuffle'               =>  $waveplayer_options['shuffle'],
        'wave_color'            =>  $waveplayer_options['wave_color'],
        'wave_color_2'          =>  $waveplayer_options['wave_color_2'],
        'progress_color'        =>  $waveplayer_options['progress_color'],
        'progress_color_2'      =>  $waveplayer_options['progress_color_2'],
        'cursor_color'          =>  $waveplayer_options['cursor_color'],
        'cursor_color_2'        =>  $waveplayer_options['cursor_color_2'],
        'cursor_width'          =>  $waveplayer_options['cursor_width'],
        'hover_opacity'         =>  $waveplayer_options['hover_opacity'],
        'wave_mode'             =>  $waveplayer_options['wave_mode'],
        'gap_width'             =>  $waveplayer_options['gap_width'],
        'wave_compression'      =>  $waveplayer_options['wave_compression'],
        'wave_asymmetry'        =>  $waveplayer_options['wave_asymmetry'],
    ), $atts );

    switch ($args['size']) {
        case 'large':
        case 'lg':
            $args['size'] = 'lg';
            break;
        case 'medium':
        case 'md':
            $args['size'] = 'md';
            break;
        case 'small':
        case 'sm':
            $args['size'] = 'sm';
            break;
        case 'xsmall':
        case 'xs':
            $args['size'] = 'xs';
            break;
    }
    $botaoClasse = 'botao-'.$args['ids'];
    $wrapClasse = 'wrap-'.$args['ids'];
    $classes_array = array();
    $classes_array[] = 'prod-'.$args['ids'];
    $classes_array[] = 'wvpl-size-'.$args['size'];
    $classes_array[] = 'wvpl-style-'.$args['style'];
    $classes_array[] = 'wvpl-shape-'.$args['shape'];

    $extra_classes = implode(' ', $classes_array);

    if ( $args['music_genre'] != '' ) {

        $music_genre_tracks = array();

        $post_args = array(
            'post_type' => 'attachment',
            'post_mime_type' => 'audio/mpeg',
            'numberposts' => -1,
            'post_status' => 'any',
            'post_parent' => null,
        );

        switch ( $args['music_genre'] ) {
            case 'all':
            case '*':
                break;
            default:
                $post_args['music_genre'] = $args['music_genre'];
                break;
        }
        $tracks = get_posts( $post_args );
        foreach ( $tracks as $t ) {
            $music_genre_tracks[] = $t->ID;
        }
        $limit = intval($args['limit']);
        if ( $limit ) $music_genre_tracks = array_splice( $music_genre_tracks, 0, $limit );
        $args['ids'] = implode( ',', $music_genre_tracks );
    }

    if ($args['shuffle']) {
        $ids_array = array_map('trim', explode(',', $args['ids']));
        shuffle($ids_array);
        $args['ids'] = implode(',', $ids_array);
    }

    $data = array();
    foreach($args as $name => $value) {
        $data[] = 'data-'.$name.'="'.$value.'"';
    }


    $html = '
        
      
  

                <div class="wrap_player ' . $wrapClasse . ' ">
                    <div class="waveplayer playerab  ' . $extra_classes . '" ' . implode(' ', $data) . '>
                        <div class="wvpl-left-box" style="background-image:url('.$waveplayer_options['default_thumbnail'].')">
                            <div class="wvpl-interface">
                                <div class="wvpl-volume-overlay"></div>
                                <div class="wvpl-button wvpl-info"><span class="wvpl-icon fa fa-list"></span></div>
                                <div class="wvpl-controls">
                                    <div class="wvpl-button wvpl-prev"><span class="wvpl-icon wvpl-disabled fa fa-angle-left"></span></div>
                                    <div class="wvpl-button wvpl-play"><span class=" ' . $botaoClasse . ' wvpl-icon fa fa-play"></span></div>
                                    <div class="wvpl-button wvpl-next"><span class="wvpl-icon wvpl-disabled fa fa-angle-right"></span></div>
                                </div>
                                <div class="wvpl-button wvpl-volume"><span class="wvpl-icon fa fa-volume-up"></span></div>
                            </div>
                            <div class="wvpl-poster"></div>
                        </div>
                        <div class="wvpl-right-box">
                            <div class="wvpl-position"></div>
                            <div class="wvpl-duration"></div>
                            <div class="wvpl-waveform"></div>
                            <div class="wvpl-infobar">
                                <div class="wvpl-playing-info"><div class="wvpl-infoblock"></div></div>
                            </div>
                        </div>
                        <div class="wvpl-playlist">
                            <div class="wvpl-playlist-wrapper"></div>
                        </div>
                    </div>
                </div>

       
    ';

    return $html;
}
add_shortcode( 'waveplayer', 'waveplayer_shortcode' );


/**
*
* Overrides the default audio element using a single track 'waveplayer' shortcode
*
*/
function waveplayer_audio_shortcode_override( $html, $attr, $content, $instances ) {

    global $waveplayer_options;

    if ( !$waveplayer_options['audio_override'] ) return $html;

    if ( !$id = $attr['ids'] ) {
        $id = get_the_ID();
        $source = '';

        if ( get_post_type($id) != 'attachment' ) {
            $id = '';
            if ( isset( $attr['src'] ) ) $source = $attr['src'];
            if ( ! $source && isset( $attr['mp3'] ) ) $source = $attr['mp3'];
            if ( ! $source && isset( $attr['m4a'] ) ) $source = $attr['m4a'];
            if ( ! $source && isset( $attr['ogg'] ) ) $source = $attr['ogg'];
            if ( ! $source && isset( $attr['wav'] ) ) $source = $attr['wav'];
            if ( ! $source && isset( $attr['wma'] ) ) $source = $attr['wma'];

            $id = waveplayer_get_audio_attachment_id( $source );
            if ( $id ) $source = '';
        }
    }

    $atts = array();
    $args = shortcode_atts( array(
        'ids'                   =>  $id,
        'url'                   =>  $source,
        'music_genre'           =>  '',
        'limit'                 =>  '0',
        'size'                  =>  $waveplayer_options['size'],
        'style'                 =>  $waveplayer_options['style'],
        'shape'                 =>  $waveplayer_options['shape'],
        'info'                  =>  $waveplayer_options['info'],
        'autoplay'              =>  isset( $attr['autoplay'] ) ? $attr['autoplay'] : false,
        'repeat_all'            =>  isset( $attr['loop'] ) ? $attr['loop'] : false,
        'shuffle'               =>  $waveplayer_options['shuffle'],
        'wave_color'            =>  $waveplayer_options['wave_color'],
        'wave_color_2'          =>  $waveplayer_options['wave_color_2'],
        'progress_color'        =>  $waveplayer_options['progress_color'],
        'progress_color_2'      =>  $waveplayer_options['progress_color_2'],
        'cursor_color'          =>  $waveplayer_options['cursor_color'],
        'cursor_color_2'        =>  $waveplayer_options['cursor_color_2'],
        'cursor_width'          =>  $waveplayer_options['cursor_width'],
        'hover_opacity'         =>  $waveplayer_options['hover_opacity'],
        'wave_mode'             =>  $waveplayer_options['wave_mode'],
        'gap_width'             =>  $waveplayer_options['gap_width'],
        'wave_compression'      =>  $waveplayer_options['wave_compression'],
        'wave_asymmetry'        =>  $waveplayer_options['wave_asymmetry'],
    ), $atts );

    $html = waveplayer_shortcode($args);
    return $html;
};
add_filter( 'wp_audio_shortcode_override', 'waveplayer_audio_shortcode_override', 10, 4 );
add_filter( 'post_playlist', 'waveplayer_audio_shortcode_override', 10, 4 );

?>
