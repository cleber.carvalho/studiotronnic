<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
*
* Adds a 'Settings' item for WavePlayer in the plugins page
*
*/
function waveplayer_action_links($links, $file) {
    $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=waveplayer">Settings</a>';
    array_unshift($links, $settings_link);
    return $links;
}
add_filter('plugin_action_links_waveplayer/waveplayer.php', 'waveplayer_action_links', 10, 2);

/**
*
* Adds a 'WavePlayer' submenu in WordPress 'Settings' menu
*
*/
function waveplayer_add_options_page() {
    add_options_page( 'WavePlayer', 'WavePlayer', 'manage_options', 'waveplayer', 'waveplayer_admin_page' );
}
add_action('admin_menu','waveplayer_add_options_page');


/**
*
* Creates the main Settings page
*
*/
function waveplayer_admin_page() {

	global $current_user;
	global $waveplayer_options;
	global $wp_roles;

	if(isset($_POST['submit'])) {
		//Form data sent

        foreach ( $waveplayer_options as $option => $value ) {
            if ( isset( $_POST['waveplayer_' . $option] ) ) {
                $waveplayer_options[ $option ] = stripslashes( $_POST[ 'waveplayer_' . $option ] );
            } else {
                $waveplayer_options[ $option ] = 0;
            }
        }

        $waveplayer_options[ 'version' ] = WAVEPLAYER_VERSION;

		update_waveplayer_options();
?>
<div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
<?php
	}

	if (isset($_POST['waveplayer_current_tab'])) {
		$current_tab = $_POST['waveplayer_current_tab'];
	} else {
        if (isset($_GET['tab'])) {
            $current_tab = 'waveplayer-' . $_GET['tab'];
        } else {
            $current_tab = 'waveplayer-player_options';
        }
	}
	$tabs = array(
        'player_options'		=>	__('Player Options','waveplayer'),
        'waveform_options'		=>	__('Waveform Options','waveplayer'),
		'customization' 		=>	__('HTML & CSS','waveplayer'),
		'maintenance'    		=>	__('Maintenance','waveplayer'),
	);

    if ( defined('WOOCOMMERCE_ACTIVE') ) {
        $tabs['woocommerce_options'] = __('WooCommerce Integration','waveplayer');
    }

?>
	<div class="wrap">
        <h1 class="nav-tab-wrapper">
		<?php echo __( 'WavePlayer', 'waveplayer' ) . ' ' . WAVEPLAYER_VERSION; ?>&nbsp;
<?php
			foreach ($tabs as $tab => $title) {
?>
				<span id="waveplayer-tab-<?php echo $tab; ?>" class="waveplayer_tab nav-tab <?php if ($current_tab == 'waveplayer-'.$tab) echo 'nav-tab-active';  ?>" style="cursor:pointer;">
					<?php echo $title; ?>
				</span>
<?php
			}

?>
        </h1>
		<form id="waveplayer_form" name="waveplayer_form" method="post" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
			<input type="hidden" name="waveplayer_current_tab" id="waveplayer_current_tab" value="<?php echo $current_tab; ?>">

            <!-- PLAYER OPTIONS TAB -->
			<div id="waveplayer-player_options" class="waveplayer-option-page" <?php echo ($current_tab != 'waveplayer-player_options' ? 'style="display:none"' : '');  ?>>
				<h2><?php _e( 'Player Default Options', 'waveplayer' ); ?></h2>
                <p>
                    <?php _e('If one of the following parameters is not specified in a shortcode, these are going to be the default settings for the player.', 'waveplayer'); ?>
                </p>
				<table class="form-table">
					<tbody>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_size"><?php _e('Size: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_size">
                                    <option name="waveplayer_size_large" value="lg" <?php selected($waveplayer_options['size'], 'lg'); ?>><?php _e('Large', 'waveplayer' ); ?></option>
                                    <option name="waveplayer_size_medium" value="md" <?php selected($waveplayer_options['size'], 'md'); ?>><?php _e('Medium', 'waveplayer' ); ?></option>
                                    <option name="waveplayer_size_small" value="sm" <?php selected($waveplayer_options['size'], 'sm'); ?>><?php _e('Small', 'waveplayer' ); ?></option>
                                    <option name="waveplayer_size_xsmall" value="xs" <?php selected($waveplayer_options['size'], 'xs'); ?>><?php _e('Extra Small', 'waveplayer' ); ?></option>
                                </select>
                                <br />
                                <span class="description"><?php _e('The player comes in four different sizes: large, medium, small and extra small, that correspond to heights of 200px, 160px, 120px and 80 px respectively.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_style"><?php _e('Style: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_style">
                                    <option name="waveplayer_style_light" value="light" <?php selected($waveplayer_options['style'], 'light'); ?>>Light</option>
                                    <option name="waveplayer_style_dark" value="dark" <?php selected($waveplayer_options['style'], 'dark'); ?>>Dark</option>
                                </select>
                                <br />
                                <span class="description"><?php _e('The player comes in two different styles, that mainly define the information bar where the track info is displayed.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_shape"><?php _e('Shape: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_shape">
                                    <option name="waveplayer_shape_square" value="square" <?php selected($waveplayer_options['shape'], 'square'); ?>><?php _e('Square', 'waveplayer' ); ?></option>
                                    <option name="waveplayer_shape_circle" value="circle" <?php selected($waveplayer_options['shape'], 'circle'); ?>><?php _e('Circle', 'waveplayer' ); ?></option>
                                    <option name="waveplayer_shape_rounded" value="rounded" <?php selected($waveplayer_options['shape'], 'rounded'); ?>><?php _e('Rounded', 'waveplayer' ); ?></option>
                                </select>
                                <br />
                                <span class="description"><?php _e('The thumbnail of the active track is displayed inside a container that can have three different shapes: square, circle and rounded.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_autoplay"><?php _e('Autoplay: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_autoplay" value=1 <?php checked($waveplayer_options['autoplay']); ?>>
                                <span class="description"><?php _e('If checked, the player will start playing back its playlist as soon as the page that contains it finishes loading up.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_repeat_all"><?php _e('Repeat all: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_repeat_all" value=1 <?php checked($waveplayer_options['repeat_all']); ?>>
                                <span class="description"><?php _e('If checked, the player will continuously play back its playlist. At the end of the last track, the player will restart from the first one.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_shuffle"><?php _e('Shuffle: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_shuffle" value=1 <?php checked($waveplayer_options['shuffle']); ?>>
                                <span class="description"><?php _e('If checked, the tracks of each player will be shuffled in a random order every time the page containing the player loads.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_info"><?php _e('Display info: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_info">
                                    <option name="waveplayer_info_none" value="none" <?php selected($waveplayer_options['info'], 'none'); ?>>None</option>
                                    <option name="waveplayer_info_bar" value="bar" <?php selected($waveplayer_options['info'], 'bar'); ?>>Info Bar</option>
                                    <option name="waveplayer_info_playlist" value="playlist" <?php selected($waveplayer_options['info'], 'playlist'); ?>>Info Bar and Playlist</option>
                                </select>
                                <br />
                                <span class="description"><?php _e('Select whether to display the info bar, the playlist or nothing.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_full_width_playlist"><?php _e('Full width playlist: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_full_width_playlist" value=1 <?php checked($waveplayer_options['full_width_playlist']); ?>>
                                <span class="description"><?php _e('If checked, the playlist will span across the full width of the player, instead of just under the waveform.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_default_thumbnail"><?php _e('Default thumbnail: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <div id="waveplayer_default_thumbnail_preview" class="wvpl-thumbnail-preview" style="background-image:url('<?php echo $waveplayer_options['default_thumbnail']; ?>');"></div>
								<input type="hidden" name="waveplayer_default_thumbnail" size="80" value="<?php echo $waveplayer_options['default_thumbnail']; ?>" />
                                <span class="description"><?php _e('Set the image to be displayed as a default thumbnail, whenever an audio track has no featured image associated.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_audio_override"><?php _e('Audio override: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_audio_override" value=1 <?php checked($waveplayer_options['audio_override']); ?>>
                                <span class="description"><?php _e('If checked, every <code>[audio]</code> shortcode will be replaced with WavePlayer.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_jump"><?php _e('Jump to next player: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_jump" value=1 <?php checked($waveplayer_options['jump']); ?>>
                                <span class="description"><?php _e('If checked, upon completion of a playlist, the next player in the page will start.', 'waveplayer'); ?></span>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_media_library_title"><?php _e('Use title in Media Library thumbnail: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="checkbox" name="waveplayer_media_library_title" value=1 <?php checked($waveplayer_options['media_library_title']); ?>>
                                <span class="description"><?php _e('By default, WordPress uses file names to describe the thumbnail of an audio track in the Media Library. Setting this option, the thumbnail will display the title instead.', 'waveplayer'); ?></span>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<input class="button button-primary" type="submit" name="submit" value="<?php _e('Save settings', 'waveplayer' ) ?>" />
				</p>
            </div>


			<!-- WAVEFORM OPTIONS TAB -->
			<div id="waveplayer-waveform_options" class="waveplayer-option-page" <?php echo ($current_tab != 'waveplayer-waveform_options' ? 'style="display:none"' : '');  ?>>
				<h2><?php _e( 'Waveform Options and Color Scheme', 'waveplayer' ); ?></h2>
                <p>
                    <?php _e('If one of the following parameters is not specified in a shortcode, these are going to be the default settings for the player.', 'waveplayer'); ?>
                </p>
				<table class="form-table">
					<tbody>
						<tr valign="top">
							<th scope="row">
								<label><?php _e('Waveform preview: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <div id="wvpl-sample-waveform"></div>
                                <p class="submit">
                                    <input type="text" name="background_color" value="#fff" class="waveplayer-color-picker">
                                    <br />
                                    <span class="description"><?php _e('Select a different background color to test your color scheme (this is not a player option and is just for previewing purpose).', 'waveplayer'); ?></span>
                                </p>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_wave_color"><?php _e('Wave color: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="text" name="waveplayer_wave_color" value="<?php echo $waveplayer_options['wave_color']; ?>" class="waveplayer-color-picker">
                                <span class="wvpl-hspacer"></span>
								<input type="text" name="waveplayer_wave_color_2" value="<?php echo $waveplayer_options['wave_color_2']; ?>"  class="waveplayer-color-picker">
                                <br/>
                                <span class="description"><?php _e('Starting and ending color of a gradient filling the waveform.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_progress_color"><?php _e('Progress color: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="text" name="waveplayer_progress_color" value="<?php echo $waveplayer_options['progress_color']; ?>"  class="waveplayer-color-picker">
                                <span class="wvpl-hspacer"></span>
								<input type="text" name="waveplayer_progress_color_2" value="<?php echo $waveplayer_options['progress_color_2']; ?>"  class="waveplayer-color-picker">
                                <br/>
                                <span class="description"><?php _e('Starting and ending color of a gradient filling the playing portion of the waveform.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_hover_opacity"><?php _e('Hover opacity: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_hover_opacity">
                                    <option name="waveplayer_wave_hover_opacity_100" value="100" <?php selected($waveplayer_options['hover_opacity'], '100'); ?>>100%</option>
                                    <option name="waveplayer_wave_hover_opacity_90" value="90" <?php selected($waveplayer_options['hover_opacity'], '90'); ?>>90%</option>
                                    <option name="waveplayer_wave_hover_opacity_80" value="80" <?php selected($waveplayer_options['hover_opacity'], '80'); ?>>80%</option>
                                    <option name="waveplayer_wave_hover_opacity_70" value="70" <?php selected($waveplayer_options['hover_opacity'], '70'); ?>>70%</option>
                                    <option name="waveplayer_wave_hover_opacity_60" value="60" <?php selected($waveplayer_options['hover_opacity'], '60'); ?>>60%</option>
                                    <option name="waveplayer_wave_hover_opacity_50" value="50" <?php selected($waveplayer_options['hover_opacity'], '50'); ?>>50%</option>
                                    <option name="waveplayer_wave_hover_opacity_40" value="40" <?php selected($waveplayer_options['hover_opacity'], '40'); ?>>40%</option>
                                    <option name="waveplayer_wave_hover_opacity_30" value="30" <?php selected($waveplayer_options['hover_opacity'], '30'); ?>>30%</option>
                                    <option name="waveplayer_wave_hover_opacity_20" value="20" <?php selected($waveplayer_options['hover_opacity'], '20'); ?>>20%</option>
                                    <option name="waveplayer_wave_hover_opacity_10" value="10" <?php selected($waveplayer_options['hover_opacity'], '10'); ?>>10%</option>
                                    <option name="waveplayer_wave_hover_opacity_0" value="0" <?php selected($waveplayer_options['hover_opacity'], '0'); ?>>0%</option>
                                </select>
                                <br />
                                <span class="description"><?php _e('When you hover the wave with the mouse, the progress color will be extended to the position under the cursor.<br />
                                This option defines the opacity of that overlay.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_cursor_color"><?php _e('Cursor color: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<input type="text" name="waveplayer_cursor_color" value="<?php echo $waveplayer_options['cursor_color']; ?>"  class="waveplayer-color-picker">
                                <span class="wvpl-hspacer"></span>
								<input type="text" name="waveplayer_cursor_color_2" value="<?php echo $waveplayer_options['cursor_color_2']; ?>"  class="waveplayer-color-picker">
                                <br />
                                <span class="description"><?php _e('Starting and ending color of a gradient filling the cursor.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_cursor_width"><?php _e('Cursor width: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_cursor_width">
                                    <option name="waveplayer_wave_cursor_width_0" value="0" <?php selected($waveplayer_options['cursor_width'], '0'); ?>><?php _e('Invisible', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_cursor_width_1" value="1" <?php selected($waveplayer_options['cursor_width'], '1'); ?>><?php _e('Thin (1px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_cursor_width_2" value="2" <?php selected($waveplayer_options['cursor_width'], '2'); ?>><?php _e('Normal (2px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_cursor_width_4" value="4" <?php selected($waveplayer_options['cursor_width'], '4'); ?>><?php _e('Thick (4px)', 'waveplayer'); ?></option>
                                </select>
                                <br />
                                <span class="description"><?php _e('Defines the thickness of the cursor line indicating the current playback position.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_wave_mode"><?php _e('Wave mode: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_wave_mode">
                                    <option name="waveplayer_wave_mode_0" value="0" <?php selected($waveplayer_options['wave_mode'], '0'); ?>><?php _e('Continuous', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_1" value="1" <?php selected($waveplayer_options['wave_mode'], '1'); ?>><?php _e('Bars (1px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_2" value="2" <?php selected($waveplayer_options['wave_mode'], '2'); ?>><?php _e('Bars (2px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_3" value="3" <?php selected($waveplayer_options['wave_mode'], '3'); ?>><?php _e('Bars (3px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_4" value="4" <?php selected($waveplayer_options['wave_mode'], '4'); ?>><?php _e('Bars (4px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_5" value="5" <?php selected($waveplayer_options['wave_mode'], '5'); ?>><?php _e('Bars (5px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_6" value="6" <?php selected($waveplayer_options['wave_mode'], '6'); ?>><?php _e('Bars (6px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_7" value="7" <?php selected($waveplayer_options['wave_mode'], '7'); ?>><?php _e('Bars (7px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_8" value="8" <?php selected($waveplayer_options['wave_mode'], '8'); ?>><?php _e('Bars (8px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_9" value="9" <?php selected($waveplayer_options['wave_mode'], '9'); ?>><?php _e('Bars (9px)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_mode_10" value="10" <?php selected($waveplayer_options['wave_mode'], '10'); ?>><?php _e('Bars (10px)', 'waveplayer'); ?></option>
                                </select>
                                <span id="waveplayer_gap_width_group" class="<?php echo $waveplayer_options['wave_mode'] == '0' ? 'wvpl-inactive' : '';?>">
                                    <span class="wvpl-hspacer"><?php _e('with a', 'waveplayer'); ?></span>
                                    <select name="waveplayer_gap_width">
                                        <option name="waveplayer_gap_width_0" value="0" <?php selected($waveplayer_options['gap_width'], '0'); ?>>0px</option>
                                        <option name="waveplayer_gap_width_1" value="1" <?php selected($waveplayer_options['gap_width'], '1'); ?>>1px</option>
                                        <option name="waveplayer_gap_width_2" value="2" <?php selected($waveplayer_options['gap_width'], '2'); ?>>2px</option>
                                        <option name="waveplayer_gap_width_3" value="3" <?php selected($waveplayer_options['gap_width'], '3'); ?>>3px</option>
                                        <option name="waveplayer_gap_width_4" value="4" <?php selected($waveplayer_options['gap_width'], '4'); ?>>4px</option>
                                        <option name="waveplayer_gap_width_5" value="5" <?php selected($waveplayer_options['gap_width'], '5'); ?>>5px</option>
                                        <option name="waveplayer_gap_width_6" value="6" <?php selected($waveplayer_options['gap_width'], '6'); ?>>6px</option>
                                        <option name="waveplayer_gap_width_7" value="7" <?php selected($waveplayer_options['gap_width'], '7'); ?>>7px</option>
                                        <option name="waveplayer_gap_width_8" value="8" <?php selected($waveplayer_options['gap_width'], '8'); ?>>8px</option>
                                        <option name="waveplayer_gap_width_9" value="9" <?php selected($waveplayer_options['gap_width'], '9'); ?>>9px</option>
                                        <option name="waveplayer_gap_width_10" value="10" <?php selected($waveplayer_options['gap_width'], '10'); ?>>10px</option>
                                    </select>
                                    <span class="wvpl-hspacer"><?php _e('gap between bars', 'waveplayer'); ?></span>
                                </span>
                                <br />
                                <span class="description"><?php _e('Defines the visualization mode of the waveform.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_wave_compression"><?php _e('Wave compression: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_wave_compression">
                                    <option name="waveplayer_wave_compression_linear" value="1" <?php selected($waveplayer_options['wave_compression'], '1'); ?>><?php _e('None (linear)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_compression_square" value="2" <?php selected($waveplayer_options['wave_compression'], '2'); ?>><?php _e('Moderate (square)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_compression_cubic" value="3" <?php selected($waveplayer_options['wave_compression'], '3'); ?>><?php _e('High (cubic)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_compression_4th" value="4" <?php selected($waveplayer_options['wave_compression'], '4'); ?>><?php _e('Very high (4th order)', 'waveplayer'); ?></option>
                                    <option name="waveplayer_wave_compression_5th" value="5" <?php selected($waveplayer_options['wave_compression'], '5'); ?>><?php _e('Extreme (5th order)', 'waveplayer'); ?></option>
                                </select>
                                <br />
                                <span class="description"><?php _e('Defines the compression of the wave. This option does not affect the file of the audio, but only the way its waveform gets displayed.<br />
                                A lower level of compression shows a more evident difference between low intensity and high intensity in the waveform.<br />
                                Conversely, a higher level of compression flattens those differences, showing a more uniform wave.', 'waveplayer'); ?></span>
							</td>
						</tr>
						<tr valign="top">
							<th scope="row">
								<label for="waveplayer_wave_asymmetry"><?php _e('Wave asymmetry: ', 'waveplayer' ); ?></label>
							</th>
							<td>
                                <select name="waveplayer_wave_asymmetry">
                                    <option name="waveplayer_wave_asymmetry_1" value="1" <?php selected($waveplayer_options['wave_asymmetry'], '1'); ?>>1/2 + 1/2</option>
                                    <option name="waveplayer_wave_asymmetry_2" value="2" <?php selected($waveplayer_options['wave_asymmetry'], '2'); ?>>2/3 + 1/3</option>
                                    <option name="waveplayer_wave_asymmetry_3" value="3" <?php selected($waveplayer_options['wave_asymmetry'], '3'); ?>>3/4 + 1/4</option>
                                    <option name="waveplayer_wave_asymmetry_4" value="4" <?php selected($waveplayer_options['wave_asymmetry'], '4'); ?>>4/5 + 1/5</option>
                                    <option name="waveplayer_wave_asymmetry_5" value="5" <?php selected($waveplayer_options['wave_asymmetry'], '5'); ?>>5/6 + 1/6</option>
                                </select>
                                <br />
                                <span class="description"><?php _e('Defines the ratio between the height of the top half of the wave and the height of the bottom part of the waves.', 'waveplayer'); ?></span>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<input class="button button-primary" type="submit" name="submit" value="<?php _e('Save settings', 'waveplayer' ) ?>" />
				</p>
            </div>


            <!-- HTML & CSS TAB -->
			<div id="waveplayer-customization" class="waveplayer-option-page" <?php echo ($current_tab != 'waveplayer-customization' ? 'style="display:none"' : '');  ?>>
				<h2><?php _e( 'HTML Customization', 'waveplayer' ); ?></h2>
				<table class="form-table">
					<tbody>
                        <tr valign="top">
                            <td colspan=2>
								<p>
                                    <?php _e('Use the text areas below to customize the content of the Info Bar or the Playlist row.', 'waveplayer'); ?>
                                </p>
								<p>
									<?php _e('You can use HTML syntax. As a placeholder for a specific metadata, you can use any ID3 tag present in the audio file, delimited by the <code>%</code> character.
                                    Please bear in mind that the presence of every tag is not guaranteed for every single track you upload.', 'waveplayer'); ?>
                                </p>
                                <p>
                                    <?php _e('Here is a list of the most common ID3 tags and metadata you can use:', 'waveplayer'); ?>
                                    <code>%album%</code>, <code>%artist%</code>, <code>%bitrate%</code>, <code>%bitrate_mode%</code>, <code>%channelmode%</code>, <code>%channels%</code>,
                                    <code>%compression_ratio%</code>, <code>%dataformat%</code>, <code>%encoder_options%</code>, <code>%file%</code>, <code>%fileformat%</code>,
                                    <code>%filesize%</code>, <code>%genre%</code>, <code>%length%</code>, <code>%length_formatted%</code>, <code>%lossless%</code>,
                                    <code>%mime_type%</code>, <code>%sample_rate%</code>, <code>%title%</code>, <code>%year%</code>.
								</p>
                                <p>
                                    <?php _e('In addition to the previous tags, you can use five special placeholders, as follows:', 'waveplayer'); ?><br/>
                                    <code>%likes%</code>: a like button <span class="fa fa-heart"></span> and a counter of the total likes for a given track,<br/>
                                    <code>%downloads%</code>: a download button <span class="fa fa-download"></span> and a counter of the total downloads for a given track,<br/>
                                    <code>%cart%</code>: a cart button <span class="fa fa-cart-plus"></span> (an active WooCommerce installation is required),<br/>
                                    <code>%play_count%</code>: a counter of the total playbacks for a given track,<br/>
                                    <code>%runtime%</code>: a counter of the total time a given track has been listened to,<br/>
                                    <code>%share_fb%</code>, <code>%share_gp%</code> or <code>%share_tw%</code>: a share button <span class="fa fa-facebook-official"></span>, <span class="fa  fa-google-plus-official"></span> or <span class="fa fa-twitter"></span> to share a track on Facebook, Google+ or Twitter, respectively.
								</p>
							</td>
						</tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_template"><?php _e('Info bar template: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<textarea name="waveplayer_template" size="120" rows="3"><?php echo $waveplayer_options['template']; ?></textarea>
                            </td>
                        </tr>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_playlist_template"><?php _e('Playlist row template: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<textarea name="waveplayer_playlist_template" size="120" rows="3"><?php echo $waveplayer_options['playlist_template']; ?></textarea>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h2><?php _e( 'CSS & JavaScript Customization', 'waveplayer' ); ?></h2>
                <table class="form-table">
                    <tbody>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_custom_css"><?php _e('Custom CSS: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<textarea name="waveplayer_custom_css" size="120" rows="6"><?php echo $waveplayer_options['custom_css']; ?></textarea>
								<span class="description"><?php _e('Use this text area to add your custom CSS code to style the player.', 'waveplayer'); ?></span>
							</td>
						</tr>
                    <?php if ( function_exists( 'wp_add_inline_script' ) ) { ?>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_custom_js"><?php _e('Custom Javascript: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<textarea name="waveplayer_custom_js" size="120" rows="6"><?php echo $waveplayer_options['custom_js']; ?></textarea>
								<span class="description"><?php _e('Use this text area to add your custom javascript code to the player.', 'waveplayer'); ?></span>
							</td>
						</tr>
                    <?php } else { ?>
                        <tr valign="top">
							<td colspan="2">
								<span class="description"><?php _e('<strong>NOTICE</strong>: your current version of WordPress does not support inline scripting. Please consider to upgrade to a newer version.', 'waveplayer'); ?></span>
							</td>
						</tr>
                    <?php } ?>
                        <tr valign="top">
							<th scope="row">
								<label for="waveplayer_ajax_containers"><?php _e('AJAX containers: ', 'waveplayer' ); ?></label>
							</th>
							<td>
								<textarea name="waveplayer_ajax_containers" size="120" rows="6"><?php echo $waveplayer_options['ajax_containers']; ?></textarea>
								<span class="description"><?php _e('You can list here the selectors of all the elements WavePlayer should check for visibility when looking for hidden instances. This option is useful if you are planning to incorporate WavePlayer into dynamic tabs, dropdowns or slides. For example, if you add WavePlayer instances into Bootstrap tabs, you should add <code>.tab-pane</code> in the textarea above. If you need to include more selectors, please separate them by commas (e.g. <code>.tab-pane, .dropdown-pane, #container-1</code>).', 'waveplayer'); ?></span>
							</td>
						</tr>
					</tbody>
				</table>
				<p class="submit">
					<input class="button button-primary" type="submit" name="submit" value="<?php _e('Save settings', 'waveplayer' ) ?>" />
				</p>
			</div>


            <!-- MAINTENANCE TAB -->
            <div id="waveplayer-maintenance" class="waveplayer-option-page" <?php echo ($current_tab != 'waveplayer-maintenance' ? 'style="display:none"' : '');  ?>>
                <h2><?php _e( 'Maintenance', 'waveplayer' ); ?></h2>
                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <td colspan="2">
                                <h2><?php _e( 'Uninstalling WavePlayer', 'waveplayer' ); ?></h2>
                                <p>
                                    <?php _e('By default, WavePlayer does not delete its settings when uninstalling. This will help you keep all your customization when installing a new version of the plugin, that implies you uninstall the previous version first.<br/>
                                    If you want to permanently delete WavePlayer, set the <strong>Delete Settings</strong> checkbox before uninstalling and WavePlayer will delete all its settings during installation.<br/>
                                    All peaks and info files stored in the <strong>/peaks</strong> subfolder will be deleted as well, completely cleaning your WordPress setup from any traces from WavePlayer.', 'waveplayer'); ?>
                                </p>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <label for="waveplayer_delete_settings"><?php _e('Delete settings: ', 'waveplayer' ); ?></label>
                            </th>
                            <td>
                                <input type="checkbox" name="waveplayer_delete_settings" value=1 <?php checked( $waveplayer_options['delete_settings'], true ); ?>>
                                <span class="description"><?php _e( 'Set this option, if you want to delete all WavePlayer settings when uninstalling the plugin.', 'waveplayer' ); ?></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="submit">
                    <input class="button button-primary" type="submit" name="submit" value="<?php _e('Save settings', 'waveplayer' ) ?>" />
                </p>
                <table class="form-table">
                    <tbody>
                        <tr valign="top">
                            <td colspan="2">
                                <hr/>
                                <h2><?php _e( 'Peak and info files', 'waveplayer' ); ?></h2>
                                <p>
                                    <?php _e('A peak file is saved by WavePlayer in the \'<strong>peaks</strong>\' subfolder of the WordPress upload folder, whenever an audio track is loaded for the first time.<br/>
                                    This allows WavePlayer to render the waveform of each audio file much faster the following times.', 'waveplayer'); ?>
                                </p>
                                <p>
                                    <?php _e('Additionally, every time WavePlayer uses an external audio file for the first time, a small text file containing all the information about the external track gets saved in the same folder, together with an image of the cover art, if present in the audio file.
                                    These files are very small in size but help WavePlayer speed up the loading process of external audio files enormously, while offering you the possibility to playback audio files that are not stored in your own web server.', 'waveplayer'); ?>
                                </p>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <label><?php _e('Delete orphan peak files:<br/>(internal only)', 'waveplayer' ); ?></label>
                            </th>
                            <td>
                                <input class="button button-primary wvpl_delete_peaks" type="submit" data-elements="orphan" value="<?php _e('Delete orphan peak files', 'waveplayer' ) ?>" />
                                <p class="description">
                                    <?php _e('If you delete an audio track that was previously used in WavePlayer, the peak file remains unused forever in the peak subfolder of the plugin.<br />
                                    Although peak files are very small in size (usually around 30 kB), it can be a waste of your hosting space if you happen to upload and delete audio attachments regularly.<br />
                                    It is recommended to delete orphan peak files every time you delete a massive amount of audio attachments from your website.', 'waveplayer'); ?>
                                </p>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <label><?php _e('Delete all peak files:<br/>(internal only)', 'waveplayer' ); ?></label>
                            </th>
                            <td>
                                <input class="button button-primary wvpl_delete_peaks" type="submit" data-elements="all-internal" value="<?php _e('Delete all peak files', 'waveplayer' ) ?>" />
                                <p class="description">
                                    <?php _e('If you want to make sure WavePlayer regenerates all the peak files, you have to delete all of them using this button.', 'waveplayer'); ?>
                                </p>
                                <p class="description">
                                    <?php _e('<strong>NOTE</strong>: This operation will not delete any audio file you uploaded in the Media Library.', 'waveplayer'); ?>
                                </p>
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row">
                                <label><?php _e('Delete all peak & info files:<br/>(both internal and external)', 'waveplayer' ); ?></label>
                            </th>
                            <td>
                                <input class="button button-primary wvpl_delete_peaks" type="submit" data-elements="all" value="<?php _e('Delete all peak and info files', 'waveplayer' ) ?>" />
                                <p class="description">
                                    <?php _e('If you want to make sure WavePlayer regenerates all the peak files for both internal and external audio files, you have to delete all of them using this button.', 'waveplayer'); ?>
                                </p>
                                <p class="description">
                                    <?php _e('<strong>NOTE</strong>: This operation will not delete any audio file you uploaded in the Media Library.', 'waveplayer'); ?>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

    <?php if ( defined('WOOCOMMERCE_ACTIVE') ) {

        $result = waveplayer_woocommerce_music_inputs();
        $track_inputs = $result['track_inputs'];
        $album_inputs = $result['album_inputs'];
        $player_default_size = $waveplayer_options['size']

    ?>

        <!-- WOOCOMMERCE OPTIONS TAB -->
        <div id="waveplayer-woocommerce_options" class="waveplayer-option-page" <?php echo ($current_tab != 'waveplayer-woocommerce_options' ? 'style="display:none"' : '');  ?>>
            <h2><?php _e( 'WooCommerce Customization', 'waveplayer' ); ?></h2>
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_woocommerce_auto_player"><?php _e('Add player in Shop Page: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p class="description"><?php _e('WavePlayer can add a player for each item in Shop pages automatically, using the preview files attached to each product.', 'waveplayer'); ?></p><br/>
                            <select name="waveplayer_woocommerce_shop_player">
                                <option name="waveplayer_woocommerce_shop_player_none" value="none" <?php selected($waveplayer_options['woocommerce_shop_player'], 'none'); ?>>Disable</option>
                                <option name="waveplayer_woocommerce_shop_player_before" value="before" <?php selected($waveplayer_options['woocommerce_shop_player'], 'before'); ?>>Insert before the title</option>
                                <option name="waveplayer_woocommerce_shop_player_after" value="after" <?php selected($waveplayer_options['woocommerce_shop_player'], 'after'); ?>>Insert after the title</option>
                            </select>
                            with a
                            <select name="waveplayer_woocommerce_shop_player_size">
                                <option name="waveplayer_woocommerce_shop_player_size_default" value="default" <?php selected($waveplayer_options['woocommerce_shop_player_size'], 'default'); ?>>Default (<?php echo $player_default_size; ?>)</option>
                                <option name="waveplayer_woocommerce_shop_player_size_lg" value="lg" <?php selected($waveplayer_options['woocommerce_shop_player_size'], 'lg'); ?>>Large</option>
                                <option name="waveplayer_woocommerce_shop_player_size_md" value="md" <?php selected($waveplayer_options['woocommerce_shop_player_size'], 'md'); ?>>Medium</option>
                                <option name="waveplayer_woocommerce_shop_player_size_sm" value="sm" <?php selected($waveplayer_options['woocommerce_shop_player_size'], 'sm'); ?>>Small</option>
                                <option name="waveplayer_woocommerce_shop_player_size_xs" value="xs" <?php selected($waveplayer_options['woocommerce_shop_player_size'], 'xs'); ?>>Extra Small</option>
                            </select>
                            size, displaying
                            <select name="waveplayer_woocommerce_shop_player_info">
                                <option name="waveplayer_woocommerce_shop_player_info_none" value="none" <?php selected($waveplayer_options['woocommerce_shop_player_info'], 'none'); ?>>nothing</option>
                                <option name="waveplayer_woocommerce_shop_player_info_bar" value="bar" <?php selected($waveplayer_options['woocommerce_shop_player_info'], 'bar'); ?>>the info bar only</option>
                                <option name="waveplayer_woocommerce_shop_player_info_playlist" value="playlist" <?php selected($waveplayer_options['woocommerce_shop_player_info'], 'playlist'); ?>>both the info bar and the playlist</option>
                            </select>
                            <p><input type="checkbox" name="waveplayer_woocommerce_remove_shop_image" value=1 <?php checked($waveplayer_options['woocommerce_remove_shop_image'], true); ?>/> Also remove the product image.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_woocommerce_product_player"><?php _e('Add player in Product Page: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p class="description"><?php _e('WavePlayer can add a player in Product pages automatically, using the preview files attached to each product.', 'waveplayer'); ?></p><br/>
                            <select name="waveplayer_woocommerce_product_player">
                                <option name="waveplayer_woocommerce_product_player_none" value="none" <?php selected($waveplayer_options['woocommerce_product_player'], 'none'); ?>>Disable</option>
                                <option name="waveplayer_woocommerce_product_player_before" value="before" <?php selected($waveplayer_options['woocommerce_product_player'], 'before'); ?>>Insert before the title</option>
                                <option name="waveplayer_woocommerce_product_player_after" value="after" <?php selected($waveplayer_options['woocommerce_product_player'], 'after'); ?>>Insert after the title</option>
                                <option name="waveplayer_woocommerce_product_player_after_summary" value="after_summary" <?php selected($waveplayer_options['woocommerce_product_player'], 'after_summary'); ?>>Insert after the summary</option>
                            </select>
                            with a
                            <select name="waveplayer_woocommerce_product_player_size">
                                <option name="waveplayer_woocommerce_product_player_size_default" value="default" <?php selected($waveplayer_options['woocommerce_product_player_size'], 'default'); ?>>Default (<?php echo $player_default_size; ?>)</option>
                                <option name="waveplayer_woocommerce_product_player_size_lg" value="lg" <?php selected($waveplayer_options['woocommerce_product_player_size'], 'lg'); ?>>Large</option>
                                <option name="waveplayer_woocommerce_product_player_size_md" value="md" <?php selected($waveplayer_options['woocommerce_product_player_size'], 'md'); ?>>Medium</option>
                                <option name="waveplayer_woocommerce_product_player_size_sm" value="sm" <?php selected($waveplayer_options['woocommerce_product_player_size'], 'sm'); ?>>Small</option>
                                <option name="waveplayer_woocommerce_product_player_size_xs" value="xs" <?php selected($waveplayer_options['woocommerce_product_player_size'], 'xs'); ?>>Extra Small</option>
                            </select>
                            size, displaying
                            <select name="waveplayer_woocommerce_product_player_info">
                                <option name="waveplayer_woocommerce_product_player_info_none" value="none" <?php selected($waveplayer_options['woocommerce_product_player_info'], 'none'); ?>>nothing</option>
                                <option name="waveplayer_woocommerce_product_player_info_bar" value="bar" <?php selected($waveplayer_options['woocommerce_product_player_info'], 'bar'); ?>>the info bar only</option>
                                <option name="waveplayer_woocommerce_product_player_info_playlist" value="playlist" <?php selected($waveplayer_options['woocommerce_product_player_info'], 'playlist'); ?>>both the info bar and the playlist</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_woocommerce_replace_product_image"><?php _e('Replace product image: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p><input type="checkbox" name="waveplayer_woocommerce_replace_product_image" value=1 <?php checked($waveplayer_options['woocommerce_replace_product_image'], true); ?>/> Replace product image with featured image of the corresponding audio track.</p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_woocommerce_music_type_filter"><?php _e('Filter results in shop page by music type: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p><input type="checkbox" name="waveplayer_woocommerce_music_type_filter" value=1 <?php checked($waveplayer_options['woocommerce_music_type_filter'], true); ?>/> If you want to create a filter in the shop page to separate singles from albums.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
            <p class="submit">
                <input class="button button-primary" type="submit" name="submit" value="<?php _e('Save settings', 'waveplayer' ) ?>" />
            </p>
            <h2><?php _e( 'Product Batch Creation', 'waveplayer' ); ?></h2>
            <p>
                <?php _e('The following batch creation processes create a WooCommerce simple product for each of the audio tracks (music singles) or albums (music albums) selected in the list below.<br/>
                For both singles and albums, the batch creation saves a draft product per each item. The default product is simple, virtual and downloadable.<br/>
                WavePlayer automatically associates to each product the corresponding audio track, as a preview files.<br/>
                After the creation of all the product drafts, you can review and publish all the products, adding the downloadable files you want to associate to each product.<br/>', 'waveplayer'); ?>
            </p>
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_music_tracks"><?php _e('Singles: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p>
                                Single price: <?php echo get_woocommerce_currency_symbol(); ?><input type="text" id="waveplayer_woocommerce_price_tracks" name="waveplayer_woocommerce_price_tracks" value="0.99">
                            </p>
                            <div id="waveplayer_music_tracks" style="margin-top:10px;max-height:300px;width:50%;overflow-y:scroll;border:1px solid #666;padding:5px;background:#fff;">
                            <?php
                                echo $track_inputs;
                            ?>
                            </div>
                            <p><a href="#" class="wvpl_toggle_selection" data-type="tracks" data-mode="select"><?php _e('Select all', 'waveplayer' ); ?></a> | <a href="#" class="wvpl_toggle_selection" data-type="tracks" data-mode="deselect"><?php _e('Deselect all', 'waveplayer' ); ?></a></p><br/>
                            <p><a class="button button-primary wvpl_create_products" data-type="tracks"><?php _e('Create product drafts for each selected track', 'waveplayer' ) ?></a><p>
                            <p><progress class="wvpl_products_progress_tracks" value="0" max="1" style="display:none;width:100%;"></progress></p>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">
                            <label for="waveplayer_music_albums"><?php _e('Albums: ', 'waveplayer' ); ?></label>
                        </th>
                        <td>
                            <p class="description">
                                <?php _e('When you upload audio files, WordPress extracts the information regarding the track from the ID3 tags.<br/>
                                If an "album" tag is also specified in the ID3 tags, WavePlayer uses that information to automatically group audio tracks that belong to the same album while creating an WooCommerce album product.<br/>
                                Each audio track that is part of an album but has not been added as a single in the previous section will not be available as a simple product but only as part of the album.', 'waveplayer'); ?>
                            </p>
                            <p>
                                Album price: <?php echo get_woocommerce_currency_symbol(); ?><input type="text" id="waveplayer_woocommerce_price_albums" name="waveplayer_woocommerce_price_albums" value="9.99">
                            </p>
                            <div id="waveplayer_music_albums" style="margin-top:10px;max-height:300px;width:50%;overflow-y:scroll;border:1px solid #666;padding:5px;background:#fff;">
                            <?php
                                echo $album_inputs;
                            ?>
                            </div>
                            <p><a href="#" class="wvpl_toggle_selection" data-type="albums" data-mode="select"><?php _e('Select all', 'waveplayer' ); ?></a> | <a href="#" class="wvpl_toggle_selection" data-type="albums" data-mode="deselect"><?php _e('Deselect all', 'waveplayer' ); ?></a></p><br/>
                            <p><a class="button button-primary wvpl_create_products" data-type="albums"><?php _e('Create product drafts for each selected album', 'waveplayer' ) ?></a><p>
                            <p><progress class="wvpl_products_progress_albums" value="0" max="1" style="display:none;width:100%;"></progress></p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    <?php } ?>

    </form>

<?php
}

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 * CURRENTLY IN DEVELOPMENT
 */

function waveplayer_dashboard_statistics() {

	wp_add_dashboard_widget(
                 'waveplayer_dashboard_statistics',
                 'WavePlayer Statistics',
                 'waveplayer_dashboard_statistics_callback'
        );
}
// add_action( 'wp_dashboard_setup', 'waveplayer_dashboard_statistics' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function waveplayer_dashboard_statistics_callback() {

	// Display whatever it is you want to show.
    global $wpdb;
    $limit = 10;

    $tracks = $wpdb->get_results(
        'SELECT ID, meta_value FROM wp_posts
        INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id
        WHERE wp_postmeta.meta_key = "wvpl_stats"
        AND wp_posts.post_mime_type LIKE "audio/%";'
    );

    $g_stats = array();
    foreach($tracks as $track) {
        $track->meta_value = unserialize($track->meta_value);
        if ($track->meta_value->downloads) $g_stats[$track->ID] = $track->meta_value->downloads;
    }
    arsort($g_stats);

    $max = array_sum($g_stats);

    $j = 0;
    foreach($g_stats as $i => $v) {
        echo get_the_title($i) . ' (' . $v . ')';
        echo '<progress class="wvpl-dash-bars" value="'.$v.'" max="'.$max.'"></progress>';
        echo '<span class="wvpl-dash-values">' . round(10000*$v/$max)/100 . '%</span>';
        $j++;
        if ( $j > $limit ) break;
    }

}

?>
