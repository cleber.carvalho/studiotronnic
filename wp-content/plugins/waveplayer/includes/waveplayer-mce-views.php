<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
*
* Adds WavePlayer styles to the MCE Editor for a Visual rendition of the player
*
*/
function wpview_waveplayer_sandbox_styles() {
 	$version = '?ver=' . WAVEPLAYER_VERSION;

    $fontawesome = '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css';
 	$waveplayer = plugins_url( '/assets/css/style.css' , dirname(__FILE__) ) . $version;

	return array( $fontawesome, $waveplayer );
}


/**
*
* Renders a waveplayer shortcode and sends an head and body of an iframe to waveplayer.js through the AJAX call
*
* (this is used to render a [waveplayer] shortcode in the MCE Editor)
*
*/
function waveplayer_ajax_parse_shortcode() {
	global $post, $wp_scripts, $waveplayer_nonce;

	if ( empty( $_POST['shortcode'] ) ) {
		wp_send_json_error();
	}

	$shortcode = wp_unslash( $_POST['shortcode'] );

	if ( ! empty( $_POST['post_ID'] ) ) {
		$post = get_post( (int) $_POST['post_ID'] );
	}

	if ( ! $post || ! current_user_can( 'edit_post', $post->ID ) ) {
		if ( 'embed' === $shortcode ) {
			wp_send_json_error();
		}
	} else {
		setup_postdata( $post );
	}

	$parsed = do_shortcode( $shortcode );

	if ( empty( $parsed ) ) {
		wp_send_json_error( array(
			'type' => 'no-items',
			'message' => __( 'No items found.' ),
		) );
	}

	$head = '';
	$styles = wpview_waveplayer_sandbox_styles();
    $admin_ajax_url = admin_url('admin-ajax.php');
    $scripts = '
        <script type="text/javascript">
            /* <![CDATA[ */
            var waveplayer_ajax = {"ajax_url":"' . $admin_ajax_url . '","nonce":"'.$waveplayer_nonce.'"};
            /* ]]> */
        </script>
    ';
    if (SCRIPT_DEBUG) {
        $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.js', dirname(__FILE__) ) . '?ver=' . WAVESURFER_VERSION . '"></script>';
    } else {
        $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.min.js', dirname(__FILE__) ) . '?ver=' . WAVESURFER_VERSION . '"></script>';
    }
    $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/waveplayer.js', dirname(__FILE__) ) . '?ver=' . WAVEPLAYER_VERSION . '"/></script>';

	foreach ( $styles as $style ) {
		$head .= '<link type="text/css" rel="stylesheet" href="' . $style . '">';
	}

	if ( ! empty( $wp_scripts ) ) {
		$wp_scripts->done = array();
	}

    ob_start();

    echo $parsed;
    wp_print_scripts(array('jquery', 'underscore'));
    echo $scripts;

	wp_send_json_success( array(
		'head' => $head,
		'body' => ob_get_clean()
	) );
}
add_action( 'wp_ajax_parse-waveplayer-shortcode', 'waveplayer_ajax_parse_shortcode' );


/**
*
* Renders a waveplayer shortcode and sends an head and body of an iframe to waveplayer.js through the AJAX call
*
* (this is used to render an [audio] shortcode in the MCE Editor)
*
*/
function waveplayer_ajax_parse_single_shortcode() {
	global $post, $wp_scripts, $waveplayer_nonce;

	if ( empty( $_POST['shortcode'] ) ) {
		wp_send_json_error();
	}

	$shortcode = wp_unslash( $_POST['shortcode'] );

	if ( ! empty( $_POST['post_ID'] ) ) {
		$post = get_post( (int) $_POST['post_ID'] );
	}

	// the embed shortcode requires a post
	if ( ! $post || ! current_user_can( 'edit_post', $post->ID ) ) {
		if ( 'embed' === $shortcode ) {
			wp_send_json_error();
		}
	} else {
		setup_postdata( $post );
	}

	$parsed = do_shortcode( $shortcode );

	if ( empty( $parsed ) ) {
		wp_send_json_error( array(
			'type' => 'no-items',
			'message' => __( 'No items found.' ),
		) );
	}

	$head = '';
	$styles = wpview_waveplayer_sandbox_styles();
    $admin_ajax_url = admin_url('admin-ajax.php');
    $scripts = '
        <script type="text/javascript">
            /* <![CDATA[ */
            var waveplayer_ajax = {"ajax_url":"' . $admin_ajax_url . '","nonce":"'.$waveplayer_nonce.'"};
            /* ]]> */
        </script>
    ';
    if (SCRIPT_DEBUG) {
        $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.js', dirname(__FILE__) ) . '?ver=' . WAVESURFER_VERSION . '"></script>';
    } else {
        $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.min.js', dirname(__FILE__) ) . '?ver=' . WAVESURFER_VERSION . '"></script>';
    }
    $scripts .= '<script type="text/javascript" src="' . plugins_url( '/assets/js/waveplayer.js', dirname(__FILE__) ) . '?ver=' . WAVEPLAYER_VERSION . '"/></script>';

	foreach ( $styles as $style ) {
		$head .= '<link type="text/css" rel="stylesheet" href="' . $style . '">';
	}

	if ( ! empty( $wp_scripts ) ) {
		$wp_scripts->done = array();
	}

    ob_start();

    echo $parsed;
    wp_print_scripts(array('jquery', 'underscore'));
    echo $scripts;

	wp_send_json_success( array(
		'head' => $head,
		'body' => ob_get_clean()
	) );
}
add_action( 'wp_ajax_parse-waveplayer-single-shortcode', 'waveplayer_ajax_parse_single_shortcode' );

?>
