<?php

if ( ! defined( 'ABSPATH' ) ) exit;


function waveplayer_template_path() {
    return plugin_dir_path( dirname(__FILE__) ) . '/templates/';
}

function waveplayer_template( $slug, $name = '' ) {
    ob_start();

    $template = $slug . $name ? "-$name" : "";
    include( waveplayer_template_path() . "$slug-$name.php" );

    return ob_get_clean();
}

debug_log(waveplayer_template('playlist', 'row'));
