<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
*
* Sets the text domain for plugin's translations
*
*/
function waveplayer_load_languages() {
	load_plugin_textdomain('waveplayer', false, dirname( plugin_basename( __FILE__ ) )  . '/lang/' );
}
add_action('init', 'waveplayer_load_languages');


/**
*
* Enqueues styles and scripts for both the front end and the back end
* (since the plugin also overrides the default audio element,
* styles and scripts must be enqueued also for the back end)
*
*/
function waveplayer_enqueue_scripts() {

    global $waveplayer_nonce;
    global $waveplayer_options;

//  Adds the required dependency scripts
	$deps = array( 'jquery', 'underscore' );

//  Adds Wavesurfer.js support to the plugin
    if (SCRIPT_DEBUG) {
        wp_register_script('waveplayer_wavesurfer-dev', plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.js' , dirname(__FILE__) ), $deps, WAVESURFER_VERSION, true);
        wp_enqueue_script('waveplayer_wavesurfer-dev');
    } else {
        wp_register_script('waveplayer_wavesurfer', plugins_url( '/assets/js/wavesurfer/wavesurfer.wvpl.min.js' , dirname(__FILE__) ), $deps, WAVESURFER_VERSION, true);
        wp_enqueue_script('waveplayer_wavesurfer');
    }

//  Adds Font Awesome support to the plugin
	wp_register_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style('font-awesome');

//  Adds custom style to the front-end
	wp_register_style('waveplayer_style', plugins_url( '/assets/css/style.css' , dirname(__FILE__) ), array(), WAVEPLAYER_VERSION);
	wp_enqueue_style('waveplayer_style');
    wp_add_inline_style( 'waveplayer_style', $waveplayer_options['custom_css'] );

//  Adds custom scripts to the front-end
	wp_register_script('waveplayer_class', plugins_url( '/assets/js/waveplayer.js', dirname(__FILE__) ), array('jquery'), WAVEPLAYER_VERSION, true);
	wp_enqueue_script('waveplayer_class');
	$custom_js = '(function($){' . $waveplayer_options['custom_js'] . '})(jQuery);';
	if ( function_exists( 'wp_add_inline_script' ) ) {
		wp_add_inline_script( 'waveplayer_class', $custom_js );
	}

	wp_localize_script( 'waveplayer_class', 'waveplayer_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'nonce' => $waveplayer_nonce ) );

}
add_action('wp_enqueue_scripts', 'waveplayer_enqueue_scripts');
add_action('admin_enqueue_scripts', 'waveplayer_enqueue_scripts');


/**
*
* Enqueues the scripts for the back end only
*
*/
function waveplayer_admin_enqueue_scripts() {
    global $waveplayer_nonce;

    $waveplayer_nonce = wp_create_nonce( "waveplayer" );

	$deps = array( 'jquery', 'underscore', 'mce-view', 'wp-color-picker', 'media-views' );

    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_media();

//  Adds custom scripts to the front-end
	wp_register_script('waveplayer_media_waveplayer', plugins_url( '/assets/js/media-waveplayer.js', dirname(__FILE__) ), $deps, WAVEPLAYER_VERSION, true);
	wp_enqueue_script('waveplayer_media_waveplayer');

	wp_register_script('waveplayer_admin_scripts', plugins_url( '/assets/js/admin-scripts.js', dirname(__FILE__) ), $deps, WAVEPLAYER_VERSION, true);
	wp_enqueue_script('waveplayer_admin_scripts');

	wp_localize_script( 'waveplayer_admin_scripts', 'waveplayer_ajax', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'nonce' => $waveplayer_nonce ) );

	wp_register_style('waveplayer_admin_style', plugins_url( '/assets/css/admin_style.css' , dirname(__FILE__) ), array(), WAVEPLAYER_VERSION);
	wp_enqueue_style('waveplayer_admin_style');

}
add_action('admin_enqueue_scripts', 'waveplayer_admin_enqueue_scripts');

?>
