=== Wave Player ===
Tags: audio, audio player, media, mp3, music, player, playlist, plugin, wave, waveform, waveplayer, wordpress
Requires at least: 3.9
Tested up to: 4.8
Stable tag: 2.0.12


== What is WavePlayer? ==

WavePlayer is a fully customizable, responsive HTML5 audio plugin for WordPress. Its interface is built around the waveform of the audio file that is playing back.
Visit the official page at http://waveplayer.luigipulcini.com

Here is a list of the main features:

*   responsive interface, with a modern looking style
*   HTML5 support
*   WordPress Multisite support
*   four different sizes that automatically adapt to your page
*   full integration with the WordPress Media Manager
*   Visual Editor in WordPress Post Editor
*   archival of peak files for an instantaneous access to the waveforms

== What is new in v2.0.0 ==

WavePlayer has been accurately rewritten to make it faster, more efficient and to incorporate all the features requested by customers during the past year.

* PLAYLIST PANEL - WavePlayer finally features a visible list of all the tracks included in a single instance. The site administrator can easily configure the info and buttons shown on each row of the list.
* PEAK FILES - Audio tracks get analyzed as soon as they are uploaded to the Media Library. This way you do not have to wait for the waveform to be render when using a track for the first time, which is particularly useful for extremely large files. Moreover, a fully renovated Maintenance section allows you to Manage peak files more easily.
* FASTER AND MORE EFFICIENT - All player instances in a single page are loaded simultaneously, reducing enormously the loading time and making the interaction with the server much faster and more efficient. For example, if you have 10 instances on a page, the older versions of WavePlayer sent 10 calls to the server, getting the playlist of each instance at a time. WavePlayer 2.0 analyzes the current page and send to the server a single request for all the instances on the page. This makes the interaction with the server more efficient and reduces the loading time enormously.
* CUSTOMIZATION – WavePlayer is very appreciated by its customers for its incredible customization capabilities. With the new version 2.0.0 the possibilities go even further. With the introduction of the new playlist panel, you can now customize the information shown on both the Info Bar and the playlist rows. The number of placeholders you can now use to customize the info shown has expanded. The webmaster can now customize both the Info Bar and the playlist adding buttons and stats for the most common utilities, including likes, social shares, download, play count and runtime. The system is easy and flexible, allowing you to customize what to show.
* WOOCOMMERCE INTEGRATION – WavePlayer is now fully compatible with WooCommerce. The integration aims to automate as much as possible the creation process for music products, such as music singles or music albums. The new features include: the creation of a product meta value that stores the music type of the product (single or album); the creation of a product meta value that stores the location of the preview files for a single or an album; the automatic insertion of the player, based on the preview files, in both the Shop and Product pages; the optional replacement of the product image with the featured image connected to the preview file; a double batch process that automates the creation of products for singles and albums.
* SETTINGS – When updating the plugin to a new version, you can now keep your current configuration. You will never lose your favorite settings when updating to a newer version.


== How to install it? ==

*   In WordPress Plugins section, click on the Add new button, right beside the Plugins page title.
*   Click on the Upload Plugin button, right beside the Add Plugins page title.
*   Click on the Choose file button in order to select the location of your waveplayer.zip file.
*   Browse to the location where you downloaded the waveplayer.zip file.
*   Click on the Open button.
*   The plugin is now active. You can click on the Settings link right below its name or go to Settings -> WavePlayer to configure its options.
*   Once WordPress has finished uploading the plugin files, click on the Activate Plugin link.
*   The plugin is now active. You can click on the Settings link right below its name or go to Settings -> WavePlayer to configure its options.


== How to use it? ==

Starting from version 3.9, WordPress makes it possible to create audio playlists directly from the WordPress Media Manager. WavePlayer takes full advantage of this capability introducing a waveplayer shortcode that extends the default playlist shortcode. This new shortcode offers the users a more modern interface for their audio tracks, while keeping the same ease of operation in creating their favorite playlists. In addition to that, WavePlayer also overrides the built-in audio shortcode, so that you will automatically find a single-track instance of WavePlayer wherever you have already an audio shortcode in your pages. Even in the backend!

= How to create a new instance of WavePlayer =

*	In order to create a new instance of WavePlayer, simply create or edit the Post or the Page where you want to add the WavePlayer and click on the Add Media button, right above the Visual Editor toolbars.
*	In the left sidebar, together with all the other elements WordPress allows you to create, you will find a menu to create a new instance of WavePlayer. Click on Create Waveplayer.
*	If you have already uploaded some audio files, you can select it here. Otherwise, click on the Upload files tab, in order to upload new audio files. (WordPress allows you to attach a featured image to audio tracks as well as to any other post type. When you set the feature image of an audio track, WavePlayer will use it as the thumbnail of that track in the player).
*	Upload your audio tracks dragging them to WordPress drop area or Selecting files from your local drives.
*	The files uploaded while you are in the process of creating a new Waveplayer instance get automatically selected for the instance’s playlist.
*	While the uploading is still processing, you can select more tracks already present in your Media Library
*	When the upload is complete and you are happy with you selection, you can finally create a new instance of WavePlayer clicking on the Create a new WavePlayer button.
*	Now, you can edit your instance of WavePlayer, starting with the order of the tracks. In order to change the order, simply drag and drop their thumbnails around.
*	In the WavePlayer Settings panel, you can change the default settings of the player, that will alter the aspect of this instance in the page.
*	When you are happy with your configuration, click on the Insert WavePlayer button to insert the instance you have just created in your page or post.
*	You can immediately preview the instance you have just inserted in the post editor. You can even playback the audio tracks! WavePlayer allows you to work with a visually complete and fully interactive rendition of the player right in the post editor, thus saving you the time to go back and forth between the post editor and the actual page in the front end. If you want to make changes to the selected instance, simply click on the button.
*	If you want to add more tracks to your playlist, click on the Add to Waveplayer menu item.
*	When altering an instance of WavePlayer that you have previously inserted in your post, you can still add more audio tracks, selecting them from the Media Library or even uploading new files, the exact same way you did when originally creating the player.
*	When you are happy with your selection, click on the Add to WavePlayer button to see your new tracks added to the previous playlist of the player.
*	Of course, you can still change the order of the tracks (draggind the files around) or the aspect of the player (altering the WavePlayer settings). When you are done with you editing, click on the Update WavePlayer button to make your changes effective.
*	Click on the Publish (for newly created posts or pages) or the Update button (for already published posts or pages) to finalize your changes. Now it is time for you to visit the page with your final result!


== Shortcode ==

Although the waveplayer shortcode with all its parameters is created automatically when inserting the Waveplayer through the Add Media frame, it is useful to understand its structure that is composed as follows:

        [ waveplayer ids = "comma separated IDs of the selected tracks"
                     url = ""
                    size = "lg" | "md" | "sm" | "xs"
                   style = "light" | "dark"
                   shape = "square" | "circle" | "rounded"
                    info = "none" | "bar" | "playlist"
                autoplay = "true" | "false"
              repeat_all = "true" | "false"
                 shuffle = "true" | "false"
              wave_color = "#ddd"
            wave_color_2 = "#666"
          progress_color = "#59f"
        progress_color_2 = "#05a"
           hover_opacity = "0.4"
            cursor_color = "#ee2"
          cursor_color_2 = "#d93"
            cursor_width = "2"
               wave_mode = "4"
               gap_width = "1"
        wave_compression = "2"
          wave_asymmetry = "2" ]

= Parameters =

ids – This is the only required parameter and it gets automatically populated with the IDs of the tracks selected in the Add Media frame. If left blank together with the following parameter mp3, this instance of the Waveplayer will be hidden.

mp3 – If the ids parameter is left blank, you can provide a URL to an mp3 file through this parameter. If the file is local, Waveplayer will try to access the IDS tags information and show them in the info bar of the player.

size – Defines the size of the player. Accepted values are: lg, md, sm and xs. If left blank, the player will be displayed at the default size setting, according to the corresponding option of Settings –> Waveplayer.

style – Can be set to light or dark and defines the coloring of the area where the information of the current track are displayed. Furthermore, Waveplayer is fully customizable through the Custom CSS option of Settings –> Waveplayer. If left blank, the player will use the default style setting, according to the corresponding option of Settings –> Waveplayer.

shape – Can be set to square, circle or rounded and defines the shape of the thumbnail where the playback controls are located. If left blank, the player will use the default shape setting, according to the corresponding option of Settings –> Waveplayer.

info – Can be set to none, bar or playlist and defines the information displayed by the instance of the player. If left blank, the player will use the default info setting, according to the corresponding option of Settings –> Waveplayer.

autoplay – If set to true, the player will start to playback the track as soon as the page completes loading. If set to false, the user will have to start the playback manually. If left blank, the player will use the default autoplay setting, according to the corresponding option of Settings –> Waveplayer.

repeat_all – If set to true, the player will keep playing back the tracks continuously, restarting from the first track when the last one has ended. If set to false, the playback will stop after the last track has ended. If left blank, the player will use the default repeat_all setting, according to the corresponding option of Settings –> Waveplayer.

shuffle – If set to true, the player will shuffle the tracks every time the visitor reloads the page. If set to false, the order of the tracks will be the one provided with the ids parameter. If left blank, the player will use the default shuffle setting, according to the corresponding option of Settings –> Waveplayer.

wave_color, wave_color_2 – Defines the starting and ending color of the vertical gradient filling the waveform. If left blank, the player will use the default wave_color and wave_color_2 settings, according to the corresponding option of Settings –> Waveplayer. For a better understanding of the waveform coloring scheme, please refer to the Waveform Options section of the Settings.

progress_color, progress_color_2 – Defines the starting and ending color of the vertical gradient filling the performed portion of the waveform. If left blank, the player will use the default progress_color and progress_color_2 settings, according to the corresponding option of Settings –> Waveplayer. For a better understanding of the waveform coloring scheme, please refer to the Waveform Options section of the Settings.

hover_opacity – Defines the opacity of the wave overlay when the user moves the pointer over the wave. Set this parameter to 0 to have a regular continuous waveform. If left blank, the player will use the default hover_opacity setting, according to the corresponding option of Settings –> Waveplayer.

cursor_color, cursor_color_2 – Defines the starting and ending colors of a vertical gradient for the cursor scrolling over the waveform. If left blank, the player will use the default cursor_color and cursor_color_2 settings, according to the corresponding option of Settings –> Waveplayer. For a better understanding of the waveform coloring scheme, please refer to the Waveform Options section of the Settings.

cursor_width – Defines the width of the bars displaying the waveform. Set this parameter to 0 to have a regular continuous waveform. If left blank, the player will use the default cursor_width setting, according to the corresponding option of Settings –> Waveplayer.

wave_mode – Defines the visualization mode of the wave. If set to 0 (‘Continuous’ in the settings page), the waveform will be displayed as a regular continuous waveform. Any other setting from 1 to 10 represents the width a the bars used to display the waveform in a histogram fashion. If left blank, the player will use the default wave_mode setting, according to the corresponding option of Settings –> Waveplayer.

gap_width – If wave_mode is set to any value other than 0 (‘Continuous’), this parameter defines the width of the gap between the bars representing the waveform. If left blank, the player will use the default gap_width setting, according to the corresponding option of Settings –> Waveplayer.

wave_compression – Defines the compression of the wave. This option does not affect the file of the audio, but only the way its waveform gets displayed. A lower level of compression shows a more evident difference between low intensity and high intensity in the waveform. Conversely, a higher level of compression flattens those differences, showing a more uniform wave. If left blank, the player will use the default wave_compression setting, according to the corresponding option of Settings –> Waveplayer.

wave_asymmetry – Defines the ratio between the height of the top half of the wave and the height of the bottom part of the waves. If left blank, the player will use the default wave_asymmetry setting, according to the corresponding option of Settings –> Waveplayer.


== Settings ==

WavePlayer settings – located at the Settings –> WavePlayer page of the WordPress Admin area – define how to display a shortcode with no parameters provided other than the IDs of the audio tracks you want to included in that particular instance of the player, like the following one:

Please note that changing any of these settings will affect the way each instance with ANY missing parameter will be displayed on your website.


= Default player options =

In the first panel of the WavePlayer Settings you can define the default options for the size, style, shape, autoplay, repeat_all and shuffle parameters. Whenever you insert an instance of the Waveplayer without providing any of these parameters, the instance will be displayed according to these settings.

= Default waveform options and color scheme =

In the second panel of the WavePlayer Settings you can set all the default values that define how the waveform is going to be rendered. Whenever you insert an instance of the Waveplayer without providing any of these parameters in the shortcode, the instance will be displayed according to these settings.
Through these options you can have the waveform perfectly matching your current WordPress theme and thanks to the realtime preview, you can see the result of your changes without even the need to save the settings.
Furthermore, all these settings can be added as parameters to each Waveplayer shortcode, customizing each instance of the player, regardless of these default settings.

= HTML & CSS Customization =

In the last panel you will find two different textareas.
The first one, Info bar template, allows you to customize the information shown in the info bar when a track starts playing back. You can use pretty much every ID3 tag you know are saved in the audio file you uploaded. Simply write you HTML template including each tag surrounded by a “%” character. For example, if you want to show the title of the song followed by the artist name, you can write the following HTML code in the textarea: %title% by %artist%
The second textarea, Custom CSS, allows you to add your own CSS rules to the default stylesheet that comes with the plugin. You can either customize the default CSS rules or create your own set of rules to be added in the info bar template.

= Maintenance =

A peak file is saved by WavePlayer in the ‘peaks‘ subfolder of the plugin, whenever an audio track is loaded for the first time. This allows WavePlayer to render the waveform of each audio file much faster the following times.
If you delete an audio track that was previously used in WavePlayer, the peak file remains unused forever in the peak subfolder of the plugin. Although peak files are very small in size (usually around 30 kB), it can be a waste of your hosting space if you happen to upload and delete audio attachments regularly. It is recommended to delete orphan peak files clicking on the Delete orphan peak files button every time you delete a massive amount of audio attachments from your website.
If, for any reason, you want to make sure WavePlayer regenerates all the peak files, you have to delete them using the Delete all peak files button.


== Credits ==

WavePlayer makes use of the following projects:

*   wavesurfer.js – The waveform design capability you find in WavePlayer is built around waveform.js, by katspaugh, optimized to a more efficient management of the canvas capabilities and extended to be integrated as a WordPress plugin. You can learn more about that project at the Wavesurfer.js Official Page or at the Wavesurfer.js GitHub Ppage. Wavesurfer.js is licensed under a Creative Commons Attribution 3.0 Unported License.

*   Font-Awesome – Buttons and icons featured in WavePlayer are provided by Font Awesome, a CSS toolkit based on an iconic font. The font is released under a SIL OFL 1.1 License. The CSS code is released under a MIT License.


== Changelog ==

= 2.0.12 =
* NEW: WavePlayer now detects the visibility status of elements to automatically refresh the content of the waveform. This is particularly useful when including WavePlayer instances into dynamically toggable elements.
* BUGFIX: Clicking on a playlist item in scenario with multiple playlists on a single page always starts the first track of the last instance.

= 2.0.11 =
* NEW: Placeholders now include 'attributes' in the form %key{"attribute":value}%, for future template developments
* BUGFIX: Player size on WooCommerce Shop page did not correspond to settings
* BUGFIX: Social share links are populated with an incorrect URL
* BUGFIX: Default thumbnail doesn't show in the playlist
* BUGFIX: Under WooCommerce integration panel, a query assumes the posts table name is "wp_posts"

= 2.0.10 =
* NEW: Added a "limit" parameter to the shortcode, so you can get only the first n result in combination with the "music_genre" parameter
* FIX: WooCommerce 3.0 deprecated the Download Type option. This made WavePlayer's integration malfunctioning.
* BUGFIX: On iOS devices, playback does not start when clicking on a playlist row

= 2.0.9 =
* BUGFIX: Missing info and cover art for remote files.

= 2.0.8 =
* NEW: When the playlist is scrollable, jumping to a track makes the corresponding item in the list visible.

= 2.0.7 =
* BUGFIX: The option "Jump to the next player" does not stay checked saving the settings.

= 2.0.6 =
* BUGFIX: Downloads counter does not get updated

= 2.0.5 =
* NEW: WavePlayer now overrides both audio and playlist shortcodes
* BUGFIX: Install fails on WordPress versions older than 4.5

= 2.0.4 =
* NEW: WavePlayer now supports auto update from the Plugins page
* BUGFIX: WooCommerce product thumbnail not showing for non-music products

= 2.0.3 =
* BUGFIX: Players on WooCommerce product pages always show in large size

= 2.0.2 =
* NEW: Added a track sharer for LinkedIn
* BUGFIX: Peak files for new external files don't get generated

= 2.0.1 =
* BUGFIX: Player does not load on website where WooCommerce is not installed or active
* BUGFIX: Players don't show the spinning icon when page is loading
* BUGFIX: On WooCommerce Shop Page, first item never gets listed
* BUGFIX: When batch creating WooCommerce products, progress bar doesn't work properly

= 2.0.0 =
* NEW: A new customizable Playlist Panel. The webmaster can configure the info and buttons displayed on each row
* NEW: WooCommerce integration. Preview files and Music Type (singles or albums) for simple, virtual, music, downloadable products.
* NEW: WooCommerce integration. Plenty of options to automatically integrate the player in your music products.
* NEW: WooCommerce integration. The webmaster can now create products based on audio files using a batch process. It is also possible to add a cart button on both the Info Bar and the Playlist Panel that interact with WooCommerce cart through AJAX.
* NEW: Peak files now get generated upon uploading of new audio files.
* NEW: Loading time have been reduced enormously. WavePlayer now loads all the instances on one page with a single AJAX call to the server. This allows all the instances to load simultaneously.
* NEW: New placeholders to insert info and buttons (like, share, cart, statistics) in both the Info Bar and the Playlist Panel
* NEW: Plugin's options don't get removed from the database when removing the plugin. This is particularly useful when updating to a new version of the plugin without losing your favorite settings.
* NEW: Managing peak files from the Maintenance tab of the Settings page is now easier and more advanced.
* NEW: Thumbnails for audio files in the Media Library can now show the title of a track instead of the file.
* NEW: A new method "refresh" that allows you to force the redraw of the waveform and is particularly useful when loading or showing content via javascript
* NEW: Current position and total length of the current track are now shown on the waveform
* FIX: Minor CSS improvements

= 1.4.2 =
* BUGFIX: Info Bar broken when inserting the %file% placeholder.

= 1.4.1 =
* BUGFIX: Playback is not available until all instances in the page completed loading

= 1.4.0 =
* NEW: Thanks to a completely rewritten AJAX call strategy, instance loading is now twice as fast as before.
* NEW: In case of multiple instances in one page, the instances load in the order they appear on the page (from top to bottom).
* NEW: It is now possible to choose whether WavePlayer override the audio shortcode or not
* NEW: WavePlayer is now fully compatible with Internet Explorer 11. The compatibility with Internet Explorer 9 and 10 will be improved soon.
* BUGFIX: The audio shortcode did not look correctly in the Post Editor and it was not possible to edit its content

= 1.3.4 =
* NEW: When automatically replacing an audio shortcode, WavePlayer now tries to verify if the URL provided actually corresponds to an attachment in the Media Library. If it does, WavePlayer uses the attachment ID instead of its URL.

= 1.3.3 =
* BUGFIX: Automatic replacement of audio shortcodes does not work if the src attribute is not provided

= 1.3.2 =
* NEW: The Info Bar automatically hides if one of the placeholders used in the template does not get replaced by any data value
* NEW: A spinning icon shows the loading status of the player. This is particularly useful when loading a file for the first time or from a remote location.
* BUGFIX: Getting peaks for remote audio files generates cross domain XHR error

= 1.3.1 =
* BUGFIX: Track's title not displaying properly when original audio file does not have the corresponding ID3 tag

= 1.3.0 =
* NEW: Introducing a new taxonomy 'Music Genre' for attachments
* NEW: Create a player that will automatically include all audio files pertaining to one or more 'Music Genre'
* NEW: Create an instance simply providing the URL of an external audio file: WavePlayer will retrieve all the info stored in the ID3 tags, including a cover art picture if available, and make a local copy for a faster future access (only the ID3 tags and the cover art will be stored locally, not the audio file).
* NEW: Customize the default image that appears when a track does not have its own thumbnail
* FIX: Color Pickers now work also in the Media Manager, not only in the WavePlayer Settings page
* FIX: Minor interface improvements

= 1.2.2 =
* BUGFIX: Default settings not applying correctly to the shortcode when editing its properties in the Media Manager
* BUGFIX: 'autoplay' not working properly in combination with 'repeat_all'
* FIX: A recent Chrome update changed some CSS default behaviors that messed up with the aspect of the player controls
* FIX: Minor improvements to the styling of the player controls

= 1.2.1 =
* BUGFIX: WavePlayer preview not showing properly in the post editor on some WordPress multisite installations

= 1.2.0 =
* NEW: Multisite support. WavePlayer is now fully compatible with any WordPress Multisite installation
* FIX: Graphic improvements in Safari

= 1.1.2 =
* NEW: Added a volume control
* NEW: Added a button to toggle the visibility of the title bar
* BUGFIX: "No container found" error in javascript of the admin area
* BUGFIX: the "NEXT" skip button disappears on last track even with repeat_all activated if the playlist only contains two tracks

= 1.0.18 =
* First release
