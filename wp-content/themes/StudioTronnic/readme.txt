V.1.4 31.5.2013
-ready for wp 3.6

-fixed variations for WooCommerce
-added options for display category in shop page
/woocommerce - all folder
/style.css from line 3772
/css/style_options.php

-fixed recent viewed products widget
/function.php

-fixed sorting portfolio bug
/function.php
/portfolio2.php
/portfolio3.php
/portfolio4.php
/portfolio3_home.php

V.1.3 13.3.2013

==================================================
Note: Inside main zip folder you have 2 theme zip. Use:
-----for WooCoomerce up to 1.6 use musica_woo_1.6.zip
-----for WooCoomerce from 2.0 use musica_woo_2.0.zip
===================================================

-fixed bug with audio player on single product
-added child theme support
-fixed some small bugs in admin

V.1.2 11.2.2013
==================================================
Note: Inside main zip folder you have 2 theme zip. Use:
-----for WooCoomerce up to 1.6 use musica_woo_1.6.zip
-----for WooCoomerce from 2.0 use musica_woo_2.0.zip
===================================================
-fixed one security issue
-replaced ereg functions with preg

/function.php
/category_template1.php
/css/style_options.php
/admin/admin-interface.php

V.1.1 4.2.2013
==================================================
Note: Inside main zip folder you have 2 theme zip. Use:
-----for WooCoomerce up to 1.6 use musica_woo_1.6.zip
-----for WooCoomerce from 2.0 use musica_woo_2.0.zip
===================================================
====================================================
musica_woo_1.6.zip and musica_woo_2.0.zip  CHANGED FILES
====================================================

-fixed iphone responsive bug
-fixed some small bugs

/function.php
/include/scripts.php
/header.php
/css/style-options.php
/css/options.css

-aded big menu options
-fixed iphone responsive bug
/style.css

-fixed bug with prices in related product
-fixed small bug in responsive mode

-general fixes
/function.php
/style.css
/css/style-options.php
/woocommerce/single-product/up-sell.php

-fix bux with video product display on ff browser

/include/script.php
/js/jquery.fitvids.js - added
/woocommerce/single-product/up-sell.php
/includes/boxes/homeracentProductF.php
/includes/boxes/homeracentProduct.php


====================================================
ONLY musica_woo_2.0.zip  CHANGED FILES
====================================================

-WooCommerce RC1 (2.0) ready.

 --sort order fix
/woocommerce/archive-product_template_1.php
/woocommerce/archive-product_template_2.php
/woocommerce/loop/sorting.php

--widget woocommerce
/includes/widget/widget-recent_reviews_pmc.php
/includes/widget/widget-price_filter_pmc.php

--add to card fix
/js/custom.js

--single product fix
/woocommerce/single-product/tabs.php
/woocommerce/single-product/tabs/tab-reviews.php
/woocommerce/single-product/tabs/product-image.php


====================================================
V.1.5
====================================================
-added new responsive menu
-added options to add socila icons
-added template with revolution slider
-fixed few small bugs
-improved responsive mode
-changed single product layout
-added zoom function
-fixed bug with stock display




/js/custom.js
/header.php
/footer.php
/function.php
/style.css
/css/style_options.php
/rev_slider--- added
/admin/ --all files
/woocommerce/ --all files
/includes/ --all files

====================================================
V.1.6
====================================================
-woocommerce 2.1

====================================================
V.1.7
====================================================
-woocommerce sorting
-woocommerce update files


/woocommerce/
/style.css

====================================================
V.1.8
====================================================
-player bug


/woocommerce/single-product/short-description.php
/style.css



====================================================
V.1.9
====================================================
-fixed shortcode bug
/js/tinymce_shortcodes.php

====================================================
V.2.0
====================================================
Woocommerce 2.3 ready.
