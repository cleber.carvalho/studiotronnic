<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.

// This file was the current value of auto_prepend_file during the Wordfence WAF installation (Fri, 28 Apr 2017 23:20:41 +0000)
if (file_exists('/var/local/php/fix_docroot.php')) {
	include_once '/var/local/php/fix_docroot.php';
}
if (file_exists('/var/www/html/studiotronnic.com.br/web/wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", '/var/www/html/studiotronnic.com.br/web/wp-content/wflogs/');
	include_once '/var/www/html/studiotronnic.com.br/web/wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>